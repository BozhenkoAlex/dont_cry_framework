set(cmake_use_version 3.24)
cmake_minimum_required(VERSION ${cmake_use_version})

# set the project name
project(DontCryEngine)

set (CMAKE_CXX_STANDARD 17)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/BIN)

set (TEST_ENGINE 0)

#add_compile_definitions(THIRD_CAMERA)
#add_compile_definitions(UI_EXAMPLE)
#add_compile_definitions(SHADOW_TEST)
#add_compile_definitions(SELECTION_TEST)


find_package(OpenGL REQUIRED)
find_package(glfw3 CONFIG REQUIRED)
find_package(assimp CONFIG REQUIRED)
find_package(GLEW REQUIRED)

set(THIRDPARTY_HEADERS
    ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/glm/include/
    ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/json/single_include/
    )

add_subdirectory(engine)

# TODO: it shouldn't be here
set(IMGUI_SRC_PATH ${CMAKE_CURRENT_SOURCE_DIR}/imgui)
set(LIB_SOURCE
    ${IMGUI_SRC_PATH}/imgui_widgets.cpp
    ${IMGUI_SRC_PATH}/imgui_draw.cpp
    ${IMGUI_SRC_PATH}/imgui.cpp
    ${IMGUI_SRC_PATH}/imgui_demo.cpp
    ${IMGUI_SRC_PATH}/imgui_tables.cpp
    ${IMGUI_SRC_PATH}/backends/imgui_impl_opengl3.cpp
    ${IMGUI_SRC_PATH}/backends/imgui_impl_glfw.cpp
    )

set(LIB_HEADERS
    ${IMGUI_SRC_PATH}/
    ${IMGUI_SRC_PATH}/backends
    )

add_library (imgui ${LIB_SOURCE})
target_include_directories(imgui PUBLIC ${LIB_HEADERS})
target_include_directories(imgui PUBLIC ${GLEW_INCLUDE_DIRS} ${OpenGL_INCLUDE_DIRS})
target_link_libraries(imgui PUBLIC glfw ${OPENGL_LIBRARY} ${CMAKE_DL_LIBS})
# end of imgui build

if (NOT ${TEST_ENGINE})
    set(LINK_LIBS engine imgui)
    add_subdirectory(game)
endif()
