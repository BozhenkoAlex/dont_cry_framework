#version 150

varying vec3 v_posW;
varying vec3 v_normalW;
varying vec4 v_clipSpace;

// refrafction, reflection, depth, displacement, normalMap  
uniform sampler2D u_s_textures[5];

uniform vec3 u_cameraPos; 
uniform float u_time;

uniform float u_tilling;
uniform float u_fresnelPower;
uniform float u_waveStrength;
uniform float u_shineDamper;
uniform float u_reflectivity;
uniform float u_waterDumpDepth;

uniform vec3 u_waterColour;

uniform vec3 u_lightColour = vec3(1.0);
uniform vec3 u_lightPosition = vec3(21.0, 50.0, -25.0);

uniform float u_near;
uniform float u_far;

uniform float u_start;
uniform float u_range;
uniform vec3 u_fogColor;

void main()
{
	vec2 ndc = ( v_clipSpace.xy / v_clipSpace.w ) * 0.5 + 0.5;

	float depth = texture2D(u_s_textures[2], ndc).r;
	float floorDistance = 2.0 * u_near * u_far / (u_far + u_near - (2.0 * depth - 1.0) * (u_far - u_near));

	depth = gl_FragCoord.z;
	float waterDistance = 2.0 * u_near * u_far / (u_far + u_near - (2.0 * depth - 1.0) * (u_far - u_near));
	float waterDepth = floorDistance - waterDistance;
	float waterDepthFactor = clamp(waterDepth / (u_waterDumpDepth), 0.0, 1.0);

	vec2 uv = v_posW.xz / u_tilling;
	float offset = u_time / u_tilling;
	vec2 distortedTexCoord = texture2D(u_s_textures[3], vec2(uv.x + offset, uv.y)).rg;
	distortedTexCoord = uv + vec2(distortedTexCoord.x, distortedTexCoord.y + offset);
	float distortionDepthFactor = clamp(0.25 * waterDepth / u_waterDumpDepth, 0.0, 1.0) ;
	vec2 totalDistortion = ndc + distortionDepthFactor * u_waveStrength * (texture2D(u_s_textures[3], vec2(distortedTexCoord.x + offset, distortedTexCoord.y)).rg * 2.0 - 1.0);
	totalDistortion = clamp(totalDistortion, 0.001, 0.999);

	vec4 normalMapColor = texture2D(u_s_textures[4], distortedTexCoord);
	vec3 normal = vec3(2.0 * normalMapColor.r - 1.0, normalMapColor.b, 2.0 * normalMapColor.g - 1.0);
	normal = normalize(normal);
	
	vec3 viewDir = normalize(u_cameraPos - v_posW);
	vec3 lightDir = v_posW - u_lightPosition;
	vec3 reflectedLight = reflect(normalize(lightDir), normal);
	float specular = dot(reflectedLight, viewDir);
	specular = max(pow(specular, u_shineDamper), 0.0);
	vec3 specularHighlights = u_lightColour * specular * u_reflectivity * waterDepthFactor;
	
	float reflection = dot(viewDir, v_normalW);
	reflection = pow(reflection, u_fresnelPower);
	reflection = clamp(reflection, 0.0, 1.0);

	vec4 reflectColour = texture2D(u_s_textures[0], vec2(totalDistortion.x, -totalDistortion.y));
	vec4 refractColour = texture2D(u_s_textures[1], totalDistortion);

	reflectColour += vec4(specularHighlights, 0.0);
	vec4 resultColor = mix(reflectColour, refractColour, reflection);
	resultColor = mix(resultColor, vec4(u_waterColour, 1.0), 0.2);
	resultColor.a = waterDepthFactor;
	gl_FragColor = resultColor;

	float distanceToCamera = length(u_cameraPos - v_posW);
	float lerpValue = clamp( ( distanceToCamera - u_start ) / u_range, 0.0, 1.0 );
	gl_FragColor = mix(resultColor, vec4(u_fogColor, 1.0), lerpValue);
}