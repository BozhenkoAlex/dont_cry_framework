#version 150

precision mediump float;

varying vec2 v_uv;
varying vec3 v_posW;
varying vec3 v_normalW;
varying vec4 v_shadowCoord;

// blending, and 3 color texture
uniform sampler2D u_s_textures[4];
uniform float u_tiling_factor;

uniform vec3 u_cameraPos;

// fog params
uniform float u_start;
uniform float u_range;
uniform vec3 u_fogColor;

// lights params
struct DirectionLight
{
    vec3 color;
    vec3 direction;
    sampler2D shadow;
};

const int lightsNum = 1;
uniform DirectionLight u_directionLights[lightsNum];
uniform sampler2D u_s_shadowTextures[lightsNum];

uniform float u_ambientWeight = 0.1;
uniform vec3 u_ambientColor = vec3(1.);
uniform float u_specular_power = 64.0;

vec3 calcLight(vec3 viewDir, vec3 normal, DirectionLight light);
float shadowCalculation(sampler2D shadowMap, vec4 fragPosLightSpace, vec3 lightDir);

void main()
{
    vec4 BT = texture2D(u_s_textures[0], v_uv);
    vec4 Tex1 = texture2D(u_s_textures[1], v_uv * u_tiling_factor);
    vec4 Tex2 = texture2D(u_s_textures[2], v_uv * u_tiling_factor);
    vec4 Tex3 = texture2D(u_s_textures[3], v_uv * u_tiling_factor);

    vec4 texelColor = ( BT.r * Tex1 + BT.g * Tex2 + BT.b * Tex3 ) / ( BT.r + BT.g + BT.b );

    vec3 viewDir = normalize(u_cameraPos - v_posW);
    vec3 resultLightColor = vec3(0.0);
    float koef = 1.0 / float(lightsNum);

    // FIXME: fix shadows on linux
    // for(int i = 0; i<lightsNum; ++i)
    {
        const int i = 0;
        resultLightColor += koef * calcLight(viewDir, v_normalW, u_directionLights[i]);
    }

    vec3 resultColor = resultLightColor * texelColor.rgb;

    float distanceToCamera = length(u_cameraPos - v_posW);
    float lerpValue = clamp( ( distanceToCamera - u_start ) / u_range, 0.0, 1.0 );
    gl_FragColor = vec4(mix(resultColor, u_fogColor, lerpValue), 1.0);
}

vec3 calcLight(vec3 viewDir, vec3 normal, DirectionLight light)
{
    // diffuse color
    float diff = max(dot(normal, light.direction), 0.0);
    vec3 diffuse = diff * light.color;

    // specular color
    vec3 reflectDir = reflect(-light.direction, normal);

    float spec = pow(max(dot(viewDir, reflectDir), 0.0), u_specular_power);
    vec3 specular = spec * light.color;

    // TODO: enable by SHADOW_TEST
    float visibility = 1.0; //shadowCalculation(light.shadowMap, v_shadowCoord, light.direction);

    return u_ambientWeight * u_ambientColor + visibility * ( diffuse + specular );
}

float shadowCalculation(sampler2D shadowMap, vec4 fragPosLightSpace, vec3 lightDir)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture2D(shadowMap, projCoords.xy).r;
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;

    // check whether current frag pos is in shadow
    float bias = max(0.01 * (1.0 - dot(v_normalW, lightDir)), 0.001);
    float shadow = currentDepth - bias < closestDepth  ? 1.0 : 0.0;
    return shadow;
}
