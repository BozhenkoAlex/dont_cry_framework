#version 150

attribute vec3 a_posL;
attribute vec3 a_norm;
attribute vec2 a_uv;

varying vec2 v_uv;
varying vec3 v_posW;
varying vec3 v_normalW;

uniform mat4 u_viewProjection;
uniform mat4 u_model;

void main()
{
	gl_Position = vec4(a_posL, 1.0) * u_viewProjection;

	mat3 modelMatrix = mat3(u_model);
	v_posW =  ( vec4( a_posL, 1.0 ) * u_model ).xyz;
	v_normalW  = normalize( a_norm * modelMatrix );
	v_uv = a_uv;
}
