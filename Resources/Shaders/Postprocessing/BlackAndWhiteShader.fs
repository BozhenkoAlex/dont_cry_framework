#version 150

precision highp float;

varying vec2 v_uv;

uniform sampler2D u_s_textures[1];

void main() 
{
	vec3 color = texture2D(u_s_textures[0], v_uv).rgb;
	vec3 blackAndWhite = vec3(0.3 * color.r + 0.59 * color.g + 0.11 * color.b);
	gl_FragColor =  vec4( blackAndWhite, 1. );
}
