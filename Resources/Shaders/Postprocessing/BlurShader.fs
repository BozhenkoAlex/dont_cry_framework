#version 150

precision mediump float;

uniform sampler2D u_s_textures[1];

varying vec2 v_uv;

uniform vec4 u_offset;

vec3 getTextureColor(float offsetX, float offsetY)
{
	return texture2D(u_s_textures[0], vec2(v_uv.x + offsetX, v_uv.y + offsetY)).rgb;
}

vec3 getBlur()
{
	vec3 color1 = getTextureColor(0., 0.);
	vec3 color2 = getTextureColor(u_offset.x, 0.);
	vec3 color3 = getTextureColor(-u_offset.x, 0.);
	vec3 color4 = getTextureColor(0., u_offset.y);
	vec3 color5 = getTextureColor(0., -u_offset.y);
	vec3 color6 = getTextureColor(u_offset.z, u_offset.w);
	vec3 color7 = getTextureColor(-u_offset.z, u_offset.w);
	vec3 color8 = getTextureColor(-u_offset.z, -u_offset.w);
	vec3 color9 = getTextureColor(u_offset.z, -u_offset.w);
	return (color1 * 2.0 + color2 + color3 + color4 + color5 + color6 + color7 + color8 + color9) * 0.1;
}

void main(void)
{
	gl_FragColor = vec4(getBlur(), 1.0);
}