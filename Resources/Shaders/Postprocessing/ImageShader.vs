#version 150

attribute vec3 a_posL;

varying vec2 v_uv;

void main()
{
	gl_Position = vec4(a_posL.xy, 0.0, 1.0);
	v_uv = 0.5 * ( a_posL.xy + 1.0 );
}