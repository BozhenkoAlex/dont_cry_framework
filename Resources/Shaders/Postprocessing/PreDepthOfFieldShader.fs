#version 150

precision mediump float;

// color, depth
uniform sampler2D u_s_textures[2];

varying vec2 v_uv;

uniform vec4 u_offset;

uniform float u_far;
uniform float u_near;

uniform float u_fade;
uniform float u_clarity;

float calculateWeight(vec2 uv)
{
	float depth = texture2D(u_s_textures[1], uv).x;
	float z = -u_far * u_near / ( depth * ( u_far - u_near ) - u_far );
	float k = clamp(abs(z - u_clarity) / u_fade, 0.0, 1.0);
	return k;
}

vec4 getTextureColor(vec2 offset)
{
	return texture2D(u_s_textures[0], vec2(v_uv + offset));
}

vec4 getBlur()
{
	vec2 offsets[9];
	offsets[0] = vec2( 0., 0. );
	offsets[1] = vec2( u_offset.x, 0. );
	offsets[2] = vec2( -u_offset.x, 0. );
	offsets[3] = vec2( 0., u_offset.y );
	offsets[4] = vec2( 0., -u_offset.y );
	offsets[5] = vec2( u_offset.z, u_offset.w );
	offsets[6] = vec2(-u_offset.z, u_offset.w );
	offsets[7] = vec2( -u_offset.z, -u_offset.w );
	offsets[8] = vec2( u_offset.z, -u_offset.w );
	
	vec4 result = 2.0 * getTextureColor(offsets[0]);
	float resultWeight = 2.0;

	for( int i = 1; i<9; ++i)
	{
		float weight = calculateWeight(offsets[i]);
		result += weight * getTextureColor(offsets[i]);
		resultWeight += weight;
	}

	return result / resultWeight;
}

void main(void)
{
	vec4 colorBlur = getBlur();
	//vec4 colorOriginal = getTextureColor(vec2(0.0));
	gl_FragColor = colorBlur;
}

