#version 150

precision highp float;

varying vec2 v_uv;

uniform sampler2D u_s_textures[1];

void main() 
{
	gl_FragColor = texture2D(u_s_textures[0], v_uv );
}