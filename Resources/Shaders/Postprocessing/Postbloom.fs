#version 150

precision mediump float;

// ColorBuffer, BlurredCBuffer
uniform sampler2D u_s_textures[2];

varying vec2 v_uv;

uniform float u_bloomWeight;

void main(void)
{
	vec4 color = texture2D(u_s_textures[0], v_uv);
	vec4 blur = texture2D(u_s_textures[1], v_uv);
	gl_FragColor = color + u_bloomWeight * blur;
}