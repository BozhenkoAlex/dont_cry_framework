#version 150

precision mediump float;

uniform vec3 u_outputColor = vec3(242.0, 153, 0.0) / 255.0;

void main()
{
    gl_FragColor = vec4(u_outputColor, 1.0);
}
