#version 150

precision mediump float;

varying vec2 v_uv;
varying vec3 v_posW;

uniform vec3 u_cameraPos; 

// displacement, color, alpha 
uniform sampler2D u_s_textures[3];

uniform float u_time;

uniform float u_dMax;

uniform float u_start;
uniform float u_range;
uniform vec3 u_fogColor;

void main()
{
	vec2 disp = texture2D(u_s_textures[0], vec2(v_uv.x, v_uv.y + u_time)).xy;

	vec2 offset = (2.0 * disp - 1.0) * u_dMax;

	vec2 uvDisplaced = v_uv + offset;
	vec4 fireColor = texture2D (u_s_textures[1], uvDisplaced);

	vec4 alphaValue = texture2D(u_s_textures[2], v_uv);

	vec4 resultColor = fireColor * vec4( vec3(1.0), alphaValue.r);
	if( step(0.05, resultColor.a) == 0.0 )
	{
		discard;
	}

	float distanceToCamera = length(u_cameraPos - v_posW);
	float lerpValue = clamp( ( distanceToCamera - u_start ) / u_range, 0.0, 1.0 );
	gl_FragColor = mix(resultColor, vec4(u_fogColor, 1.0), lerpValue);
}
