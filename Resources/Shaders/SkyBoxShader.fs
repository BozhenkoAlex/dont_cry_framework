#version 150
#extension GL_NV_shadow_samplers_cube : enable

precision mediump float;

varying vec3 v_posW;

uniform samplerCube u_s_cubeTexture;
uniform vec3 u_fogColor;

uniform float u_lowerLimit;
uniform float u_upperLimit;

void main()
{
    vec4 textureColor = textureCube(u_s_cubeTexture, v_posW);

	float val = clamp(v_posW.y, u_lowerLimit, u_upperLimit);
	float factor = val / u_upperLimit;
	gl_FragColor = mix(vec4(u_fogColor, 1.0), textureColor, factor);
}
