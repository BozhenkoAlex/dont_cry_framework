#version 150

attribute vec3 a_posL;
attribute vec3 a_norm;
attribute vec2 a_uv;

varying vec2 v_uv;
varying vec3 v_posW;
varying vec3 v_normalW;
varying vec4 v_clipSpace;
varying vec4 v_shadowCoord;

uniform mat4 u_viewProjection;
uniform mat4 u_model;
uniform mat4 u_shadowMapMatrix;

uniform vec4 u_clipPlane = vec4(0.0, 0.0, 0.0, 0.0);

uniform float u_shift = 0.0;

void main()
{
    vec4 posL = vec4(a_posL, 1.0) * ( u_model * u_viewProjection );
    v_clipSpace = posL;

    v_uv = a_uv;

    vec4 worldPosition = vec4( a_posL, 1.0 ) * u_model;
    v_posW = worldPosition.xyz;

    v_normalW = normalize( (vec4(a_norm, 0.0 ) * u_model).xyz );

    vec3 borderShift = v_normalW * u_shift;
    gl_Position = vec4(v_posW + borderShift, 1.0) * u_viewProjection;
    v_shadowCoord = vec4(v_posW, 1.0) * u_shadowMapMatrix;

    gl_ClipDistance[0] = dot(worldPosition, u_clipPlane);
}
 
