#version 150

precision highp float;

const int MAX_BONES_COUNT = 6;
const int MAX_BONES = 100;

attribute vec3 a_posL;
attribute vec3 a_norm;
attribute int a_boneIDs[MAX_BONES_COUNT];
attribute float a_weights[MAX_BONES_COUNT];
attribute vec2 a_uv;


varying vec2 v_uv;
varying vec3 v_posW;
varying vec3 v_normalW;
varying vec4 v_shadowCoord;

uniform mat4 u_viewProjection;
uniform mat4 u_model;
uniform mat4 u_shadowMapMatrix;
uniform mat4 gBones[MAX_BONES];

uniform vec4 u_clipPlane = vec4(0.0, -1.0, 0.0, 1.0);

uniform float u_shift = 0.0;

mat4 getBoneTransform()
{
    mat4 boneTransform = gBones[a_boneIDs[0]] * a_weights[0];
    for( int i = 1; i < MAX_BONES_COUNT; ++i)
    {
        if(isnan(a_weights[i]))
            break;

        boneTransform += gBones[a_boneIDs[i]] * a_weights[i];
    }
    return boneTransform;
}

void main()
{
    mat4 boneTransform = getBoneTransform();

    vec4 posL = (boneTransform * vec4(a_posL, 1.0));
    vec4 normalL = (boneTransform * vec4(a_norm, 0.0));

    vec4 worldPosition = posL * u_model;
    v_uv = a_uv;
    v_posW = worldPosition.xyz;
    v_normalW = normalize(( normalL * u_model ).xyz);

    gl_Position = vec4(v_posW + v_normalW * u_shift, 1.0) * u_viewProjection;
    v_shadowCoord = vec4(v_posW, 1.0) * u_shadowMapMatrix;

    gl_ClipDistance[0] = dot(worldPosition, u_clipPlane);
}
