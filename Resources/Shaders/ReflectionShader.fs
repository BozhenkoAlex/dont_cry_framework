#version 150
#extension GL_NV_shadow_samplers_cube : enable

precision mediump float;

varying vec3 v_normalW;
varying vec3 v_posW;

uniform samplerCube u_s_cubeTexture;

uniform vec3 u_cameraPos; 

uniform float u_start;
uniform float u_range;
uniform vec3 u_fogColor;

void main()
{
    vec3 toEye = u_cameraPos - v_posW;
    vec3 reflectDir = reflect(normalize(-toEye), normalize(v_normalW));
    vec4 resultColor = textureCube(u_s_cubeTexture, reflectDir);
	

	float distanceToCamera = length(toEye);
	float lerpValue = clamp( ( distanceToCamera - u_start ) / u_range, 0.0, 1.0 );
	gl_FragColor = mix(resultColor, vec4(u_fogColor, 1.0), lerpValue);
}