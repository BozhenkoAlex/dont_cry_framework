#include "SceneObjectEditor.h"

#include <string>

#include "ISceneObject.h"

SceneObjectEditor::SceneObjectEditor()
    :
    m_currentObject(nullptr)
    , m_positionChangedCallback(nullptr)
    , m_rotationChangedCallback(nullptr)
    , m_scaleChangedCallback(nullptr)
{
}

void SceneObjectEditor::setObject(ISceneObject* object)
{
    m_currentObject = object;
    if (m_currentObject)
    {
        if (m_positionChangedCallback)
        {
            auto vec = m_currentObject->getPosition();
            const float* floatArray = &(vec.x);
            m_positionChangedCallback(floatArray);
        }

        if (m_rotationChangedCallback)
        {
            auto vec = glm::degrees(m_currentObject->getRotation());
            const float* floatArray = &(vec.x);
            m_rotationChangedCallback(floatArray);
        }

        if (m_scaleChangedCallback)
        {
            auto vec = m_currentObject->getScale();
            const float* floatArray = &(vec.x);
            m_scaleChangedCallback(floatArray);
        }

        if (m_modelChangedCallback)
        {
            auto model = m_currentObject->getModel();
            auto modelId = getModelId(model);
            if (modelId.has_value())
            {
                m_modelChangedCallback(modelId.value());
            }
        }
    }
}

void SceneObjectEditor::changeModel(int32_t index)
{
    bool indexOutOfRange = index < 0 || index >= m_modelList.size();
    if (!indexOutOfRange && m_currentObject)
    {
        m_currentObject->setModel(m_modelList[index]);
    }
}

std::vector<std::string>
SceneObjectEditor::getModelListNames() const
{
    std::vector<std::string> result(m_modelList.size());
    for (int i = 0; i < result.size(); ++i)
    {
        result[i] = std::to_string(i);
    }
    return result;
}

std::optional<int32_t> SceneObjectEditor::getModelId(const Model* model)
{
    auto iter = std::find(std::begin(m_modelList), std::end(m_modelList), model);
    return iter != m_modelList.end()
               ? std::distance(m_modelList.begin(), iter) : std::optional<int32_t>();
}

void SceneObjectEditor::setPosition(const glm::vec3& pos)
{
    if (m_currentObject)
    {
        m_currentObject->setPosition(pos);
        if (m_positionChangedCallback)
        {
            m_positionChangedCallback(&pos.x);
        }
    }
}

void SceneObjectEditor::setRotation(const glm::vec3& rotation)
{
    if (m_currentObject)
    {
        m_currentObject->setRotation(glm::radians(rotation));
        if (m_rotationChangedCallback)
        {
            m_rotationChangedCallback(&rotation.x);
        }
    }
}

void SceneObjectEditor::setScale(const glm::vec3& scale)
{
    if (m_currentObject)
    {
        m_currentObject->setScale(scale);
        if (m_scaleChangedCallback)
        {
            m_scaleChangedCallback(&scale.x);
        }
    }
}

glm::vec3 SceneObjectEditor::getScale() const
{
    return m_currentObject->getScale();
}

glm::vec3 SceneObjectEditor::getRotation() const
{
    return glm::degrees(m_currentObject->getRotation());
}

glm::vec3 SceneObjectEditor::getPosition() const
{
    return m_currentObject->getPosition();
}

void SceneObjectEditor::moveObject(const glm::vec3 &pos)
{
    if (m_currentObject)
    {
        m_currentObject->moveTo(pos);
        // TODO: update values during movement
        if (m_positionChangedCallback)
        {
            m_positionChangedCallback(&pos.x);
        }
    }
}
