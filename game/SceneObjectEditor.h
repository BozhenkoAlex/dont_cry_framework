#pragma once

#include <functional>
#include <optional>
#include <string>
#include <vector>

#include <glm/glm.hpp>

class ISceneObject;
class Model;

using VecChangedCallback = std::function<void(const float[3])>;

class SceneObjectEditor
{
public:
    SceneObjectEditor();
    void setObject(ISceneObject* object);

    bool hasObject() const
    {
        return nullptr != m_currentObject;
    }

    void changeModel(int32_t index);
    // TODO: take in account amimated models
    void setModelList(const std::vector<const Model*> &modelList)
    {
        m_modelList = modelList;
    }

    std::vector<std::string> getModelListNames() const;
    std::optional<int32_t> getModelId(const Model* model);

    glm::vec3 getPosition() const;
    glm::vec3 getRotation() const;
    glm::vec3 getScale() const;
    void setPosition(const glm::vec3 &pos);
    void setRotation(const glm::vec3 &rotation);
    void setScale(const glm::vec3 &scaleVec);

    void setPositionChangedCallback(const VecChangedCallback& callback)
    {
        m_positionChangedCallback = callback;
    }

    void setRotationChangedCallback(const VecChangedCallback& callback)
    {
        m_rotationChangedCallback = callback;
    }

    void setScaleChangedCallback(const VecChangedCallback& callback)
    {
        m_scaleChangedCallback = callback;
    }

    void setModelChangedCallback(const std::function<void(int32_t)>& callback)
    {
        m_modelChangedCallback = callback;
    }

    void moveObject(const glm::vec3 &pos);

protected:
    ISceneObject* m_currentObject;

    VecChangedCallback m_positionChangedCallback;
    VecChangedCallback m_rotationChangedCallback;
    VecChangedCallback m_scaleChangedCallback;

    std::function<void(int32_t)> m_modelChangedCallback;

    std::vector<const Model*> m_modelList;
};
