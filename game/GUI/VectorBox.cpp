#include "VectorBox.h"

#include "imgui.h"

VectorBox::VectorBox(const std::string& title)
	: m_valuesChangedCallback(nullptr)
{
	m_title = title;
	m_values[0] = m_values[1] = m_values[2] = 0.0f;

	setRange(0.0f, 1.0f);
}

void VectorBox::setTitle(const std::string& title)
{
	m_title = title;
}

void VectorBox::setRange(float min, float max)
{
	m_min = min;
	m_max = max;

	m_speed = 0.01f * (m_max - m_min);
}

void VectorBox::setSpeed(float speed)
{
	m_speed = speed;
}

void VectorBox::setValues(const float values[3])
{
	m_values[0] = values[0];
	m_values[1] = values[1];
	m_values[2] = values[2];
}

void VectorBox::setValuesChangedCallback(std::function<void(const float[3])>& callback)
{
	m_valuesChangedCallback = callback;
}

void VectorBox::draw()
{
	bool changed = ImGui::DragFloat3(m_title.c_str(), m_values, m_speed, m_min, m_max);
	if (changed && m_valuesChangedCallback)
	{
		m_valuesChangedCallback(m_values);
	}
}
