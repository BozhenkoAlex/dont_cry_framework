#pragma once

#include <memory>
#include <chrono>

#include "GlfwWindowWrapper.h"

#include "GUI/Widget.h"
#include "GUI/Canvas.h"

class MainWindow
{
public:
	MainWindow();

	~MainWindow();

	void exec();


protected:

	void bindWindowCallbacks();

	void bindSelectedObjectEditingCallbacks();

	void bindObjectsListEditingCallbacks();

protected:

	void mouseButtonCallback(GLFWwindow* win, int button, int pressed, int mode);

	void drawCallback();

protected:

	std::chrono::time_point<std::chrono::steady_clock> m_lastTimePoint;

#ifndef SANDBOX 
	std::unique_ptr<Canvas> m_openglCanvas;
#endif

	std::unique_ptr<GlfwWindowWrapper> m_glfwWindowWrapper;
	std::unique_ptr<Widget> m_mainWidget;
};
