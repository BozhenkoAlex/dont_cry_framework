#include "ComboBox.h"

#include "imgui.h"

ComboBox::ComboBox()
{
	m_currentIndex = 0;
}

void ComboBox::draw()
{
	static auto getter = [](void* data, int idx, const char** out_text)
	{
		auto vectorStrings = (const std::vector<std::string>*)data;
		*out_text = (*vectorStrings)[idx].c_str();
		return true;
	};

	bool indexChanged = ImGui::Combo("Model", &m_currentIndex, getter, (void*)&m_itemsList, m_itemsList.size(), -1);
    if (indexChanged && m_indexChangedCallback)
	{
		m_indexChangedCallback(m_currentIndex);
	}
}

void ComboBox::setIndex(int32_t index)
{
	m_currentIndex = index;
}

void ComboBox::setList(const std::vector<std::string>& list)
{
	m_itemsList = list;
}

void ComboBox::setIndexChangedCallback(const std::function<void(int32_t)>& indexChangedCallback)
{
	m_indexChangedCallback = indexChangedCallback;
}
