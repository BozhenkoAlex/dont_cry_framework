#pragma once

#include <vector>
#include <optional>
#include <memory>

class IEngine;
class SceneObjectEditor;

class Canvas
{
public:
	Canvas(int32_t width, int32_t height);

    ~Canvas();
	
	void draw(int32_t selectedObjectId);

	void updateTime(float delta);

	void processKey(int32_t key, bool isPressed);

	void resize(int32_t width, int32_t height);

    std::optional<size_t> selectObject(bool pressed, double posX, double posY);

	std::vector<int32_t> getObjectsList();

	void selectObject(int32_t index);

    void mouseMoved(double posX, double posY);

    void moveSelectedObject(double posX, double posY);

    SceneObjectEditor* getSceneObjectEditor()
	{
		return m_sceneObjectEditor.get();
	}

protected:

    std::unique_ptr<IEngine> engine;
	std::unique_ptr<SceneObjectEditor> m_sceneObjectEditor;

    int32_t m_width;
    int32_t m_height;
};
