#include "MainWindow.h"

#include <functional>

#define GLEW_STATIC
#include <gl/glew.h>
#include <GLFW/glfw3.h>

#include "SceneObjectEditor.h"

#include "GUI/ObjectEditorWidget.h"
#include "GUI/ListWidget.h"

const int32_t DEFAULT_WIDTH = 1280;
const int32_t DEFAULT_HEIGHT = 720;

enum MouseButtons : int8_t
{
	LEFT = 0,
	RIGHT,
	COUNT
};

MainWindow::MainWindow()
{
    m_glfwWindowWrapper = std::make_unique<GlfwWindowWrapper>(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    /* OpenGL */
    glViewport(0, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    glewExperimental = 1;
    if (glewInit() != GLEW_OK)
    {
        fprintf(stderr, "Failed to setup GLEW\n");
        exit(1);
    }

    m_openglCanvas = std::make_unique<Canvas>(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    auto win = m_glfwWindowWrapper->handler();
    m_mainWidget = std::make_unique<Widget>(win);
    m_mainWidget->resize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    bindWindowCallbacks();

    bindSelectedObjectEditingCallbacks();
    bindObjectsListEditingCallbacks();

    m_lastTimePoint = std::chrono::steady_clock::now();
}

MainWindow::~MainWindow()
{
}

void MainWindow::exec()
{
	m_glfwWindowWrapper->mainLoop();
}

void MainWindow::bindWindowCallbacks()
{
	std::function<void(GLFWwindow*, int32_t, int32_t)> resizeFunc = [this](GLFWwindow*, int32_t width, int32_t height)
	{
		m_openglCanvas->resize(width, height);
		m_mainWidget->resize(width, height);
	};
	m_glfwWindowWrapper->setResizeFunction(resizeFunc);

	std::function<void(GLFWwindow*, int, int, int, int)> keyFunc = [this](GLFWwindow*, int key, int scancode, int action, int mode)
	{
        m_openglCanvas->processKey(key, action);
		m_mainWidget->imguiKeyCallback(key, scancode, action, mode);
	};
	m_glfwWindowWrapper->setKeyFunction(keyFunc);

    {
		using namespace std::placeholders;
		std::function<void(GLFWwindow*, int, int, int)> callback = std::bind(&MainWindow::mouseButtonCallback, this, _1, _2, _3, _4);
		m_glfwWindowWrapper->setMouseButtonFunction(callback);
	}

    std::function<void()> drawFunc = std::bind(&MainWindow::drawCallback, this);
    m_glfwWindowWrapper->setDrawFunction(drawFunc);

	m_mainWidget->bindImguiCallbacks();
}

void MainWindow::bindSelectedObjectEditingCallbacks()
{
	auto objectEditor = m_openglCanvas->getSceneObjectEditor();
	
	auto objectEditorForm = m_mainWidget->getObjectEditorForm();

	{
		VecChangedCallback callback = [objectEditor](const float vec[3])
		{
			objectEditor->setPosition(glm::vec3(vec[0], vec[1], vec[2]));
		};
		objectEditorForm->setPositionChangedCallback(callback);
	}

	{
		VecChangedCallback callback = [objectEditor](const float vec[3])
		{
			objectEditor->setRotation(glm::vec3(vec[0], vec[1], vec[2]));
		};
		objectEditorForm->setRotationChangedCallback(callback);
	}

	{
		VecChangedCallback callback = [objectEditor](const float vec[3])
		{
			objectEditor->setScale(glm::vec3(vec[0], vec[1], vec[2]));
		};
		objectEditorForm->setScaleChangedCallback(callback);
	}

	{
		VecChangedCallback callback = [objectEditorForm](const float vec[3])
		{
			objectEditorForm->setPosition(vec);
		};
		objectEditor->setPositionChangedCallback(callback);
	}

	{
		VecChangedCallback callback = [objectEditorForm](const float vec[3])
		{
			objectEditorForm->setRotation(vec);
		};
		objectEditor->setRotationChangedCallback(callback);
	}

	{
		VecChangedCallback callback = [objectEditorForm](const float vec[3])
		{
			objectEditorForm->setScale(vec);
		};
		objectEditor->setScaleChangedCallback(callback);
	}

	{
		std::function<void(int32_t)> callback = [objectEditor](int32_t index)
		{
            objectEditor->changeModel(index);
		};
		objectEditorForm->setModelChangedCallback(callback);
	}

	{
		std::function<void(int32_t)> callback = [objectEditorForm](int32_t index)
		{
			objectEditorForm->setModelIndex(index);
		};
		objectEditor->setModelChangedCallback(callback);
	}

    // TODO: consider animated models
    auto modelList = objectEditor->getModelListNames();
	objectEditorForm->setModelsList(modelList);
}

void MainWindow::bindObjectsListEditingCallbacks()
{
	auto sceneObjectsList = m_mainWidget->getObjectsListForm();

	auto objectList = m_openglCanvas->getObjectsList();
	for (const auto& id : objectList)
	{
		sceneObjectsList->addItem(std::to_string(id));
	}

	{
        std::function<void(int32_t)> callback = [this](int32_t index)
        {
            m_openglCanvas->selectObject(index);
        };
        sceneObjectsList->setItemChengedCallback(callback);
	}
}

void MainWindow::mouseButtonCallback(GLFWwindow* win, int button, int pressed, int mode)
{
	m_mainWidget->imguiMouseButtonCallback(button, pressed, mode);

	double posX, posY;
    glfwGetCursorPos(win, &posX, &posY);

	bool isWidgetClick = m_mainWidget->posOnWidget(posX, posY);
	if (MouseButtons::LEFT == button)
	{
		if (!isWidgetClick && pressed)
		{
			auto index = m_openglCanvas->selectObject(pressed, posX, posY);
            // TODO: magic number and direct call to object list
			auto sceneObjectsList = m_mainWidget->getObjectsListForm();
            sceneObjectsList->setCurrentIndex(index.value_or(-1));
        }
    } else if (MouseButtons::RIGHT == button)
    {
        if (!isWidgetClick && pressed)
        {
            m_openglCanvas->moveSelectedObject(posX, posY);
        }
    }
}

void MainWindow::drawCallback()
{
	auto timePoint = std::chrono::steady_clock::now();
	float deltaTime = std::chrono::duration<float>(timePoint - m_lastTimePoint).count();
	m_lastTimePoint = timePoint;

	m_mainWidget->drawGUI();

	auto objectsList = m_mainWidget->getObjectsListForm();
    // TODO: set it to canvas by callback when changed
	auto selectedObjectId = objectsList->getCurrentIndex();

    m_openglCanvas->updateTime(deltaTime);
    m_openglCanvas->draw(selectedObjectId);

	m_mainWidget->render();
}
