#pragma once

#include <vector>
#include <string>
#include <functional>


class ComboBox
{
public:

	ComboBox();

	void draw();

	void setIndex(int32_t index);

	void setList(const std::vector<std::string>& list);

	void setIndexChangedCallback(const std::function<void(int32_t)>& indexChangedCallback);

protected:
	std::vector<std::string> m_itemsList;
	int32_t m_currentIndex;

	std::function<void(int32_t)> m_indexChangedCallback;
};
