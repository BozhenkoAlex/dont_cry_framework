#pragma once

#include <memory>
#include <array>
#include <functional>
#include <string>

class VectorBox;
class ComboBox;

using VecChangedFunc = std::function<void(const float[3])>;
class ObjectEditorWidget
{
public:

	ObjectEditorWidget();

	~ObjectEditorWidget();

	void setPositionChangedCallback(VecChangedFunc& callback);
	void setRotationChangedCallback(VecChangedFunc& callback);
	void setScaleChangedCallback(VecChangedFunc& callback);

	void setPosition(const float vec[3]);
	void setRotation(const float vec[3]);
	void setScale(const float vec[3]);
	
	void draw();

	void setModelChangedCallback(const std::function<void(int32_t)>& indexChangedCallback);
	void setModelsList(const std::vector<std::string>& modelNames);
	void setModelIndex(int32_t index);

protected:

	std::unique_ptr<ComboBox> m_modelsCombobox;

	std::unique_ptr<VectorBox> m_positionBox;
	std::unique_ptr<VectorBox> m_rotationBox;
	std::unique_ptr<VectorBox> m_scaleBox;
};
