#pragma once

#include <vector>
#include <string>
#include <functional>


class ListWidget
{
public:
	ListWidget();

	~ListWidget();

	void draw();

	void addItem(const std::string& item);

	bool hasSelectedItem();

	void setItemChengedCallback(std::function<void(int32_t)>& callback);

	void setCurrentIndex(int32_t index);

	int32_t getCurrentIndex();

protected:

	int32_t m_currentIndex;
	std::vector<std::string> m_items;
	std::function<void(int32_t)> m_itemChengedCallback;
};
