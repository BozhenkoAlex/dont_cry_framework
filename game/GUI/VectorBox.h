#pragma once

#include <vector>
#include <string>
#include <functional>


class VectorBox
{
public:
	VectorBox(const std::string& title = "");

	void setTitle(const std::string& title);

	void setRange(float min, float max);

	void setSpeed(float speed);

	void setValues(const float values[3]);

	void setValuesChangedCallback(std::function<void(const float[3])>& callback);

	void draw();

protected:
	float m_min;
	float m_max;
	float m_speed;

	std::string m_title;
	float m_values[3];

	std::function<void(const float[3])> m_valuesChangedCallback;
};
