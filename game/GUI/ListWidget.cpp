#include "ListWidget.h"

#include "imgui.h"

static const int32_t DEFAULT_INDEX = -1;

ListWidget::ListWidget()
	: m_currentIndex(DEFAULT_INDEX)
{
}

ListWidget::~ListWidget()
{
}

void ListWidget::draw()
{
    static const size_t elementsShow = 5;
    if (ImGui::BeginListBox("##objects listbox", ImVec2(-FLT_MIN, elementsShow * ImGui::GetTextLineHeightWithSpacing())))
    {
        for (size_t i = 0; i < m_items.size(); ++i)
        {
            bool isSelected = i == m_currentIndex;
            if (ImGui::Selectable(m_items[i].c_str(), isSelected))
            {
                m_currentIndex = i;
                if (m_itemChengedCallback)
                {
                    m_itemChengedCallback(m_currentIndex);
                }
            }

            // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
            if (m_currentIndex)
                ImGui::SetItemDefaultFocus();
        }
        ImGui::EndListBox();
    }
}


bool ListWidget::hasSelectedItem()
{
	return m_currentIndex != DEFAULT_INDEX;
}

void ListWidget::addItem(const std::string& item)
{
	m_items.emplace_back(item);
}

void ListWidget::setItemChengedCallback(std::function<void(int32_t)>& callback)
{
	m_itemChengedCallback = callback;
}

void ListWidget::setCurrentIndex(int32_t index)
{
	m_currentIndex = index;
}

int32_t ListWidget::getCurrentIndex()
{
	return m_currentIndex;
}
