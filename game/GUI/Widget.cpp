#include "Widget.h"


#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"


#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "GUI/ListWidget.h"
#include "GUI/ObjectEditorWidget.h"

Widget::Widget(GLFWwindow* win)
	: m_window(win)
{
	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

	// Setup Dear ImGui style
	//ImGui::StyleColorsDark();
	//ImGui::StyleColorsLight();
	ImGui::StyleColorsClassic();

	// Setup Platform/Renderer backends
	ImGui_ImplGlfw_InitForOpenGL(m_window, false);

	m_sceneObjectsList = std::make_unique<ListWidget>();
	m_objectEidtorWidget = std::make_unique<ObjectEditorWidget>();

	//m_size[0] = m_size[1] =	m_size[2] = m_size[3] = 0.0f;

	m_width = m_height = 0;

    ImGui_ImplOpenGL3_Init();
}

Widget::~Widget()
{
	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}

void Widget::drawGUI()
{
#ifdef UI_EXAMPLE
	drawExample();
	return;
#endif

	// Start the Dear ImGui frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

    ImGuiWindowFlags flags = ImGuiWindowFlags_NoMove;
    ImGui::Begin("Scene properties", nullptr, flags);

    ImVec2 size = ImGui::GetWindowSize();
    m_width = static_cast<size_t>(size.x);

    ImGui::SetWindowPos(ImVec2(0, 0));
    ImGui::SetWindowSize(ImVec2(size.x, static_cast<float>(m_height)));

    m_sceneObjectsList->draw();

    if (m_sceneObjectsList->hasSelectedItem())
    {
        m_objectEidtorWidget->draw();
    }
    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

    ImGui::End();

    // Rendering
    ImGui::Render();
}

void Widget::render()
{
	//glClear(GL_COLOR_BUFFER_BIT);
	//glClearColor(1.0f, 0.5f, 0.3f, 1.0f);

	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void Widget::imguiMouseButtonCallback(int button, int action, int mods)
{
	ImGui_ImplGlfw_MouseButtonCallback(m_window, button, action, mods);
}

void Widget::bindImguiCallbacks()
{
	glfwSetScrollCallback(m_window, ImGui_ImplGlfw_ScrollCallback);
	glfwSetCharCallback(m_window, ImGui_ImplGlfw_CharCallback);
}

ObjectEditorWidget* Widget::getObjectEditorForm()
{
	return m_objectEidtorWidget.get();
}

ListWidget* Widget::getObjectsListForm()
{
	return m_sceneObjectsList.get();
}

void Widget::imguiKeyCallback(int key, int scancode, int action, int mods)
{
	ImGui_ImplGlfw_KeyCallback(m_window, key, scancode, action, mods);
}

#ifdef UI_EXAMPLE
void Widget::drawExample()
{
	// Start the Dear ImGui frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	// 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
	if (show_demo_window)
		ImGui::ShowDemoWindow(&show_demo_window);

	// 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
	{
		ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.

		ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
		ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
		ImGui::Checkbox("Another Window", &show_another_window);

		ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
		ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

		if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
			counter++;
		ImGui::SameLine();
		ImGui::Text("counter = %d", counter);

		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::End();
	}

	// 3. Show another simple window.
	if (show_another_window)
	{
		ImGui::Begin("Another Window", &show_another_window);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
		ImGui::Text("Hello from another window!");
		if (ImGui::Button("Close Me"))
			show_another_window = false;
		ImGui::End();
	}

	// Rendering
	ImGui::Render();


	int display_w, display_h;
	glfwGetFramebufferSize(m_window, &display_w, &display_h);
	glViewport(0, 0, display_w, display_h);
    glClearColor(clear_color[0], clear_color[1], clear_color[2], clear_color[3]);
	glClear(GL_COLOR_BUFFER_BIT);

	// If you are using this code with non-legacy OpenGL header/contexts (which you should not, prefer using imgui_impl_opengl3.cpp!!),
	// you may need to backup/reset/restore current shader using the commented lines below.
	//GLint last_program;
	//glGetIntegerv(GL_CURRENT_PROGRAM, &last_program);
	//glUseProgram(0);
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	//glUseProgram(last_program);
}
#endif
