#pragma once


#include <array>
#include <vector>
#include <functional>
#include <string>
#include <memory>

//#define EXAMPLE

class ObjectEditorWidget;
class ListWidget;

struct GLFWwindow;

class Widget
{
public:
	Widget(GLFWwindow * win);

	~Widget();

	void drawGUI();

	void render();

	void imguiMouseButtonCallback(int button, int action, int mods);
	void imguiKeyCallback(int key, int scancode, int action, int mods);

	void bindImguiCallbacks();

	ObjectEditorWidget* getObjectEditorForm();

	ListWidget* getObjectsListForm();

    bool posOnWidget(double x, double y)
	{
		//return x > m_size[0] && x < m_size[2] &&
		//	y > m_size[1] && y < m_size[3];
		return x < m_width && y < m_height;
	}

    void resize(int32_t /*width*/, int32_t height)
	{
		//m_width = width;
		m_height = height;
	}

protected:

#ifdef UI_EXAMPLE
	void drawExample();

	// Our state
	float f = 0.0f;
	int counter = 0;
	bool show_demo_window = true;
	bool show_another_window = false;
    float clear_color[4] = {0.45f, 0.55f, 0.60f, 1.00f};
#endif

	int32_t m_width;
	int32_t m_height;

	GLFWwindow *m_window;

	std::unique_ptr<ObjectEditorWidget> m_objectEidtorWidget;
	std::unique_ptr<ListWidget> m_sceneObjectsList;

};
