#include "ObjectEditorWidget.h"

#include "GUI/VectorBox.h"
#include "GUI/ComboBox.h"

ObjectEditorWidget::ObjectEditorWidget()
{
	m_modelsCombobox = std::make_unique<ComboBox>();

	m_positionBox = std::make_unique<VectorBox>();
	m_positionBox->setTitle("position");
	m_positionBox->setRange(-1000.0f, 1000.0f);
	m_positionBox->setSpeed(0.01f);

	m_rotationBox = std::make_unique<VectorBox>();
	m_rotationBox->setTitle("rotation");
	m_rotationBox->setRange(0.0f, 360.0f);

	m_scaleBox = std::make_unique<VectorBox>();
	m_scaleBox->setTitle("scale");
	m_scaleBox->setRange(0.01f, 100.0f);
	m_scaleBox->setSpeed(0.01f);
}

ObjectEditorWidget::~ObjectEditorWidget()
{
}

void ObjectEditorWidget::setPositionChangedCallback(VecChangedFunc& callback)
{
	m_positionBox->setValuesChangedCallback(callback);
}

void ObjectEditorWidget::setRotationChangedCallback(VecChangedFunc& callback)
{
	m_rotationBox->setValuesChangedCallback(callback);
}

void ObjectEditorWidget::setScaleChangedCallback(VecChangedFunc& callback)
{
	m_scaleBox->setValuesChangedCallback(callback);
}

void ObjectEditorWidget::setPosition(const float vec[3])
{
	m_positionBox->setValues(vec);
}

void ObjectEditorWidget::setRotation(const float vec[3])
{
	m_rotationBox->setValues(vec);
}

void ObjectEditorWidget::setScale(const float vec[3])
{
	m_scaleBox->setValues(vec);
}

void ObjectEditorWidget::draw()
{
	m_modelsCombobox->draw();

	m_positionBox->draw();
	m_rotationBox->draw();
	m_scaleBox->draw();
}

void ObjectEditorWidget::setModelChangedCallback(const std::function<void(int32_t)>& indexChangedCallback)
{
	m_modelsCombobox->setIndexChangedCallback(indexChangedCallback);
}

void ObjectEditorWidget::setModelsList(const std::vector<std::string>& modelNames)
{
	m_modelsCombobox->setList(modelNames);
}

void ObjectEditorWidget::setModelIndex(int32_t index)
{
	m_modelsCombobox->setIndex(index);
}
