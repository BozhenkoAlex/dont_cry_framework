#include "Canvas.h"

#include <functional>

#include "IEngine.h"
#include "ICameraManipulator.h"

#include "SceneObjectEditor.h"

Canvas::Canvas(int32_t width, int32_t height)
    :
    m_width(width),
    m_height(height)
{
    engine = IEngine::create(m_width, m_height);
    if (!engine)
	{
        fprintf(stderr, "Failed to create GameObject\n");
        exit(1);
	}

	m_sceneObjectEditor = std::make_unique<SceneObjectEditor>();

    auto modelList = engine->getModelList();
	m_sceneObjectEditor->setModelList(modelList);
}

Canvas::~Canvas()
{
}

void Canvas::updateTime(float delta)
{
    engine->updateTime(delta);
}

void Canvas::draw(int32_t selectedObjectId)
{
    engine->draw(selectedObjectId);
}

void Canvas::processKey(int32_t key, bool isPressed)
{
    ICameraManipulator* camera = engine->getCamera();
    camera->processKey(key, isPressed);
}

void Canvas::resize(int32_t width, int32_t height)
{
    m_width = width;
    m_height = height;
    engine->resize(width, height);
}

std::optional<size_t> Canvas::selectObject(bool pressed, double posX, double posY)
{
	if (pressed)
	{
        auto selectedObject = engine->select(static_cast<int32_t>(posX),
                                             static_cast<int32_t>(posY));
		m_sceneObjectEditor->setObject(selectedObject);
        return engine->getObjectIndex(selectedObject);
	}
    return {};
}

std::vector<int32_t> Canvas::getObjectsList()
{
    return engine->getObjectsList();
}

void Canvas::selectObject(int32_t index)
{
    auto object = engine->getObject(index);
	m_sceneObjectEditor->setObject(object);
}

void Canvas::moveSelectedObject(double posX, double posY)
{
    if (!m_sceneObjectEditor->hasObject())
    {
        return;
    }

    auto pos = engine->getTerrainInterscation(posX, posY);
    if (pos.has_value())
    {
        m_sceneObjectEditor->moveObject(pos.value());
    }
}
