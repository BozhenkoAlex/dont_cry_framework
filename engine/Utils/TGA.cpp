#include "TGA.h"

#include <fstream>

#pragma pack(push,x1)					// Byte alignment (8-bit)
#pragma pack(1)

typedef struct
{
	unsigned char  identsize;			// size of ID field that follows 18 byte header (0 usually)
	unsigned char  colourmaptype;		// type of colour map 0=none, 1=has palette
	unsigned char  imagetype;			// type of image 2=rgb uncompressed, 10 - rgb rle compressed

	short colourmapstart;				// first colour map entry in palette
	short colourmaplength;				// number of colours in palette
	unsigned char  colourmapbits;		// number of bits per palette entry 15,16,24,32

	short xstart;						// image x origin
	short ystart;						// image y origin
	short width;						// image width in pixels
	short height;						// image height in pixels
	unsigned char  bits;				// image bits per pixel 24,32
	unsigned char  descriptor;			// image descriptor bits (vh flip bits)

	// pixel data follows header

} TGA_HEADER;

#pragma pack(pop,x1)

const int32_t IT_COMPRESSED = 10;
const int32_t IT_UNCOMPRESSED = 2;

void LoadCompressedImage(uint8_t* pDest, uint8_t * pSrc, TGA_HEADER * pHeader)
{
	int32_t w = pHeader->width;
	int32_t h = pHeader->height;
	int32_t rowSize = w * pHeader->bits / 8;
	bool bInverted = ((pHeader->descriptor & (1 << 5)) != 0);
	uint8_t * pDestPtr = bInverted ? pDest + (h + 1) * rowSize : pDest;
	int32_t countPixels = 0;
	int32_t nPixels = w * h;

	while (nPixels > countPixels)
	{
		unsigned char chunk = *pSrc++;
		if (chunk < 128)
		{
			int32_t chunkSize = chunk + 1;
			for (int32_t i = 0; i < chunkSize; i++)
			{
				if (bInverted && (countPixels % w) == 0)
					pDestPtr -= 2 * rowSize;
				*pDestPtr++ = pSrc[2];
				*pDestPtr++ = pSrc[1];
				*pDestPtr++ = pSrc[0];
				pSrc += 3;
				if (pHeader->bits != 24)
					*pDestPtr++ = *pSrc++;
				countPixels++;
			}
		}
		else
		{
			int32_t chunkSize = chunk - 127;
			for (int32_t i = 0; i < chunkSize; i++)
			{
				if (bInverted && (countPixels % w) == 0)
					pDestPtr -= 2 * rowSize;
				*pDestPtr++ = pSrc[2];
				*pDestPtr++ = pSrc[1];
				*pDestPtr++ = pSrc[0];
				if (pHeader->bits != 24)
					*pDestPtr++ = pSrc[3];
				countPixels++;
			}
			pSrc += (pHeader->bits >> 3);
		}
	}
}

void LoadUncompressedImage(uint8_t* pDest, uint8_t * pSrc, TGA_HEADER * pHeader)
{
	int32_t w = pHeader->width;
	int32_t h = pHeader->height;
	int32_t rowSize = w * pHeader->bits / 8;
	bool bInverted = ((pHeader->descriptor & (1 << 5)) != 0);
	for (int32_t i = 0; i < h; i++)
	{
		uint8_t * pSrcRow = pSrc +
			(bInverted ? (h - i - 1) * rowSize : i * rowSize);
		if (pHeader->bits == 24)
		{
			for (int32_t j = 0; j < w; j++)
			{
				*pDest++ = pSrcRow[2];
				*pDest++ = pSrcRow[1];
				*pDest++ = pSrcRow[0];
				pSrcRow += 3;
			}
		}
		else
		{
			for (int32_t j = 0; j < w; j++)
			{
				*pDest++ = pSrcRow[2];
				*pDest++ = pSrcRow[1];
				*pDest++ = pSrcRow[0];
				*pDest++ = pSrcRow[3];
				pSrcRow += 4;
			}
		}
	}
}


std::unique_ptr<TGA::Image> TGA::Load(const std::string& filename)
{
    FILE* file = NULL;
    fopen_s(&file, filename.c_str(), "rb");

    if (NULL == file)
        return nullptr;

    TGA_HEADER header;
    fread(&header, sizeof(header), 1, file);

    fseek(file, 0, SEEK_END);
    int32_t fileLen = ftell(file);
    fseek(file, sizeof(header) + header.identsize, SEEK_SET);

    if (header.imagetype != IT_COMPRESSED && header.imagetype != IT_UNCOMPRESSED)
    {
        fclose(file);
        return nullptr;
	}

	if (header.bits != 24 && header.bits != 32)
	{
        fclose(file);
		return nullptr;
	}

	int32_t bufferSize = fileLen - sizeof(header) - header.identsize;
	uint8_t * pBuffer = new uint8_t[bufferSize];
    fread(pBuffer, 1, bufferSize, file);
    fclose(file);

	auto tgaImage = std::make_unique<TGA::Image>();
	tgaImage->width = header.width;
	tgaImage->height = header.height;
	tgaImage->bpp = header.bits;
	tgaImage->buffer = std::make_unique<uint8_t[]>(header.width * header.height * header.bits / 8);

	switch (header.imagetype)
	{
	case IT_UNCOMPRESSED:
		LoadUncompressedImage(tgaImage->buffer.get(), pBuffer, &header);
		break;
	case IT_COMPRESSED:
		LoadCompressedImage(tgaImage->buffer.get(), pBuffer, &header);
		break;
	}

	delete[] pBuffer;


	return tgaImage;
}

std::unique_ptr<TGA::Image> TGA::Create(
        int32_t width, int32_t height, int32_t bpp)
{
    auto tgaImage = std::make_unique<TGA::Image>();
    tgaImage->width = width;
    tgaImage->height = height;
    tgaImage->bpp = bpp;

    size_t bufferSize = tgaImage->width * tgaImage->height * tgaImage->bpp / 8;
    tgaImage->buffer = std::make_unique<uint8_t[]>(bufferSize);
    std::memset(tgaImage->buffer.get(), 0, bufferSize);

    return tgaImage;
}
