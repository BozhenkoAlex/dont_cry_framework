#pragma once

#include <string>

#define GLEW_STATIC
#include <GL/glew.h>

#include "Utils/Vertex.h"

class ImageRenderer
{
public:
	ImageRenderer()
	{
		float value = 1.0f;
		std::array<Vertex, 4> verticles;
		verticles[0].position = { -value, -value, 0.0 };
		verticles[1].position = { value,  -value, 0.0 };
		verticles[2].position = { -value, value, 0.0 };
		verticles[3].position = { value, value, 0.0 };

		m_vertexBuffer = 0;
		glGenBuffers(1, &m_vertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)* verticles.size(), verticles.data(), GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void bind()
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	}

	void unbind()
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void draw()
	{
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	}

	~ImageRenderer()
	{
		glDeleteBuffers(1, &m_vertexBuffer);
	}

protected:

	GLuint m_vertexBuffer;
};