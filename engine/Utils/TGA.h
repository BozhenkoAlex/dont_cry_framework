#pragma once

#include <memory>
#include <string>

namespace TGA
{
	struct Image
	{
		std::unique_ptr<uint8_t[]> buffer = nullptr;
		int32_t width = 0;
		int32_t height = 0;
		int32_t bpp = 0;
	};

    std::unique_ptr<Image> Load(const std::string& filename);
    std::unique_ptr<Image> Create(int32_t width,
                                  int32_t height,
                                  int32_t bpp);
}
