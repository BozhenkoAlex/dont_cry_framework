#include "JsonLoader.h"

#include <cmath>
#include <fstream>
#include <memory>

#include <glm/glm.hpp>

#include "ResourceManagment/ResourceLoader.h"
#include "ResourceManagment/Model.h"
#include "ResourceManagment/AssimpModel.h"
#include "ResourceManagment/MultiLayeredHeightmap.h"

#include "SceneManagement/SceneManager.h"
#include "SceneManagement/SceneObject.h"
#include "SceneManagement/AnimationObject.h"
#include "SceneManagement/TerrainObject.h"
#include "SceneManagement/FogObject.h"
#include "SceneManagement/WaterObject.h"
#include "SceneManagement/LookAtCamera.h"

int32_t getId(const json& value)
{
	if (value.find("ID") != value.end())
	{
		auto id = value["ID"];
		if (id.is_number())
		{
			return id;
		}
	}
	return -1;
}

glm::vec3 getValue(const json& value)
{
	if (value.is_array() && value.size() == 3)
	{
		std::array<float, 3> vec = value;
		return glm::vec3(vec[0], vec[1], vec[2]);
	}

	assert(false); 
	return glm::vec3(NAN);
};

std::unique_ptr<SceneManager>
JsonLoader::loadScene(const std::string& filename, const ResourceLoader* resouceManager, int32_t width, int32_t height)
{
	std::ifstream iStream(filename);
	if (!iStream.is_open())
	{
		return nullptr;
	}

	json rootJson;
	iStream >> rootJson;

	auto sceneManager = std::make_unique<SceneManager>();

	if (rootJson.find("skybox_object") != rootJson.end())
	{
        auto object = createObject<SceneObject>(rootJson["skybox_object"], resouceManager);
		sceneManager->m_skyObject = std::move(object);
	}

	auto pointLightSettings = rootJson["point_lights"];
	for (auto& light : pointLightSettings)
	{
        auto lightObject = JsonLoader::createPointLight(light);
		if (lightObject)
		{
			sceneManager->m_sceneLighting.addPointLight(std::move(lightObject));
		}
	}

	auto directionalLightSettings = rootJson["direction_lights"];
	for (auto& light : directionalLightSettings)
	{
        auto lightObject = JsonLoader::createDirectionalLight(light);
		if (lightObject)
		{
			sceneManager->m_sceneLighting.addDirectionalLight(std::move(lightObject));
		}
	}

	if (rootJson.find("ambient_light") != rootJson.end())
	{
		auto ambientLightSettings = rootJson["ambient_light"];
		auto object = JsonLoader::createAmbientLight(ambientLightSettings);
		sceneManager->m_sceneLighting.setAmbientLight(std::move(object));
	}

	if (rootJson.find("cameras") != rootJson.end())
	{
		json cameraSettings = rootJson["cameras"];
		for (auto& value : cameraSettings)
		{
			int32_t id = getId(value);
			if (id < 0)
				continue;

			auto camera = JsonLoader::createCamera(value, width, height);
			sceneManager->m_cameras.emplace_back(std::move(camera));
			sceneManager->setCurrentCamera(id);
		}
	}

    auto animationSettings = rootJson["animations"];
    for (auto& value : animationSettings)
    {
        auto animationObject = createObject<AnimationObject, AssimpModel>(value, resouceManager);
        if (animationObject)
        {
            animationObject->setSceneLight(&sceneManager->m_sceneLighting);
            sceneManager->m_sceneObjects.emplace_back(std::move(animationObject));
        }
    }

    auto objectSettings = rootJson["objects"];
    for (auto& value : objectSettings)
    {
        auto sceneObject = createObject<SceneObject>(value, resouceManager);
        if (sceneObject)
        {
            sceneObject->setSceneLight(&(sceneManager->m_sceneLighting));
            sceneManager->m_sceneObjects.emplace_back(std::move(sceneObject));
        }
    }

	if (rootJson.find("water_object") != rootJson.end())
	{
		auto waterObjectValue = rootJson["water_object"];
		auto camera = sceneManager->getCurrentCamera();
		auto object = createWater(waterObjectValue, resouceManager, camera);
		sceneManager->m_waterObject = std::move(object);
	}

	if (rootJson.find("terrain_object") != rootJson.end())
	{
		auto terrainObjectValue = rootJson["terrain_object"];
        auto object = createObject<TerrainObject, MultiLayeredHeightmap>(terrainObjectValue, resouceManager);
		object->setSceneLight(&sceneManager->m_sceneLighting);
		sceneManager->m_terrainObject = std::move(object);
	}

    {
        auto shader = resouceManager->getBorderShader(1); // TODO
        SceneObject::initBoundSphere(shader);
    }

    auto fogSettings = rootJson["fog"];
	auto fogObject = JsonLoader::createFog(fogSettings);
	sceneManager->setFog(std::move(fogObject));

	return sceneManager;
}

std::unique_ptr<FogObject> JsonLoader::createFog(const json& object)
{
	if (!object.is_object())
	{
		return nullptr;
	}

	auto fogObject = std::make_unique<FogObject>();

	fogObject->color = getValue(object["color"]);
	fogObject->start = object["start"];
	fogObject->range = object["range"];
	fogObject->lowerLimit = object["lowerLimit"];
	fogObject->upperLimit = object["upperLimit"];

	return fogObject;
}

std::unique_ptr<PointLight> JsonLoader::createPointLight(const json& object)
{
	auto light = std::make_unique<PointLight>();
	light->m_position = getValue(object["position"]);
	light->m_color = getValue(object["color"]);
	return light;
}

std::unique_ptr<DirectionalLight> JsonLoader::createDirectionalLight(const json& object)
{
	auto light = std::make_unique<DirectionalLight>();
	glm::vec3 direction = glm::normalize(getValue(object["direction"]));
	light->setDirection(direction);
	glm::vec3 color = getValue(object["color"]);
	light->setColor(color);
	return light;
}

std::unique_ptr<AmbientLight> JsonLoader::createAmbientLight(const json& object)
{
	auto light = std::make_unique<AmbientLight>();
	glm::vec3 color = getValue(object["color"]);
	light->setColor(color);
	float wieght = object["weight"];
	light->setWeight(wieght);
	return light;
}


std::unique_ptr<WaterObject> 
JsonLoader::createWater(const json& object, const ResourceLoader* resouceManager, const LookAtCamera* camera)
{
	if (!object.is_object())
	{
		return nullptr;
	}

    // TODO: create water models and store textures there
	auto obj = std::make_unique<WaterObject>();

	int32_t shaderId = object["shader"];
	auto shader = resouceManager->getShader(shaderId);
	obj->setShader(shader);

    std::vector<int32_t> textureIds = object["2Dtex"];
    std::vector<const Texture*> textures;
    for (int32_t id : textureIds)
    {
        auto texture = resouceManager->getTextureSampler(id);
        if (texture)
        {
            textures.emplace_back(texture);
        }
    }

	obj->setTextures(textures);

	setObjectParameters(object, obj.get());

	if (camera)
	{
		float nearPlane = camera->getNear();
		float farPlane = camera->getFar();
		obj->setFarAndNear(nearPlane, farPlane);
	}

	return obj;
}


template<class Object, class ModelType>
std::unique_ptr<Object> JsonLoader::createObject(const json& object, const ResourceLoader* resouceManager)
{
	if (!object.is_object())
	{
		return nullptr;
	}

	auto obj = std::make_unique<Object>();

	int32_t modelId = -1;
	if (object.find("model") != object.end())
	{
		modelId = object["model"];
		auto model = resouceManager->getModel<ModelType>(modelId);
		obj->setModel(model);
	}
	setObjectParameters(object, obj.get());

	return obj;
}

LookAtCamera* JsonLoader::createCamera(const json& object, int32_t width, int32_t height)
{
	auto camera = new LookAtCamera();

	float nearValue = object["near"];
	float farValue = object["far"];
	float fovValue = object["fov"];
    if (!std::isnan(nearValue) &&
        !std::isnan(farValue) &&
        !std::isnan(fovValue))
	{
		float aspect = static_cast<float>(width) / height;
		camera->setProjectionMatrix(nearValue, aspect, farValue, fovValue);
	}

	float speed = object["speed"];
    if (!std::isnan(speed))
	{
		camera->setSpeed(speed);
	}

	float rotationSpeed = object["rot_speed"];
    if (!std::isnan(rotationSpeed))
	{
		camera->setRotationSpeed(rotationSpeed);
	}

	auto position = getValue(object["position"]);
	camera->setPosition(position);

	auto rotation = getValue(object["rotation"]);
	camera->setRotation(rotation);

	return camera;
}

void JsonLoader::setObjectParameters(const json& object, SceneObject* obj)
{
	if (object.find("rotation") != object.end())
	{
		auto rotation = getValue(object["rotation"]);
		obj->setRotation(rotation);
	}

	if (object.find("position") != object.end())
	{
		auto position = getValue(object["position"]);
		obj->setPosition(position);
	}

	if (object.find("scale") != object.end())
	{
		auto scale = getValue(object["scale"]);
		obj->setScale(scale);
	}

	if (object.find("parameters") != object.end())
	{
		std::unordered_map<std::string, json> parametersValue = object["parameters"];
		for (auto& name : parametersValue)
		{
			addObjectParameter(name.second, name.first, obj);
		}
	}
}

void JsonLoader::addObjectParameter(const json& value, const std::string& name, SceneObject* obj)
{
	if (value.is_number())
	{
		float floatValue = value;
		obj->addParameter(name, floatValue);
	}
	else if (value.is_array())
	{
		auto vecValue = getValue(value);
		obj->addParameter(name, vecValue);
	}
	else
	{
		assert(false);
	}
}
