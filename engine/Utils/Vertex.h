#pragma once

#include <array>

#include <glm/glm.hpp>

struct Vertex 
{
    static const int8_t MAX_BONES_COUNT = 6;

    glm::vec3 position = glm::vec3(0);
    glm::vec3 normal = glm::vec3(1, 1, 1);
    glm::vec2 uv = glm::vec2(0);

    std::array<int, MAX_BONES_COUNT> boneIds;
    std::array<float, MAX_BONES_COUNT> boneWeights;

    void addBoneData(size_t boneId, float weight)
    {
        for (int8_t i = 0; i < MAX_BONES_COUNT; ++i)
        {
            if (std::isnan(boneWeights[i]))
            {
                boneIds[i] = boneId;
                boneWeights[i] = weight;
                return;
            }
        }

        // should never get here - more bones than we have space for
        assert(0);
        return;
    }

    void normilizeBoneWeights()
    {
        float length = 0;
        for(auto& value: boneWeights)
        {
            if(std::isnan(value))
                break;

            length += value;
        }

        for(auto& value: boneWeights)
        {
            if(std::isnan(value))
                break;

            value /= length;
        }
    }

    Vertex()
    {
        for(int8_t i = 0; i < MAX_BONES_COUNT; ++i)
        {
            boneIds[i] = boneWeights[i] = NAN;
        }
    }
};
