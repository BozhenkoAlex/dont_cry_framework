#include "JsonWriter.h"

#include <fstream>
#include <iomanip>

#include <glm/glm.hpp>

#include "ResourceManagment/ResourceLoader.h"

#include "ResourceManagment/MultiLayeredHeightmap.h"

#include "SceneManagement/SceneManager.h"

#include "SceneManagement/AnimationObject.h"
#include "SceneManagement/SceneObject.h"
#include "SceneManagement/TerrainObject.h"
#include "SceneManagement/FogObject.h"
#include "SceneManagement/WaterObject.h"
#include "SceneManagement/LookAtCamera.h"

#include "SceneManagement/lighting/SceneLight.h"

#include "Utils/UniformValue.h"

std::array<float, 3> vecToArray(const glm::vec3& vec)
{
	return {vec.x, vec.y, vec.z};
}

void JsonWriter::writeScene(const std::string& filename, const SceneManager* sceneManager, const ResourceLoader* resourceLoader)
{
	std::ofstream outputStream(filename);


	json rootValue;

	const auto& ambientLight = sceneManager->m_sceneLighting.getAmbientLight();
	rootValue["ambient_light"] = getAmbientLight(ambientLight);

	const auto& pointLights = sceneManager->m_sceneLighting.getPointLights();
	rootValue["point_lights"] = getPointLights(pointLights);

	const auto& directionLights = sceneManager->m_sceneLighting.getDirectionalLights();
	rootValue["direction_lights"] = getDirectionLights(directionLights);

	rootValue["cameras"] = getCameras(sceneManager->m_cameras);

	rootValue["fog"] = getFog(sceneManager->m_fogObject);

	auto skyObject = dynamic_cast<const SceneObject*>(sceneManager->m_skyObject.get());
    rootValue["skybox_object"] = JsonWriter::getObject(skyObject, resourceLoader);

    auto terrain = sceneManager->m_terrainObject.get();
    rootValue["terrain_object"] = JsonWriter::getObject<MultiLayeredHeightmap>(terrain, resourceLoader);

    auto* waterObject = static_cast<WaterObject*>(sceneManager->m_waterObject.get());
    rootValue["water_object"] = getWater(waterObject, resourceLoader);
	

	{
        std::vector<json> jsonSceneObjects;
        std::vector<json> jsonSceneAnimatedObjects;
        for (const auto& object : sceneManager->m_sceneObjects)
		{
            auto objectPtr = dynamic_cast<AnimationObject*>(object.get());
            if (objectPtr)
            {
                jsonSceneAnimatedObjects.emplace_back(
                    JsonWriter::getObject<AssimpModel>(object.get(), resourceLoader));
            }
            else
            {
                jsonSceneObjects.emplace_back(
                    JsonWriter::getObject(object.get(), resourceLoader));
            }
		}

        rootValue["objects"] = jsonSceneObjects;
        rootValue["animations"] = jsonSceneAnimatedObjects;
    }

	outputStream << std::setw(4) << rootValue << std::endl;
}

template<class ModelType>
json JsonWriter::getObject(const SceneObject* sceneObject, const ResourceLoader* resourceLoader)
{
	json object;

    auto model = static_cast<const ModelType*>(sceneObject->getModel());
    int32_t id = resourceLoader->getModelId(model);
	if (id != -1)
	{
		object["model"] = id;
	}

	auto scale = sceneObject->getScale();
	if (scale != glm::vec3(1))
	{
		object["scale"] = vecToArray(scale);
	}

	auto rotation = sceneObject->getRotation();
	if (rotation != glm::vec3(0))
	{
		object["rotation"] = vecToArray(rotation);
	}

	auto position = sceneObject->getPosition();
	if (position != glm::vec3(0))
	{
		object["position"] = vecToArray(position);
	}

	object["parameters"] = JsonWriter::getParameters(sceneObject);
	
	return object;
}


json JsonWriter::getWater(const WaterObject* waterObject, const ResourceLoader* resourceLoader)
{
    json object = JsonWriter::getObject(waterObject, resourceLoader);

	auto shader = waterObject->getShader();
    int32_t shaderId = resourceLoader->getShaderId(shader);
	if (-1 != shaderId)
	{
		object["shader"] = shaderId;
	}

	auto textures = waterObject->getTextures();
    auto textureIds = resourceLoader->getTextureIds(textures);
	if (!textureIds.empty())
	{
		object["2Dtex"] = textureIds;
	}

	return object;
}

json JsonWriter::getParameters(const SceneObject* sceneObject)
{
	json jsonParameters;

	auto vecParameters = sceneObject->getParameters<UniformValue<glm::vec3>>();
	for (auto& parameter : vecParameters)
	{
		jsonParameters[parameter.first] = vecToArray(parameter.second);
	}

	auto floatParameters = sceneObject->getParameters<UniformValue<float>>();
	for (auto& parameter : floatParameters)
	{
		float value = parameter.second;
		jsonParameters[parameter.first] = value;
	}

	return jsonParameters;
}

json JsonWriter::getCameras(const std::vector<LookAtCamera*>& cameras)
{
	json object;
	std::vector<json> jsonCameras;
	for (size_t i = 0; i < cameras.size(); ++i)
	{
		json jsonCamera;
		jsonCamera["ID"] = i;
		jsonCamera["near"] = cameras[i]->getNear();
		jsonCamera["far"] = cameras[i]->getFar();
		jsonCamera["fov"] = cameras[i]->getFovDegrees();

		jsonCamera["speed"] = cameras[i]->getSpeed();
		jsonCamera["rot_speed"] = cameras[i]->getRotationSpeed();

		jsonCamera["position"] = vecToArray(cameras[i]->getPosition());
		jsonCamera["rotation"] = vecToArray(cameras[i]->getRotation());

		object.emplace_back(jsonCamera);
	}

	return object;
}

json JsonWriter::getPointLights(const std::vector<std::unique_ptr<PointLight>>& pointLights)
{
	json object;
	for (const auto& light : pointLights)
	{
		json jsonLight;
		jsonLight["position"] = vecToArray(light->m_position);
		jsonLight["color"] = vecToArray(light->m_color);
		object.emplace_back(jsonLight);
	}

	return object;
}

json JsonWriter::getDirectionLights(const std::vector<std::unique_ptr<DirectionalLight>>& directionalLights)
{
	json object;
	for (const auto& light : directionalLights)
	{
		json jsonLight;
		jsonLight["direction"] = vecToArray(light->getDirection());
		jsonLight["color"] = vecToArray(light->getColor());
		object.emplace_back(jsonLight);
	}

	return object;
}

json JsonWriter::getFog(const std::unique_ptr<FogObject>& fogObject)
{
	json object;
	object["color"] = vecToArray(fogObject->color);

	object["start"] = static_cast<float>(fogObject->start);
	object["range"] = static_cast<float>(fogObject->range);

	object["lowerLimit"] = static_cast<float>(fogObject->lowerLimit);
	object["upperLimit"] = static_cast<float>(fogObject->upperLimit);
	return object;
}


json JsonWriter::getAmbientLight(const std::unique_ptr<AmbientLight>& ambientLight)
{
	json object;
	object["color"] = vecToArray(ambientLight->getColor());
	object["weight"] = ambientLight->getWeight();
	return object;
}
