#pragma once

#include <memory>
#include <string>


#include <nlohmann/json.hpp>
using json = nlohmann::json;

class SceneManager;
class ResourceLoader;

class Model;
class LookAtCamera;
class SceneObject;
class WaterObject;

struct FogObject;

class AmbientLight;
class PointLight;
class DirectionalLight;

class JsonLoader
{
public:
	static std::unique_ptr<SceneManager>
	loadScene(const std::string& filename, const ResourceLoader* resouceManager, int32_t width, int32_t height);

protected:

	static std::unique_ptr<FogObject> createFog(const json& object);

	static std::unique_ptr<PointLight> createPointLight(const json& object);

	static std::unique_ptr<DirectionalLight> createDirectionalLight(const json& object);

	static std::unique_ptr<AmbientLight> createAmbientLight(const json& object);

	static std::unique_ptr<WaterObject>
	createWater(const json& object, const ResourceLoader* resouceManager, const LookAtCamera* camera);

	template <class Object, class ModelType = Model>
	static std::unique_ptr<Object> createObject(const json& object, const ResourceLoader* resouceManager);

	static LookAtCamera* createCamera(const json& object, int32_t width, int32_t height);

	static void setObjectParameters(const json& object, SceneObject* obj);

	static void addObjectParameter(const json& objectValue, const std::string& parameterName, SceneObject* obj);
};