#pragma once

#include <string>
#include <unordered_map>

#define GLEW_STATIC
#include <gl/glew.h>

template<class Type>
class UniformValue
{
public:
	UniformValue()
	{
		m_data = Type();
	}

	UniformValue(const Type& data)
	{
		m_data = data;
	}

	UniformValue(const UniformValue& obj)
	{
		*this = obj;
	}
	
	UniformValue& operator=(const UniformValue& obj)
	{
		this->m_data = obj.m_data;
		return *this;
	}

	operator Type()
	{
		return m_data;
	}

	operator const Type() const
	{
		return m_data;
	}

    void uniform(GLint location) const;

protected:
	Type m_data;
};

template<class Type>
class UniformValuesMap
{
public:
	UniformValue<Type>&operator[](const std::string& name)
	{
		return m_valuesMap[name];
	}

    void uniform(GLint programId) const
	{
		for (auto& value : m_valuesMap)
		{
			std::string uniformPrefix = "u_";
			auto location = glGetUniformLocation(programId, (uniformPrefix + value.first).c_str());
			value.second.uniform(location);
		}
	}

	bool count(const std::string& name) const
	{
		return m_valuesMap.count(name);
    }

    const std::unordered_map<std::string, UniformValue<Type>>&
        data() const
    {
        return m_valuesMap;
    }

private:
	std::unordered_map<std::string, UniformValue<Type>> m_valuesMap;
};
