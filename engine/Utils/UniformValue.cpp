#include "UniformValue.h"

#include <glm/glm.hpp>

template<>
void UniformValue<float>::uniform(GLint location) const
{
    if (-1 != location)
    {
        glUniform1f(location, m_data);
    }
}

template<>
void UniformValue<glm::vec3>::uniform(GLint location) const
{
    if (-1 != location)
    {
        glUniform3fv(location, 1, &m_data.x);
    }
}

template<>
void UniformValue<glm::vec4>::uniform(GLint location) const
{
    if (-1 != location)
    {
        glUniform4fv(location, 1, &m_data.x);
    }
}

template<>
void UniformValue<glm::mat4>::uniform(GLint location) const
{
    if (-1 != location)
    {
        glUniformMatrix4fv(location, 1, false, &m_data[0][0]);
    }
}
