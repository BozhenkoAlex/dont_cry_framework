#pragma once

#include <memory>
#include <string>
#include <vector>


#include <nlohmann/json.hpp>
using json = nlohmann::json;

class ResourceLoader;

class SceneManager;

class Model;
class LookAtCamera;
class SceneObject;
class WaterObject;

class AmbientLight;
class PointLight;
class DirectionalLight;

struct FogObject;

class JsonWriter
{
public:
	static void	writeScene(const std::string& filename, const SceneManager* sceneManager, const ResourceLoader* ResourceLoader);

protected:

	template<class ModelType = Model>
	static json getObject(const SceneObject* sceneObject, const ResourceLoader* ResourceLoader);

	static json getWater(const WaterObject* waterObject, const ResourceLoader* ResourceLoader);

	static json getParameters(const SceneObject* sceneObject);

	static json getCameras(const std::vector<LookAtCamera*>& cameras);

	static json getPointLights(const std::vector<std::unique_ptr<PointLight>>& lights);

	static json getDirectionLights(const std::vector<std::unique_ptr<DirectionalLight>>& lights);

	static json getFog(const std::unique_ptr<FogObject>& fogObject);

	static json getAmbientLight(const std::unique_ptr<AmbientLight>& ambientLight);
};