#include "ShaderLoader.h"

#include <stdio.h>
#include <vector>

/// \brief Load a shader, check for compile errors, print error messages to output log
/// \param type Type of shader (GL_VERTEX_SHADER or GL_FRAGMENT_SHADER)
/// \param shaderSrc Shader source string
/// \return A new shader object on success, 0 on failure
//
GLuint LoadShaderFromFile(GLenum type, const std::string& filename)
{
    // Load the shader source
    FILE* file = NULL;
    fopen_s(&file, filename.c_str(), "rb");

    if (file == NULL)
        return 0;

    fseek(file, 0, SEEK_END);
    size_t size = ftell(file);
    fseek(file, 0, SEEK_SET);

	std::string shaderSrc;
	shaderSrc.resize(size + 1);
    fread(const_cast<char*>(shaderSrc.c_str()), sizeof(char), size, file);
	shaderSrc[size] = 0;
    fclose(file);

	return LoadShader(type, shaderSrc);
}

GLuint LoadShader(GLenum type, const std::string& shaderSrc)
{
	// Create the shader object
	GLuint shader = glCreateShader(type);
	if (shader == 0)
	{
		return 0;
	}

	auto source = shaderSrc.c_str();
	glShaderSource(shader, 1, (const char**)(&source), NULL);

	// Compile the shader
	glCompileShader(shader);

	// Check the compile status
	GLint compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		GLint infoLen = 0;

		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

		if (infoLen > 1)
		{
			char* infoLog = new char[infoLen];
			glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
            fprintf(stderr, "Error compiling shader:\n%s\n", infoLog);
			delete[] infoLog;
		}

		glDeleteShader(shader);
		return 0;
	}

	return shader;
}

/// \brief Create a program object, link program.
//         Errors output to log.
/// \param vertShaderSrc Vertex shader source code
/// \param fragShaderSrc Fragment shader source code
/// \return A new program object linked with the vertex/fragment shader pair, 0 on failure

GLuint LoadProgram(GLuint vertexShader, GLuint fragmentShader)
{
	GLuint programObject;
	GLint linked;

	// Create the program object
	programObject = glCreateProgram();
    if (programObject == 0)
    {
        return 0;
    }

	glAttachShader(programObject, vertexShader);
	glAttachShader(programObject, fragmentShader);

	// Link the program
	glLinkProgram(programObject);

	// Check the link status
	glGetProgramiv(programObject, GL_LINK_STATUS, &linked);

	if (!linked)
	{
		GLint infoLen = 0;

		glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);

		if (infoLen > 1)
		{
            std::string log;
            log.resize(infoLen);

            glGetProgramInfoLog(programObject, static_cast<GLsizei>(log.size()), NULL, log.data());
			printf("Error linking program:\n%s\n", log.data());
		}

		glDeleteProgram(programObject);
		return 0;
	}

	return programObject;
}
