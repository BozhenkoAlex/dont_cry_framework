#pragma once

#define _USE_MATH_DEFINES
#include <math.h>

#include <stdlib.h>
#include <time.h>

#include <glm/glm.hpp>

class RandomGenerator 
{
public:
	RandomGenerator(glm::vec3 scale)
	{
		srand((unsigned int)time(nullptr));
		m_limits[0] = scale[0];
		m_limits[1] = 0.5f * scale[1];
		m_limits[2] = scale[2];
	}

	glm::vec3 randomPositionVec() const
	{
		glm::vec3 result(0.0);
		result.x = value() * m_limits[0] * 0.5f - m_limits[0] * 0.5f;
		return result;
	}

	glm::vec3 randomRotationVec() const
	{
		glm::vec3 result;
		result.x = 0.0f;
		result.y = static_cast<float>( value() * 2 * M_PI );
		result.z = 0.0f;
			
		return result;
	}

	//return random value from 0 to 1
	float value() const
	{
		auto randomValue = rand();
		auto floatValue = static_cast<float>(randomValue);
		return floatValue / RAND_MAX;;
	}

private:
	float m_limits[3];
};