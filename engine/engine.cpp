#include "engine.h"

#include "EffectManagment/EffectManager.h"
#include "ResourceManagment/ResourceLoader.h"
#include "SceneManagement/CameraManipulator.h"
#include "SceneManagement/LookAtCamera.h"
#include "SceneManagement/SceneManager.h"
#include "SceneManagement/SceneObject.h"

#include "Utils/JsonLoader.h"
#include "Utils/JsonWriter.h"

Engine::Engine(int width, int height)
    : m_isInitialized(init(width, height))
{
}

Engine::~Engine()
{
//    JsonWriter::writeScene("SM.json", m_sceneManager.get(), m_resourceLoader.get());
}

bool Engine::isInitialized() const
{
    return m_isInitialized;
}

bool Engine::init(int32_t width, int32_t height)
{
    m_width = width;
    m_height = height;

    m_resourceLoader = std::make_unique<ResourceLoader>();
    int32_t ret = m_resourceLoader->init("RM.json");
    if (ret < 0)
    {
        fprintf(stderr, "failed to init ResourceLoader\n");
        return false;
    }

    m_sceneManager = JsonLoader::loadScene("SM.json", m_resourceLoader.get(), m_width, m_height);
    if (!m_sceneManager)
    {
        fprintf(stderr, "failed to init SceneManager\n");
        return false;
    }


    m_effectManager = std::make_unique<EffectManager>();
    ret = m_effectManager->init("EM.json");
    if (ret < 0)
    {
        fprintf(stderr, "failed to init EffectManager\n");
        return false;
    }

    {
        auto camera = m_sceneManager->getCurrentCamera();
        if (camera)
        {
            float nearPlane = camera->getNear();
            float farPlane = camera->getFar();
            m_effectManager->setFarAndNear(nearPlane, farPlane);
            m_cameraManipulator = std::make_unique<CameraManipulator>(*camera);
        }
    }

    return true;
}

void Engine::draw(int32_t selectedObjectId)
{
    SceneManager* scene = m_sceneManager.get();
    scene->updateCameraPosition();
    scene->preranderWater(m_width, m_height);

#ifdef SHADOW_TEST
    scene->preranderShadows();
#endif

    std::function<void()> drawFunc = [scene]()
    {
        scene->draw();
        scene->drawWater();
    };

    m_effectManager->drawToTexture(m_width, m_height, drawFunc);
    m_effectManager->drawThroughEffectPathes();

    scene->drawSelectedObj(selectedObjectId);

    int32_t width = m_width / 3;
    int32_t height = m_height / 3;
    glViewport(m_width - width, m_height - height, width, height);

#ifdef SHADOW_TEST
    scene->renderShadowMap();
#endif
}

void Engine::resize(int32_t width, int32_t height)
{
    if (width && height)
    {
        m_width = width;
        m_height = height;
        m_sceneManager->resize(m_width, m_height);
    }
}

ISceneObject* Engine::select(int32_t x, int32_t y)
{
    auto selectedObject = m_sceneManager->selectObject(x, y, m_width, m_height);
#ifdef THIRD_CAMERA
    if (selectedObject)
    {
        auto camera = m_sceneManager->getCurrentCamera();
        if (camera)
        {
            m_cameraManipulator = std::make_unique<ObjectManipulator>(*selectedObject, *camera);
        }
    }
#endif
    return selectedObject;
}

ISceneObject* Engine::getObject(int32_t index)
{
    return m_sceneManager->getObject(index);
}

std::optional<size_t> Engine::getObjectIndex(const ISceneObject* object)
{
    return m_sceneManager->getObjectIndex((const SceneObject*)object);
}

std::vector<int32_t> Engine::getObjectsList()
{
    return m_sceneManager->getObjectsList();
}

std::vector<const Model*> Engine::getModelList() const
{
    return m_resourceLoader->getModelList();
}

void Engine::updateTime(float delta)
{
    m_sceneManager->updateTime(delta);
    m_cameraManipulator->update(delta);
}

std::optional<glm::vec3>
Engine::getTerrainInterscation(int32_t posX, int32_t posY)
{
    return m_sceneManager->getTerrainInterscation(posX, posY, m_width, m_height);
}
