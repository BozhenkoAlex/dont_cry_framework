#pragma once

#include <functional>
#include <memory>

struct GLFWwindow;

class GlfwWindowWrapper
{
public:
    GlfwWindowWrapper(int32_t width, int32_t height, bool isVisible = true);

	~GlfwWindowWrapper();

	void setDrawFunction(std::function<void()> &func);

	void setResizeFunction(std::function<void(GLFWwindow*, int32_t, int32_t)> &func);

	void setKeyFunction(std::function<void(GLFWwindow*, int, int, int, int)>& keyFunc);

	void setPosFunction(std::function<void(GLFWwindow*, double, double)>& posFunc);

	void setMouseButtonFunction(std::function<void(GLFWwindow*, int, int, int)>& mouseFunc);

	void mainLoop();

	GLFWwindow *handler()
	{
		return m_window;
	}

protected:

	std::function<void()> m_drawFunction;
	GLFWwindow *m_window;
};
