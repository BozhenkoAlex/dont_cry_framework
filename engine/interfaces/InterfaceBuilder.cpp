#include "engine.h"

std::unique_ptr<IEngine> IEngine::create(int32_t width, int32_t height)
{
    auto engine = std::make_unique<Engine>(width, height);
    return engine->isInitialized() ? std::move(engine) : nullptr;
}
