#pragma once

#include <memory>
#include <vector>
#include <optional>

#include "ISceneObject.h"

class ICameraManipulator;

class IEngine
{
public:
    static std::unique_ptr<IEngine> create(int32_t width, int32_t height);

    virtual void draw(int32_t selectedObjectId) = 0;
    virtual void resize(int32_t width, int32_t height) = 0;
    virtual void updateTime(float delta) = 0;

    virtual ISceneObject* select(int32_t x, int32_t y) = 0;
    virtual ISceneObject* getObject(int32_t index) = 0;
    virtual std::optional<size_t> getObjectIndex(const ISceneObject* object) = 0;
    virtual std::vector<int32_t> getObjectsList() = 0;
    virtual std::vector<const Model*> getModelList() const = 0;

    // posX and posY are screen pixel position
    virtual std::optional<glm::vec3>
    getTerrainInterscation(int32_t posX, int32_t posY) = 0;

    virtual ICameraManipulator* getCamera() = 0;
};
