#pragma once

#include <string>

#include <glm/glm.hpp>

class Model;

class ISceneObject
{
public:
    const Model* getModel() const { return m_model; }
    void setModel(const Model* model)
    {
        m_model = model;
    }

    void setPosition(const glm::vec3& pos)
    {
        m_position = pos;
        m_needCaluclateMatrix = true;
    }
    void setRotation(const glm::vec3& rotation)
    {
        m_rotation = rotation;
        m_needCaluclateMatrix = true;
    }

    void setScale(const glm::vec3& scale)
    {
        m_scale = scale;
        m_needCaluclateMatrix = true;
    }

    glm::vec3 getScale() const { return m_scale; }
    glm::vec3 getRotation() const { return m_rotation; }
    glm::vec3 getPosition() const { return m_position; }

    virtual void changeAnimation(const std::string& animation) {}
    virtual void moveTo(const glm::vec3& /*pos*/) {}

protected:
	glm::vec3 m_position;
	glm::vec3 m_rotation;
	glm::vec3 m_scale;
	const Model* m_model;
    bool m_needCaluclateMatrix;
};
