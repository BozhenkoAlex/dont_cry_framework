#include "GlfwWindowWrapper.h"

#include <string>
#include <functional>

#include <GLFW/glfw3.h>

GlfwWindowWrapper::GlfwWindowWrapper(
        int32_t width, int32_t height, bool isVisible)
	:
	m_drawFunction(nullptr)
{
	auto error_callback = [](int e, const char *d)
	{
		printf("Error %d: %s\n", e, d);
	};

	/* GLFW */
	glfwSetErrorCallback(error_callback);
    if (!glfwInit())
    {
        fprintf(stderr, "[GFLW] failed to init!\n");
		exit(1);
    }

    std::string title = "Demo";
    glfwWindowHint(GLFW_VISIBLE, isVisible);
    m_window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
    glfwMakeContextCurrent(m_window);
}

GlfwWindowWrapper::~GlfwWindowWrapper()
{
	glfwDestroyWindow(m_window);
	glfwTerminate();
}

void GlfwWindowWrapper::setDrawFunction(std::function<void()>& drawFunc)
{
	m_drawFunction = drawFunc;
}

template <typename Func>
GLFWwindowsizefun lambdaToSizeFunc(Func lambda)
{
	static auto lambda_copy = lambda;
    return [](GLFWwindow*win, int w, int h)
    {
		lambda_copy(win, w, h);
	};
}

void GlfwWindowWrapper::setResizeFunction(std::function<void(GLFWwindow*, int32_t, int32_t)>& resizeFunc)
{
	glfwSetWindowSizeCallback(m_window, lambdaToSizeFunc(resizeFunc));
}

template <typename Func>
GLFWkeyfun lambdaToKeyFunc(Func lambda)
{
	static auto lambda_copy = lambda;
    return [](GLFWwindow*win, int key, int scancode, int action, int mode)
    {
		lambda_copy(win, key, scancode, action, mode);
	};
}

void GlfwWindowWrapper::setKeyFunction(std::function<void(GLFWwindow*, int, int, int, int)>& keyFunc)
{
	glfwSetKeyCallback(m_window, lambdaToKeyFunc(keyFunc));
}

template <typename Func>
GLFWcursorposfun lambdaToPosFunc(Func lambda)
{
	static auto lambda_copy = lambda;
    return [](GLFWwindow* win, double posX, double posY)
    {
		lambda_copy(win, posX, posY);
	};
}

void GlfwWindowWrapper::setPosFunction(std::function<void(GLFWwindow*, double, double)>& posFunc)
{
	glfwSetCursorPosCallback(m_window, lambdaToPosFunc(posFunc));
}

template <typename Func>
GLFWmousebuttonfun lambdaToMouseButtonFunc(Func lambda)
{
	static auto lambda_copy = lambda;
    return [](GLFWwindow* win, int button, int action, int mode)
    {
		lambda_copy(win, button, action, mode);
	};
}
void GlfwWindowWrapper::setMouseButtonFunction(
        std::function<void(GLFWwindow*, int, int, int)>& mouseFunc)
{
	glfwSetMouseButtonCallback(m_window, lambdaToMouseButtonFunc(mouseFunc));
}

void GlfwWindowWrapper::mainLoop()
{
	while (!glfwWindowShouldClose(m_window))
    {
		/* Input */
		glfwPollEvents();

        if (m_drawFunction)
		{
            m_drawFunction();
		}

		glfwSwapBuffers(m_window);
    }
}
