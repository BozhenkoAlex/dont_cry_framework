#pragma once

#include <cstdint>
#include <memory>

enum Directions
{
    e_Left = -2,
    e_Back = -1,
    e_Forward = 1,
    e_Right = 2,
};

class ICameraManipulator
{
public:
    virtual void processKey(int32_t key, bool enable) = 0;

    virtual void rotate(float x, float y) = 0;
    virtual void update(float deltaTime) = 0;
};
