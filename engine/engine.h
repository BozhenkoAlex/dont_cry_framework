#pragma once

#include "interfaces/IEngine.h"
#include <optional>

class ResourceLoader;
class SceneManager;
class EffectManager;

class Engine: public IEngine
{
public:
    explicit Engine(int width, int height);
    virtual ~Engine();

    bool isInitialized() const;

    void draw(int32_t selectedObjectId) override;
    void resize(int32_t width, int32_t height) override;
    void updateTime(float delta) override;

    ISceneObject* select(int32_t x, int32_t y) override;
    ISceneObject* getObject(int32_t index) override;
    std::optional<size_t> getObjectIndex(const ISceneObject* object) override;

    std::vector<int32_t> getObjectsList() override;
    std::vector<const Model*> getModelList() const override;

    ICameraManipulator* getCamera() override
	{
		return m_cameraManipulator.get();
    }

    std::optional<glm::vec3>
    getTerrainInterscation(int32_t posX, int32_t posY) override;

protected:
    bool init(int32_t width, int32_t height);

protected:
    std::unique_ptr<ResourceLoader> m_resourceLoader;
	std::unique_ptr<SceneManager> m_sceneManager;
	std::unique_ptr<EffectManager> m_effectManager;
    std::unique_ptr<ICameraManipulator> m_cameraManipulator;

	int32_t m_width;
	int32_t m_height;

    bool m_isInitialized;
};
