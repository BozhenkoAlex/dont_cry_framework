﻿#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <chrono>

#include "GlfwWindowWrapper.h"

#include "SceneManagement/TerrainObject.h"
#include "ResourceManagment/MultiLayeredHeightmap.h"
#include "Utils/TGA.h"

class Timer
{
public:
    void start()
    {
        m_startPoint = std::chrono::system_clock::now();
    }

    template<class units>
    double getTimeSpent() const
    {
        auto endPoint = std::chrono::system_clock::now();
        return std::chrono::duration_cast<units>(endPoint - m_startPoint).count();
    }
private:
    std::chrono::time_point<std::chrono::system_clock> m_startPoint;
};

std::ostream& operator<<(std::ostream& os, const glm::vec3& vec)
{
    return std::cout << vec.x << " " << vec.y << " " << vec.z;
}

bool isEqual(float val1, float val2, float tolerance = FLT_EPSILON)
{
    return abs(val1 - val2) < tolerance;
}

bool isEqual(glm::vec3 val1, glm::vec3 val2, float tolerance = FLT_EPSILON)
{
    return isEqual(val1.x, val2.x, tolerance) &&
            isEqual(val1.y, val2.y, tolerance) &&
            isEqual(val1.z, val2.z, tolerance);
}

bool testRay(glm::vec3 orig, glm::vec3 dir, glm::vec3 scaleValue, TerrainObject* terrainObj)
{
    const float TOLERANCE = 0.0001; // FLT_EPSILON;

    const glm::vec3 expctedValue = orig - orig.y / dir.y * dir;
    const bool outOfBox = abs(expctedValue.x) > 0.5 * scaleValue.x ||
                          abs(expctedValue.z) > 0.5 * scaleValue.z;

    auto intersectionPoint = terrainObj->getTerrainIntersectedPoint(orig, dir);

    const bool pointMeetExpectation = outOfBox ^ !intersectionPoint.has_value();
    bool result = pointMeetExpectation;
    if (intersectionPoint.has_value())
    {
        result = isEqual(intersectionPoint.value(), expctedValue, TOLERANCE);
    }

    if (!result)
    {
        std::cout << "orig: " << orig << std::endl;
        std::cout << "dir: " << dir << std::endl;
        std::cout << "expctedValue: " << expctedValue << std::endl;
        std::cout << "intersectionPoint: " << intersectionPoint.value_or(glm::vec3(NAN)) << std::endl;
        std::cout << std::endl;
    }

    return result;
}

int main()
{
    // init opengl
    const int32_t DEFAULT_WIDTH = 400;
    const int32_t DEFAULT_HEIGHT = 400;
    auto glfwWindowWrapper =
            std::make_unique<GlfwWindowWrapper>(DEFAULT_WIDTH, DEFAULT_HEIGHT, false);

    /* OpenGL */
    glViewport(0, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    glewExperimental = 1;
    if (glewInit() != GLEW_OK)
    {
        fprintf(stderr, "Failed to setup GLEW\n");
        exit(1);
    }

    // init heightmap
    size_t imageSize = 400;
    auto tgaImage = TGA::Create(imageSize, imageSize, 32);
    auto heightmap = std::make_unique<MultiLayeredHeightmap>();
    bool res = heightmap->create(std::move(tgaImage));
    if (!res)
    {
        return 1;
    }

    uint32_t testsCount = 0;
    uint32_t fallenTestsCount = 0;

    const float SCALE_FACTOR = 20;

    auto terrainObj = std::make_unique<TerrainObject>();
    terrainObj->setModel(heightmap.get());
    glm::vec3 scaleValue(SCALE_FACTOR, 1, SCALE_FACTOR);
    terrainObj->setScale(scaleValue);
    terrainObj->setPosition(glm::vec3(0, 0.5, 0));

    Timer perfTimer;
    perfTimer.start();

    // tests
    float step = 0.05f;
    for (float x = -SCALE_FACTOR; x < 0; x += step)
    {
        for (float y = -SCALE_FACTOR; y < 0; y += step)
        {
            {
                ++testsCount;
                glm::vec3 orig(x, 0.5 * SCALE_FACTOR, y);
                glm::vec3 dir(1, -1, 1);
                bool result = testRay(orig, dir, scaleValue, terrainObj.get());
                if (!result)
                    ++fallenTestsCount;
            }
            {
                ++testsCount;
                glm::vec3 orig(-x, 0.5 * SCALE_FACTOR, -y);
                glm::vec3 dir(-1, -1, -1);
                bool result = testRay(orig, dir, scaleValue, terrainObj.get());
                if (!result)
                    ++fallenTestsCount;
            }
        }
    }


    float angleStep = M_PI / 12; // 15 degrees
    float radius = 10.0;
    for (float angle = 0; angle < 2 * M_PI; angle += angleStep)
    {
        ++testsCount;
        float x = sin(angle);
        float y = cos(angle);
        glm::vec3 orig(radius * x, radius, radius * y);
        glm::vec3 dir(-x, -1, -y);
        bool result = testRay(orig, dir, scaleValue, terrainObj.get());
        if (!result)
            ++fallenTestsCount;
    }

    const double timeSpent = perfTimer.getTimeSpent<std::chrono::milliseconds>() / 1000.0;
    std::cout << "----------------------------" << std::endl
              << "test count: " << testsCount << std::endl
              << "fallen tests: " << fallenTestsCount<< std::endl
              << "time spent: " << timeSpent  << "s" << std::endl
              << "----------------------------" << std::endl;
    return 0;
}
