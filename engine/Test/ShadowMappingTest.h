#pragma once

#include <memory>
#include <string>

#define GLEW_STATIC
#include <GL/glew.h>

#include "Utils/ImageRenderer.h"
#include "ResourceManagment/Shader.h"

namespace test
{
	class ShadowMapping
	{
	public:
		ShadowMapping()
		{
			m_imageRenderer = std::make_unique<ImageRenderer>();
			m_shader = std::make_unique<Shader>();
			m_shader->load(VERTEX_SHADER, FRAGMENT_SHADER);
		}

		void draw(GLuint texture)
		{
			m_shader->useProgram();

			m_shader->uniformTextures({ texture });

			m_imageRenderer->bind();
			m_shader->uniformValues();
			m_imageRenderer->draw();
			m_imageRenderer->unbind();
		}


	protected:
		std::string VERTEX_SHADER =
"#version 150 \n \
\n \
attribute vec3 a_posL; \n \
\n \
varying vec2 v_uv; \n \
\n \
void main() \n \
{ \n \
	gl_Position = vec4(a_posL.xy, 0.0, 1.0); \n \
	v_uv = 0.5 * (a_posL.xy + 1.0); \n \
}";


		std::string FRAGMENT_SHADER =
"#version 150 \n \
precision highp float; \n \
\n \
varying vec2 v_uv; \n \
\n \
uniform sampler2D u_s_textures[1]; \n \
void main() \n \
{ \n \
	gl_FragColor = texture2D(u_s_textures[0], v_uv ); \n \
}";

	protected:

		std::unique_ptr<ImageRenderer> m_imageRenderer;
		std::unique_ptr<Shader> m_shader;
	};
}