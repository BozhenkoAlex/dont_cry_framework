#include "AssimpModel.h"

#include <algorithm>
#include <cctype>

#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

#include "ResourceManagment/TextureSampler.h"

void LoadMesh(const aiNode* node, const aiScene* scene,
              std::vector<std::unique_ptr<AssimpMesh>> &meshes,
              std::vector<std::shared_ptr<Texture>> &textures)
{
    for (size_t i = 0; i < node->mNumMeshes; i++)
    {
        aiMesh* assimpMesh = scene->mMeshes[node->mMeshes[i]];
        auto mesh = AssimpMesh::processMesh(assimpMesh);
        if (assimpMesh->mMaterialIndex < textures.size())
        {
            mesh->setTexture(textures[assimpMesh->mMaterialIndex]);
        }

        meshes.emplace_back(std::move(mesh));
    }

    for (size_t i = 0; i < node->mNumChildren; i++)
    {
        LoadMesh(node->mChildren[i], scene, meshes, textures);
    }
}

AssimpModel::AssimpModel()
    :
    Model()
{
}

std::vector<std::shared_ptr<Texture>> LoadTextures(const aiScene* scene, const std::string& pathToResources)
{
    std::vector<std::shared_ptr<Texture>> textures;
    for (size_t i = 0; i < scene->mNumMaterials; ++i)
    {
        auto pTexture = scene->mMaterials[i];

        aiString texturePath;
        auto texturesType = { aiTextureType_DIFFUSE, aiTextureType_SPECULAR };
        for (auto& type : texturesType)
        {
            if (aiReturn_SUCCESS == aiGetMaterialTexture(pTexture, type, 0, &texturePath))
            {
                auto texture = std::make_shared<TextureSampler>();
                texture->load(pathToResources + texturePath.C_Str(), GL_CLAMP_TO_EDGE);
                textures.emplace_back(std::move(texture));
            }
        }
    }
    return textures;
}

void AssimpModel::updateTime(const std::string& animationName, float time) const
{
    if (m_animations.count(animationName))
    {
        m_animations.at(animationName)->updateMeshes(m_meshes, time);
        return;
    }

    for (auto& [name, animation] : m_animations)
    {
        animation->updateMeshes(m_meshes, time);
        return;
    }
}

void AssimpModel::draw(const Shader* shader) const
{
    for (size_t i = 0; i < this->m_meshes.size(); i++)
    {
        m_meshes[i]->draw(shader);
    }
}

bool AssimpModel::load(const std::string& path)
{
    auto flag =
        // aiProcess_LimitBoneWeights |			// limit bone weights to 4 per vertex
        aiProcess_GenSmoothNormals |            // generate smooth normal vectors if not existing
        aiProcess_Triangulate;                  // triangulate polygons with more than 3 edges

    Assimp::Importer importer;
    auto scene = importer.ReadFile(path, flag);
    if (!scene || !scene->mRootNode)
    {
        return false;
    }

    size_t pathIndex = path.find_last_of("/");
    std::string pathToResources = path.substr(0, pathIndex + 1);
    auto textures = LoadTextures(scene, pathToResources);

    LoadMesh(scene->mRootNode, scene, m_meshes, textures);

    m_minBound = glm::vec3(std::numeric_limits<float>::max());
    m_maxBound = glm::vec3(std::numeric_limits<float>::min());
    for (auto& mesh: m_meshes)
    {
        auto minBound = mesh->getMinBound();
        auto maxBound = mesh->getMaxBound();
        for (size_t i = 0; i < 3; ++i)
        {
            m_minBound[i] = std::min(m_minBound[i], minBound[i]);
            m_maxBound[i] = std::max(m_maxBound[i], maxBound[i]);
        }
    }

    glm::vec3 boundSize = 0.5f * (m_maxBound - m_minBound);
    for (int i = 0; i< 3; ++i)
    {
        m_boundRadius = std::max(m_boundRadius, boundSize[i]);
    }

    for (size_t i = 0; i < scene->mNumAnimations; ++i)
    {
        auto pAnimation = scene->mAnimations[i];
        auto animation = std::make_unique<AssimpAnimation>();
        animation->load(pAnimation->mTicksPerSecond, pAnimation->mDuration, scene->mRootNode, pAnimation);

        size_t typeIndex = path.find_last_of(".");
        std::string name = path.substr(pathToResources.size(), typeIndex - pathToResources.size());

        std::transform(name.begin(), name.end(), name.begin(),
            [](unsigned char c){ return std::tolower(c); });

        m_animations[name] = std::move(animation);
    }

    return true;
}

bool AssimpModel::intersectBound(const glm::mat4& modelMatrix,
                                 const glm::vec3& orig,
                                 const glm::vec3& dir,
                                 float &shortestDistance) const
{
    float distance = -1;
    bool intersectionFound = false;
    for(auto& mesh: m_meshes)
    {
        bool isIntersected = mesh->intersectBound(modelMatrix, orig, dir, distance);
        if (!isIntersected)
        {
            continue;
        }

        if (intersectionFound)
        {
            shortestDistance = std::min(distance, shortestDistance);
        }
        else
        {
            intersectionFound = true;
            shortestDistance = distance;
        }
    }

    return intersectionFound;
}

glm::mat4 AssimpModel::getRootTransform() const
{
    return m_meshes[0]->getRootTransform();
}

