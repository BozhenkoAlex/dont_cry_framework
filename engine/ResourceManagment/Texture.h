#pragma once

#include <string>

#define GLEW_STATIC
#include <GL/glew.h>

class Texture
{
public:
	Texture()
	{
		glGenTextures(1, &m_texture);
	}

	virtual ~Texture()
	{
		glDeleteTextures(1, &m_texture);
	}

	virtual bool load(const std::string& FileName, GLenum tiling) = 0;

	GLuint getTexture() const
	{
		return m_texture;
	}

protected:
    GLuint m_texture;
	GLuint m_textureTarget;
};

