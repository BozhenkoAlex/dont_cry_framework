#include "AssimpAnimation.h"

#include <functional>
#include <map>

#include "AssimpMesh.h"

#include "AssimpMathUtils.h"

struct KeyFrame
{
    std::map<double, glm::vec3> scales;
    std::map<double, glm::vec3> positions;
    std::map<double, glm::quat> rotates;
};

struct AnimationNode
{
    std::string name;
    glm::mat4 transform;

    std::unique_ptr<KeyFrame> keyFrame;

    std::vector<std::unique_ptr<AnimationNode>> children;
};

template<class Type>
Type calcInterpolated(double animationTime, const std::map<double, Type>& keys,
                      std::function<Type(const Type&, const Type&, float)> &interpolate)
{
    if (keys.size() == 1)
    {
        return keys.begin()->second;
    }

    auto lower = keys.upper_bound(animationTime);
    if (lower == keys.begin())
    {
        return lower->second;
    }

    auto upper = lower--;

    double deltaTime = (upper->first - lower->first);
    double factor = (animationTime - lower->first) / deltaTime;
    assert(factor >= 0.0 && factor <= 1.0);

    return interpolate(lower->second, upper->second, static_cast<float>(factor));
}

glm::mat4 getKeyFrameTransformMatrix(const KeyFrame* keyFrame, float animationTime)
{
    std::function interpolateVector = [](const glm::vec3& start, const glm::vec3& end, float factor)
    {
        glm::vec3 delta = end - start;
        return (start + delta * factor);
    };

    std::function interpolateQuaternion = [](const glm::quat& start, const glm::quat& end, float factor)
    {
        auto result = glm::lerp(start, end, factor);
        return glm::normalize(result);
    };

    auto scalingVec = calcInterpolated<glm::vec3>(animationTime, keyFrame->scales, interpolateVector);
    auto RotationQ = calcInterpolated<glm::quat>(animationTime, keyFrame->rotates, interpolateQuaternion);
    auto translationVec = calcInterpolated<glm::vec3>(animationTime, keyFrame->positions, interpolateVector) / scalingVec;

    glm::mat4 resultMatrix(1);
    resultMatrix = glm::scale(resultMatrix, scalingVec);
    resultMatrix = resultMatrix * glm::toMat4(RotationQ);
    resultMatrix[3][0] = translationVec.x;
    resultMatrix[3][1] = translationVec.y;
    resultMatrix[3][2] = translationVec.z;

    return glm::transpose(resultMatrix);
}

void ReadNodeHeirarchy(double animationTime, const glm::mat4& ParentTransform, const AnimationNode* animationNode, const std::vector<std::unique_ptr<AssimpMesh>>&meshes)
{
    glm::mat4 GlobalTransformation = animationNode->transform;
    if (animationNode->keyFrame)
    {
        GlobalTransformation = getKeyFrameTransformMatrix(animationNode->keyFrame.get(), animationTime);
    }

    GlobalTransformation = GlobalTransformation *  ParentTransform;
    for (auto& mesh : meshes)
    {
        mesh->setBoneTransform(animationNode->name, GlobalTransformation);
    }

    for (auto& child : animationNode->children)
    {
        ReadNodeHeirarchy(animationTime, GlobalTransformation, child.get(), meshes);
    }
}

// Animation Loading
aiNodeAnim * FindNodeAnim(const aiAnimation * pAnimation, const aiString & NodeName)
{
    for (size_t i = 0; i < pAnimation->mNumChannels; i++)
    {
        aiNodeAnim* pNodeAnim = pAnimation->mChannels[i];
        if (pNodeAnim->mNodeName == NodeName)
        {
            return pNodeAnim;
        }
    }

    return nullptr;
}

std::unique_ptr<AnimationNode> LoadAnimation(const aiNode * pNode, const aiAnimation* pAnimation)
{
    auto node = std::make_unique<AnimationNode>();
    node->transform = assimpMatrixToGlm(pNode->mTransformation);
    node->name = pNode->mName.C_Str();

    aiNodeAnim* pNodeAnim = FindNodeAnim(pAnimation, pNode->mName);
    if (pNodeAnim)
    {
        node->keyFrame = std::make_unique<KeyFrame>();

        for (size_t i = 0; i < pNodeAnim->mNumScalingKeys; ++i)
        {
            double time = pNodeAnim->mScalingKeys[i].mTime;
            auto value = assimpVecToGlm(pNodeAnim->mScalingKeys[i].mValue);
            node->keyFrame->scales.insert({ time, value });
        }

        for (size_t i = 0; i < pNodeAnim->mNumRotationKeys; ++i)
        {
            double time = pNodeAnim->mRotationKeys[i].mTime;
            auto value = assimpQuatToGlm(pNodeAnim->mRotationKeys[i].mValue);
            node->keyFrame->rotates.insert({ time, value });
        }

        for (size_t i = 0; i < pNodeAnim->mNumPositionKeys; ++i)
        {
            double time = pNodeAnim->mPositionKeys[i].mTime;
            auto value = assimpVecToGlm(pNodeAnim->mPositionKeys[i].mValue);
            node->keyFrame->positions.insert({ time, value });
        }
    }

    for (size_t i = 0; i < pNode->mNumChildren; i++)
    {
        node->children.push_back(LoadAnimation(pNode->mChildren[i], pAnimation));
    }

    return node;
}

AssimpAnimation::AssimpAnimation()
    :
    m_rootNode(nullptr)
    , m_ticksPerSecond(0)
    , m_duration(0)
{
}

void AssimpAnimation::load(double ticksPerSecond, double duration, const aiNode * pNode, const aiAnimation* pAnimation)
{
    this->m_ticksPerSecond = ticksPerSecond;
    this->m_duration = duration;
    this->m_rootNode = LoadAnimation(pNode, pAnimation);
}

AssimpAnimation::~AssimpAnimation()
{
    m_rootNode = nullptr;
}

void AssimpAnimation::updateMeshes(const std::vector<std::unique_ptr<AssimpMesh>>& meshes, double time) const
{
    double animationTime = getAnimationTime(time);
    ReadNodeHeirarchy(animationTime, glm::mat4(1), m_rootNode.get(), meshes);
}

glm::mat4 AssimpAnimation::getRootTransform() const
{
    return m_rootNode ? m_rootNode->transform : glm::mat4(1);
}

double AssimpAnimation::getAnimationTime(double time) const
{
    double ticksPerSecond = m_ticksPerSecond != 0 ? m_ticksPerSecond : 25.0;
    double timeInTicks = time * ticksPerSecond;
    return fmod(timeInTicks, m_duration);
}
