#include "Sphere.h"

#include <iomanip>
#include <cmath>

#include <glm/glm.hpp>
#include <glm/gtx/intersect.hpp>

#include "Shader.h"
#include "Utils/Vertex.h"

const int MIN_SECTOR_COUNT = 3;
const int MIN_STACK_COUNT  = 2;

#ifndef M_PI
    #define M_PI 3.14159265358979323846
    #define M_PI_2 0.5 * M_PI
#endif

SphereBound::SphereBound(float radius, int sectors, int stacks)
{
    sectors = std::max(MIN_SECTOR_COUNT, sectors);
    stacks = std::max(MIN_STACK_COUNT, stacks);

    buildVerticesSmooth(radius, sectors, stacks);
    buildIndices(sectors, stacks);
}

///////////////////////////////////////////////////////////////////////////////
// build vertices of sphere with smooth shading using parametric equation
// x = r * cos(u) * cos(v)
// y = r * cos(u) * sin(v)
// z = r * sin(u)
// where u: stack(latitude) angle (-90 <= u <= 90)
//       v: sector(longitude) angle (0 <= v <= 360)
///////////////////////////////////////////////////////////////////////////////
void SphereBound::buildVerticesSmooth(float radius, int sectorCount, int stackCount)
{
    float lengthInv = 1.0f / radius;    // normal

    float sectorStep = 2 * M_PI / sectorCount;
    float stackStep = M_PI / stackCount;

    std::vector<Vertex> outputVertices;
    outputVertices.reserve((stackCount + 1) * (sectorCount + 1));
    for(int i = 0; i <= stackCount; ++i)
    {
        float stackAngle = M_PI_2 - i * stackStep;        // starting from pi/2 to -pi/2
        float xy = radius * cosf(stackAngle);             // r * cos(u)
        float z = radius * sinf(stackAngle);              // r * sin(u)

        // add (sectorCount+1) vertices per stack
        // the first and last vertices have same position and normal, but different tex coords
        for(int j = 0; j <= sectorCount; ++j)
        {
            float sectorAngle = j * sectorStep;           // starting from 0 to 2pi

            Vertex vertex;
            // vertex position
            float x = xy * cosf(sectorAngle);             // r * cos(u) * cos(v)
            float y = xy * sinf(sectorAngle);             // r * cos(u) * sin(v)
            vertex.position = glm::vec3(x, y, z);

            // normalized vertex normal
            vertex.normal = lengthInv * vertex.position;

            // vertex tex coord between [0, 1]
            float s = (float)j / sectorCount;
            float t = (float)i / stackCount;
            vertex.uv = glm::vec2(s, t);

            outputVertices.emplace_back(vertex);
        }
    }

    glGenBuffers(1, &m_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * outputVertices.size(), outputVertices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void SphereBound::buildIndices(int sectorCount, int stackCount)
{
    m_indeces.reserve(3 * 2 * stackCount * sectorCount);
    m_lineIndices.reserve(2 * stackCount * sectorCount);
    //  indices
    //  k1--k1+1
    //  |  / |
    //  | /  |
    //  k2--k2+1
    for(int i = 0; i < stackCount; ++i)
    {
        uint32_t k1 = i * (sectorCount + 1);     // beginning of current stack
        uint32_t k2 = k1 + sectorCount + 1;      // beginning of next stack

        for(int j = 0; j < sectorCount; ++j, ++k1, ++k2)
        {
            // 2 triangles per sector excluding 1st and last stacks
            if(i != 0)
            {
                // k1---k2---k1+1
                m_indeces.emplace_back(k1);
                m_indeces.emplace_back(k2);
                m_indeces.emplace_back(k1 + 1);
            }

            if(i != (stackCount-1))
            {
                // k1+1---k2---k2+1
                m_indeces.emplace_back(k1 + 1);
                m_indeces.emplace_back(k2);
                m_indeces.emplace_back(k2 + 1);
            }

            // vertical lines for all stacks
            m_lineIndices.emplace_back(k1);
            m_lineIndices.emplace_back(k2);
            if(i != 0)  // horizontal lines except 1st stack
            {
                m_lineIndices.emplace_back(k1);
                m_lineIndices.emplace_back(k1 + 1);
            }
        }
    }

    glGenBuffers(1, &m_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * m_lineIndices.size(), m_lineIndices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void SphereBound::draw(const Shader* shader) const
{
    if (!shader)
    {
        return;
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);

    shader->uniformValues();

//    glDrawElements(GL_TRIANGLES, m_indeces.size(), GL_UNSIGNED_INT, m_indeces.data());
    glDrawElements(GL_LINES, m_lineIndices.size(), GL_UNSIGNED_INT, m_lineIndices.data());

    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

bool SphereBound::intersectBound(const glm::mat4& transform,
                             const glm::vec3& minBound,
                             const glm::vec3& maxBound,
                             const glm::vec3& orig,
                             const glm::vec3& dir,
                             float& distance)
{
    glm::vec3 translatedMin = glm::vec4(minBound, 1.0f) * transform;
    glm::vec3 translatedMax = glm::vec4(maxBound, 1.0f) * transform;

    glm::vec3 sphereCenter = (translatedMax + translatedMin) * 0.5f;
    float radius = glm::length(0.5f * (translatedMax - translatedMin));

    return glm::intersectRaySphere(orig,
                                   dir,
                                   sphereCenter,
                                   radius,
                                   distance);
}

