﻿#include "Model.h"

#include <array>
#include <algorithm>
#include <numeric>

#include <glm/gtx/intersect.hpp>

#include "Shader.h"
#include "Sphere.h"
#include "Texture.h"

Model::Model()
{
    m_shader = m_borderShader = nullptr;
    m_minBound = m_maxBound = glm::vec3(NAN);
    m_boundRadius = 0;

    m_indexBuffer = m_vertexBuffer = 0;

    m_drawFlag = GL_TRIANGLES;
}

Model::~Model()
{
    glDeleteBuffers(1, &m_indexBuffer);
    glDeleteBuffers(1, &m_vertexBuffer);
}

bool Model::load(const std::string &modelFilename)
{
    FILE* pFile = NULL;
    fopen_s(&pFile, modelFilename.c_str(), "r");

    if (nullptr == pFile)
    {
        return false;
    }

    int32_t numVertices = 0;
    fscanf_s(pFile, "%*s %d", &numVertices);

    m_verticesData.resize(numVertices);

    m_minBound = glm::vec3(std::numeric_limits<float>::max());
    m_maxBound = glm::vec3(std::numeric_limits<float>::min());

    for (int32_t i = 0; i < numVertices; ++i)
    {
        fscanf_s(pFile,
            "%*d. pos:[%f, %f, %f]; norm:[%f, %f, %f]; binorm:[%*f, %*f, %*f]; tgt:[%*f, %*f, %*f]; uv:[%f, %f];",
            &m_verticesData[i].position.x, &m_verticesData[i].position.y, &m_verticesData[i].position.z,
            &m_verticesData[i].normal.x, &m_verticesData[i].normal.y, &m_verticesData[i].normal.z,
            &m_verticesData[i].uv.x, &m_verticesData[i].uv.y);

        for (size_t j = 0; j < 3; ++j)
        {
            m_minBound[j] = std::min(m_minBound[j], m_verticesData[i].position[j]);
            m_maxBound[j] = std::max(m_maxBound[j], m_verticesData[i].position[j]);
        }
    }

    glm::vec3 boundSize = 0.5f * (m_maxBound - m_minBound);
    for (int i = 0; i<3; ++i)
    {
        m_boundRadius = std::max(m_boundRadius, boundSize[i]);
    }

    int32_t noIndeces = 0;
    fscanf_s(pFile, "%*s %d", &noIndeces);
    m_indeces.resize(noIndeces);

    for (int32_t i = 0; i < noIndeces; i += 3)
    {
        fscanf_s(pFile,
            " %*d. %d, %d, %d ",
            &m_indeces[i], &m_indeces[i + 1], &m_indeces[i + 2]);
    }

    glGenBuffers(1, &m_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * m_indeces.size(), m_indeces.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glGenBuffers(1, &m_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_verticesData.size(), m_verticesData.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    fclose(pFile);

    return true;
}

void Model::draw(const Shader* shader) const
{
    if (!shader)
    {
        return;
    }

    uniformTextures(shader);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);

    shader->uniformValues();

    glDrawElements(
        m_drawFlag, static_cast<GLsizei>(m_indeces.size()), GL_UNSIGNED_INT, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

bool Model::intersectBound(const glm::mat4& modelMatrix,
                            const glm::vec3& orig,
                            const glm::vec3& dir,
                            float& distance) const
{
    return SphereBound::intersectBound(modelMatrix, m_minBound, m_maxBound, orig, dir, distance);
}

//float Model::intersect(const glm::mat4& modelMatrix, const glm::vec3& orig, const glm::vec3& dir) const
//{
//    glm::vec2 baryPoint;
//    float distance;

//    size_t trianglesNum = m_indeces.size() / 3;
//    for (size_t i = 0; i < trianglesNum; ++i)
//    {
//        std::array<size_t, 3> indexes = { 3 * i, 3 * i + 1, 3 * i + 2 };
//        std::array<glm::vec3, 3> triangle = getTriangle(modelMatrix, indexes);

//        bool result = glm::intersectRayTriangle(orig, dir,
//                                                triangle[0], triangle[1], triangle[2],
//                                                baryPoint, distance);

//        if (result && distance > 0.0f)
//        {
//            return distance;
//        }
//    }

//    return -1.0;
//}

std::array<glm::vec3, 3> Model::getTriangle(const glm::mat4& modelMatrix, const std::array<size_t, 3> &indexes) const
{
    std::array<glm::vec3, 3>triangle;
    for (size_t i = 0; i < triangle.size(); ++i)
    {
        auto index = m_indeces[indexes[i]];
        const auto& pos = m_verticesData[index].position;
        triangle[i] = glm::vec4(pos, 1.0f) * modelMatrix;
    }
    return triangle;
}

void Model::uniformTextures(const Shader* shader) const
{
    GLint cubeTexture = -1;

    std::vector<GLuint> textures;
    for (auto& textureHolder : m_textureHolderList)
    {
        textures.push_back(textureHolder->getTexture());
    }

    if (!m_cubeTexturesList.empty())
    {
        auto cubeTextureHolder = m_cubeTexturesList.begin();
        cubeTexture = (*cubeTextureHolder)->getTexture();
    }

    shader->uniformTextures(textures);
    shader->uniformCubeTexture(cubeTexture);
}

void Model::setShader(const Shader* shader)
{
    m_shader = shader;
}

const Shader* Model::getShader() const
{
    return m_shader;
}

const Shader* Model::getBorderShader() const
{
    return m_borderShader;
}

void Model::setBorderShader(const Shader* shader)
{
    m_borderShader = shader;
}

void Model::setTextures(const std::vector<const Texture*> &textures)
{
    m_textureHolderList = textures;
}

void Model::setCubeTextures(const std::vector<const Texture*>& textures)
{
    m_cubeTexturesList = textures;
}
