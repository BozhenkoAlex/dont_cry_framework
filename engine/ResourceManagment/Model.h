#pragma once

#include <string>
#include <vector>
#include <array>

#define GLEW_STATIC
#include <GL/glew.h>

#include <glm/glm.hpp>

#include "Utils/Vertex.h"

class Shader;
class Texture;

class Model
{
public:
    Model();

    virtual ~Model();

    virtual bool load(const std::string &modelFilename);

    virtual void draw(const Shader* shader) const;

    virtual bool intersectBound(const glm::mat4& modelMatrix,
                                const glm::vec3& orig,
                                const glm::vec3& dir,
                                float &distance) const;

//    virtual float intersect(const glm::mat4& modelMatrix,
//                            const glm::vec3& orig, const glm::vec3& dir) const;

    glm::vec3 getObjectCenter() const
    {
        return 0.5f * (m_minBound + m_maxBound);
    }

    glm::vec3 getMinBound() const
    {
        return m_minBound;
    }

    glm::vec3 getMaxBound() const
    {
        return m_maxBound;
    }

    float getBoundRadius() const
    {
        return m_boundRadius;
    }

    void setShader(const Shader* shader);
    void setBorderShader(const Shader* shader);

    const Shader* getShader() const;
    const Shader* getBorderShader() const;

    void setTextures(const std::vector<const Texture*>& textureHolder);
    void setCubeTextures(const std::vector<const Texture*>& textureHolder);

protected:

    void uniformTextures(const Shader* shader) const;

    std::array<glm::vec3, 3>
    getTriangle(const glm::mat4& modelMatrix, const std::array<size_t, 3> &indexes) const;

protected:

    glm::vec3 m_minBound;
    glm::vec3 m_maxBound;
    float m_boundRadius;

    GLuint m_indexBuffer;
    GLuint m_vertexBuffer;

    std::vector<Vertex> m_verticesData;
    std::vector<uint32_t> m_indeces;

    const Shader* m_shader;
    const Shader* m_borderShader;

    GLint m_drawFlag;

    std::vector<const Texture*>m_textureHolderList;
    std::vector<const Texture*> m_cubeTexturesList;
};
