﻿#include "ResourceLoader.h"

#include <fstream>

#include "CubeTexture.h"
#include "TextureSampler.h"
#include "AssimpModel.h"

#include "Model.h"
#include "Shader.h"
#include "MultiLayeredHeightmap.h"

using json = nlohmann::json;

ResourceLoader::ResourceLoader()
{
}

ResourceLoader::~ResourceLoader()
{
	m_animations.clear();

	m_models.clear();

	m_heightmaps.clear();

	for (auto& shader : m_shaders)
		delete shader.second;
	m_shaders.clear();

	for (auto& shader : m_borderShaders)
		delete shader.second;
	m_borderShaders.clear();

	for (auto& texture : m_textureHolders)
		delete texture.second;
	m_textureHolders.clear();

	for (auto& cubeTexture : m_cubeTextures)
		delete cubeTexture.second;
	m_cubeTextures.clear();
}

std::vector<const Texture*>
ResourceLoader::getTextureSamplers(const json& settings) const
{
    std::vector<const Texture*> textures;
    if (settings.find("2Dtex") != settings.end())
    {
        std::vector<int32_t> ids = settings["2Dtex"];
        for(auto& id: ids)
        {
            textures.emplace_back(getTextureSampler(id));
        }
    }

    return textures;
}

std::vector<const Texture*>
ResourceLoader::getCubeTextures(const json& settings) const
{
    std::vector<const Texture*> textures;
    if (settings.find("cubeTex") != settings.end())
    {
        std::vector<int32_t> ids = settings["cubeTex"];
        for(auto& id: ids)
        {
            textures.emplace_back(getCubeTexture(id));
        }
    }

    return textures;
}

const Texture* ResourceLoader::getCubeTexture(int32_t id) const
{
    assert(m_cubeTextures.count(id));
    return m_cubeTextures.count(id) ? m_cubeTextures.at(id) : nullptr;
}

const Texture* ResourceLoader::getTextureSampler(int32_t id) const
{
    assert(m_textureHolders.count(id));
    return m_textureHolders.count(id) ? m_textureHolders.at(id) : nullptr;
}

int32_t ResourceLoader::init(const std::string& configFilename)
{
	std::ifstream iStream(configFilename);
	if (!iStream.is_open())
	{
		return -1;
	}

	json rootJson;
	iStream >> rootJson;

	std::string pathToShaders;
	if (rootJson.find("pathToShaders") != rootJson.end())
	{
		pathToShaders = rootJson["pathToShaders"];
    }

    if (rootJson.find("shaders") != rootJson.end())
	{
		json settings = rootJson["shaders"];
		m_shaders = initShaders(settings, pathToShaders);
	}

	if (rootJson.find("borderShader") != rootJson.end())
	{
		json settings = rootJson["borderShader"];
		m_borderShaders = initShaders(settings, pathToShaders);
	}

	if (rootJson.find("2DTextures") != rootJson.end())
	{
		json settings = rootJson["2DTextures"];
		m_textureHolders = initTextures<TextureSampler>(settings);
	}

	glEnable(GL_TEXTURE_CUBE_MAP);
	if (rootJson.find("cubeTextures") != rootJson.end())
	{
		json settings = rootJson["cubeTextures"];
		m_cubeTextures = initTextures<CubeTexture>(settings);
	}
	glDisable(GL_TEXTURE_CUBE_MAP);

	if (rootJson.find("heightmaps") != rootJson.end())
	{
		json settings = rootJson["heightmaps"];
		initHeightmaps(settings);
	}

	if (rootJson.find("animations") != rootJson.end())
	{
		json settings = rootJson["animations"];
        m_animations = initModels<AssimpModel>(settings);
	}

	if (rootJson.find("models") != rootJson.end())
	{
		json settings = rootJson["models"];
		m_models = initModels<Model>(settings);
	}

	return 0;
}

const Shader* ResourceLoader::getShader(int32_t id) const
{
	return m_shaders.count(id) ?
		m_shaders.at(id) : nullptr;
}

int32_t ResourceLoader::getShaderId(const Shader* shader) const
{
	auto compareFunc = [shader](const std::pair<const int32_t, const Shader*>& pair) -> bool
	{
		return pair.second == shader;
	};
	auto findResult = std::find_if(m_shaders.begin(), m_shaders.end(), compareFunc);

	if (findResult != m_shaders.end())
		return findResult->first;
	return -1;
}

const Shader* ResourceLoader::getBorderShader(int32_t id) const
{
	return m_borderShaders.count(id) ?
		m_borderShaders.at(id) : nullptr;
}

std::vector<const Model*> ResourceLoader::getModelList() const
{
	std::vector<const Model*> list;
	list.reserve(m_models.size());
	for (const auto& item : m_models)
	{
		if (item.second)
		{
			list.emplace_back(item.second.get());
		}
	}
	return list;
}

template<>
const Model* ResourceLoader::getModel(int32_t id) const
{
	return m_models.count(id) ?
		m_models.at(id).get() : nullptr;
}

template<>
const AssimpModel* ResourceLoader::getModel(int32_t id) const
{
	return m_animations.count(id) ?
		m_animations.at(id).get() : nullptr;
}

template<>
const MultiLayeredHeightmap* ResourceLoader::getModel(int32_t id) const
{
	return m_heightmaps.count(id) ?
		m_heightmaps.at(id).get() : nullptr;
}

template<>
int32_t ResourceLoader::getModelId(const Model* model) const
{
	return getModelId(model, m_models);
}

template<>
int32_t ResourceLoader::getModelId(const AssimpModel* model) const
{
	return getModelId(model, m_animations);
}

template<>
int32_t ResourceLoader::getModelId(const MultiLayeredHeightmap* model) const
{
	return getModelId(model, m_heightmaps);
}

template<class Type>
int32_t ResourceLoader::getModelId(const Type* model, const std::unordered_map<int32_t, std::unique_ptr<Type>>& models) const
{
	auto compareFunc = [model](const std::pair<const int32_t, std::unique_ptr<Type>>& pair) -> bool
	{
		return pair.second.get() == model;
	};
	auto findResult = std::find_if(models.begin(), models.end(), compareFunc);

	if (findResult != models.end())
		return findResult->first;
	return -1;
}

template<class Type>
std::unordered_map<int32_t, std::unique_ptr<Type>>
ResourceLoader::initModels(const nlohmann::json& settings)
{
    std::unordered_map<int32_t, std::unique_ptr<Type>> models;

    for (auto& value : settings)
    {
        auto model = std::make_unique<Type>();
        if (value.find("files") != value.end() &&
            value["files"].is_array())
        {
            for(auto& filename: value["files"])
            {
                bool res = model->load(filename);
                if (!res)
                {
                    continue;
                }
            }
        }

        int32_t id = -1;
        if (value.find("ID") != value.end())
        {
            id = value["ID"];
        }

        if(value.find("shader") != value.end())
        {
            int32_t shaderId = value["shader"];
            auto shader = getShader(shaderId);
            model->setShader(shader);
        }

        if (value.find("borderShader") != value.end())
        {
            int32_t shaderId = value["borderShader"];
            auto borderShader = getBorderShader(shaderId);
            model->setBorderShader(borderShader);
        }

        auto textures = getTextureSamplers(value);
        model->setTextures(textures);

        auto cubeTextures = getCubeTextures(value);
        model->setCubeTextures(cubeTextures);

        models[id] = std::move(model);
    }

    return models;
}

std::unordered_map<int32_t, Shader*>
ResourceLoader::initShaders(const json& settings, const std::string& path)
{
	std::unordered_map<int32_t, Shader*> shaders;

	for (auto& value : settings)
	{
        int32_t id = value.contains("ID") ? static_cast<int32_t>(value["ID"]) : -1;
        std::string fsFilename = value.contains("FS") ? value["FS"]: "";
        std::string vsFilename = value.contains("VS") ? value["VS"]: "";

		auto shader = new Shader();
        int32_t ret = shader->loadFromFile(path + vsFilename, path + fsFilename);
		if (ret < 0)
		{
			delete shader;
			continue;
		}

		shaders[id] = shader;
	}

	return shaders;
}

void ResourceLoader::initHeightmaps(const json& settings)
{
	for (auto& value : settings)
	{
		int32_t id = value["ID"];

		std::string filename = value["file"];

		auto heightmap = std::make_unique<MultiLayeredHeightmap>();
		bool res = heightmap->load(filename);
		if (!res)
		{
			continue;
		}

		{
			int32_t shaderId = value["shader"];
			auto shader = getShader(shaderId);
			heightmap->setShader(shader);
		}

		{
			int32_t shaderId = value["borderShader"];
			auto shader = getBorderShader(shaderId);
			heightmap->setBorderShader(shader);
        }

        auto textures = getTextureSamplers(value);
		heightmap->setTextures(textures);

		m_heightmaps[id] = std::move(heightmap);
	}
}

template<typename Type>
std::unordered_map<int32_t, Texture*>
ResourceLoader::initTextures(const json& settings)
{
    std::unordered_map<int32_t, Texture*> textures;
	for (auto& value : settings)
	{
		int32_t id = value["ID"];

		std::string filename = value["file"];

		std::string textureTilingValue = value["tiling"];
		GLenum textureTiling = GL_CLAMP_TO_EDGE;
		if (textureTilingValue == "CLAMP_TO_EDGE")
			textureTiling = GL_CLAMP_TO_EDGE;
		else if (textureTilingValue == "REPEAT")
			textureTiling = GL_REPEAT;

		auto texture = new Type();
		int32_t res = texture->load(filename, textureTiling);
		if (res < 0)
		{
			delete texture;
		}
		else
		{
			textures[id] = texture;
		}
	}
	return textures;
}

std::vector<int32_t>
ResourceLoader::getTextureIds(const std::vector<const Texture*>& textures) const
{
	std::vector<int32_t> ids;
    for (auto& [id, textureHander]: m_textureHolders)
	{
		for (auto& texture : textures)
		{
            if (texture == textureHander)
			{
                ids.emplace_back(id);
				continue;
			}
		}
	}

	return ids;
}
