#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

#include "Texture.h"

class TextureSampler : public Texture
{
public:
	bool load(const std::string& filename, GLenum tiling) override;
};

