#pragma once

#include <string>
#include <vector>

#include "AssimpMesh.h"
#include "AssimpAnimation.h"

#include "Model.h"

class Shader;

class AssimpModel : public Model
{
public:
    AssimpModel();

    void draw(const Shader* shader) const override;

    void updateTime(const std::string& animationName, float time) const;

    bool load(const std::string& path) override;

    bool intersectBound(const glm::mat4& modelMatrix,
                        const glm::vec3& orig,
                        const glm::vec3& dir,
                        float &distance) const override;

    glm::mat4 getRootTransform() const;

    void setCurrentAnimation(const std::string& animation)
    {
        m_currentAnimationName = animation;
    }

private:
    using AnimationsMap = std::unordered_map<
        std::string, std::unique_ptr<AssimpAnimation>>;
    std::vector<std::unique_ptr<AssimpMesh>> m_meshes;
    AnimationsMap m_animations;

    std::string m_currentAnimationName;
};
