#pragma once

#include <unordered_map>
#include <string>
#include <vector>
#include <memory>

#include <nlohmann/json.hpp>

#define GLEW_STATIC
#include <gl/glew.h>

class Model;
class Texture;
class TextureSampler;
class CubeTexture;
class Shader;
class AssimpModel;
class MultiLayeredHeightmap;

class ResourceLoader
{
public:
    ResourceLoader();

    ~ResourceLoader();

	int32_t init(const std::string& configFilename);

public:
	const Shader* getShader(int32_t id) const;
	int32_t getShaderId(const Shader*) const;
	const Shader* getBorderShader(int32_t id) const;

	std::vector<const Model*> getModelList() const;

	template<class Type>
	const Type* getModel(int32_t id) const;
	template<class Type>
    int32_t getModelId(const Type* model) const;

    std::vector<int32_t> getTextureIds(const std::vector<const Texture *> &ids) const;

    const Texture* getCubeTexture(int32_t id) const;
    const Texture* getTextureSampler(int32_t id) const;

protected:
    template<class Type>
    int32_t getModelId(const Type* model,
                       const std::unordered_map<int32_t, std::unique_ptr<Type>>& models) const;


protected:
    template<class Type>
	std::unordered_map<int32_t, std::unique_ptr<Type>>
    initModels(const nlohmann::json& settings);

    std::unordered_map<int32_t, Shader*>
    initShaders(const nlohmann::json& settings, const std::string& path);

    void initHeightmaps(const nlohmann::json& settings);

    template<typename Type>
    std::unordered_map<int32_t, Texture*>
    initTextures(const nlohmann::json& settings);

    std::vector<const Texture*> getTextureSamplers(const nlohmann::json& settings) const;
    std::vector<const Texture*> getCubeTextures(const nlohmann::json& settings) const;

private:

    std::unordered_map<int32_t, Shader*> m_shaders;
    std::unordered_map<int32_t, Shader*> m_borderShaders;

	std::unordered_map<int32_t, std::unique_ptr<Model>> m_models;
    std::unordered_map<int32_t, std::unique_ptr<AssimpModel>> m_animations;
    std::unordered_map<int32_t, std::unique_ptr<MultiLayeredHeightmap>> m_heightmaps;

    std::unordered_map<int32_t, Texture*> m_textureHolders;
    std::unordered_map<int32_t, Texture*> m_cubeTextures;
};
