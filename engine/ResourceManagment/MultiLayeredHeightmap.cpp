﻿#include "MultiLayeredHeightmap.h"

#include <optional>
#include <vector>
#include <map>
#include <cmath>

#define GLEW_STATIC
#include <gl/glew.h>

#include <glm/glm.hpp>
#include <glm/gtx/intersect.hpp>

#include "Shader.h"
#include "Utils/TGA.h"
#include "Utils/Vertex.h"

std::vector<Vertex> calculateVertexData(const TGA::Image* tgaImage, size_t pixelSize)
{
    const int32_t width = tgaImage->width;
    const int32_t height = tgaImage->height;
    const int32_t stride = pixelSize * tgaImage->width;

    std::vector<Vertex> verticesData;
    verticesData.resize(width * height);

    for (int32_t j = 0; j < height; ++j)
    {
        for (int32_t i = 0; i < width; ++i)
        {
            float fScaleC = float(i) / float(width - 1);
            float fScaleR = float(j) / float(height - 1);

            size_t index = stride * i + j * pixelSize;
            float fVertexHeight = static_cast<float>(tgaImage->buffer[index]) / 255.0f - 0.5f;

            verticesData[i + j * width].position = glm::vec3(fScaleC - 0.5f, fVertexHeight, fScaleR - 0.5f);
            verticesData[i + j * width].uv = glm::vec2(fScaleC, fScaleR);
        }
    }


    std::vector<std::vector<glm::vec3>> tempNormals[2];
    for (auto i = 0; i < 2; i++)
    {
        tempNormals[i] = std::vector<std::vector<glm::vec3>>(height - 1, std::vector<glm::vec3>(width - 1));
    }

    for (auto i = 0; i < height - 1; i++)
    {
        for (auto j = 0; j < width - 1; j++)
        {
            const auto& vertexA = verticesData[i + j * width].position;
            const auto& vertexB = verticesData[i + 1 + j * width].position;
            const auto& vertexC = verticesData[i + (j + 1) * width].position;
            const auto& vertexD = verticesData[i + 1 + (j + 1) * width].position;

            glm::vec3 triangleNormalA = glm::cross((vertexB - vertexA ), (vertexA - vertexD ) );
            glm::vec3 triangleNormalB = glm::cross((vertexD - vertexC ), (vertexC - vertexB ) );

            tempNormals[0][i][j] = glm::normalize(triangleNormalA);
            tempNormals[1][i][j] = glm::normalize(triangleNormalB);
        }
    }

    for (auto i = 0; i < height; i++)
    {
        for (auto j = 0; j < width; j++)
        {
            const auto isFirstRow = i == 0;
            const auto isFirstColumn = j == 0;
            const auto isLastRow = i == height - 1;
            const auto isLastColumn = j == width - 1;

            auto finalVertexNormal = glm::vec3(0.0f, 0.0f, 0.0f);

            // Look for triangle to the upper-left
            if (!isFirstRow && !isFirstColumn)
            {
                finalVertexNormal += tempNormals[0][i - 1][j - 1];
            }

            // Look for triangles to the upper-right
            if (!isFirstRow && !isLastColumn)
            {
                finalVertexNormal += tempNormals[0][i - 1][j];
                finalVertexNormal += tempNormals[1][i - 1][j];
            }

            // Look for triangle to the bottom-right
            if (!isLastRow && !isLastColumn)
            {
                finalVertexNormal += tempNormals[0][i][j];
            }

            // Look for triangles to the bottom-right
            if (!isLastRow && !isFirstColumn)
            {
                finalVertexNormal += tempNormals[0][i][j - 1];
                finalVertexNormal += tempNormals[1][i][j - 1];
            }

            // Store final normal of j-th vertex in i-th row
            glm::vec3 scale = glm::vec3(0.5f * width, 1.0, 0.5f * height);
            verticesData[i + j * width].normal = glm::normalize(finalVertexNormal / scale);
        }
    }

    return verticesData;
}

std::vector<uint32_t> calculateIndeces(int32_t width, int32_t height)
{
    size_t noIndeces = 2 * (width - 1) * height;
    std::vector<uint32_t>indeces;
    indeces.resize(noIndeces);
    bool direction = false;
    for (int32_t i = 0; i < width - 1; ++i)
    {
        for (int32_t j = 0; j < height; ++j)
        {
            for (size_t k = 0; k < 2; ++k)
            {
                // TODO: do it in one pass
                int row = i + (1 - k);
                int index = direction ?
                    (row + 1) * (height)-j - 1 :
                    row * height + j;

                int temp = 2 * (i * height + j) + k;
                indeces[temp] = index;
            }
        }

        direction = !direction;
    }

    return indeces;
}

MultiLayeredHeightmap::MultiLayeredHeightmap()
    :
    Model()
    , m_width(0)
    , m_height(0)
{
    m_drawFlag = GL_TRIANGLE_STRIP;
}

bool MultiLayeredHeightmap::load(const std::string& filename)
{
    return create(TGA::Load(filename));
}

bool MultiLayeredHeightmap::create(std::unique_ptr<TGA::Image>&& tgaImage)
{
    if (nullptr == tgaImage)
    {
        return false;
    }

    size_t pixelSize = 1;
    if (tgaImage->bpp == 24)
    {
        pixelSize = 3;
    }
    else if (tgaImage->bpp == 32)
    {
        pixelSize = 4;
    }
    else
    {
        return false;
    }

    m_verticesData = calculateVertexData(tgaImage.get(), pixelSize);
    m_indeces = calculateIndeces(tgaImage->width, tgaImage->height);
    m_width = tgaImage->width;
    m_height = tgaImage->height;

    glGenBuffers(1, &m_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * m_indeces.size(), m_indeces.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glGenBuffers(1, &m_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_verticesData.size(), m_verticesData.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return true;
}


struct Line
{
    glm::vec2 p1;
    glm::vec2 p2;
};

float intersecRayLine(const glm::vec2& orig, const glm::vec2& dir, const Line& line)
{
    glm::vec2 v1 = orig - line.p1;
    glm::vec2 v2 = line.p2 - line.p1;
    glm::vec2 v3 = glm::vec2(-dir.y, dir.x);

    float dot = glm::dot(v2, v3);
    if (std::abs(dot) < FLT_EPSILON)
        return -1.0f;

    float t1 = (v2.x * v1.y - v2.y * v1.x) / dot;
    float t2 = glm::dot(v1, v3) / dot;

    if (t1 >= 0.0 && (t2 >= 0.0 && t2 <= 1.0))
        return t1;

    return -1.0f;
}

std::optional<Line> intersectRayRectangle(
        const glm::vec2& orig, const glm::vec2& dir, const std::array<glm::vec2, 4>& points)
{
    std::map<float, glm::vec2> resultPoints;

    std::array<size_t, 5> lines =
    {
        0, 1, 3, 2, 0
    };

    // get intersection points
    for (size_t i = 0; i < 4; ++i)
    {
        Line line{ points[lines[i]], points[lines[i + 1]] };
        float distance = intersecRayLine(orig, dir, line);
        if (distance > 0.0f)
        {
            resultPoints[distance] = orig + dir * distance;
        }
    }

    // if we have 1 intersection point it means orig is inside rect
    if (resultPoints.size() == 1)
    {
        Line result = { orig, resultPoints.begin()->second };
        return result;
    }

    // return 2 intersection points
    if (resultPoints.size() == 2)
    {
        auto iter = resultPoints.begin();
        auto firstPoint = iter->second;
        std::advance(iter, 1);
        auto secondPoint = iter->second;
        Line result = { firstPoint, secondPoint };
        return result;
    }

    // intersection points not founded
    return {};
}


std::array<glm::vec2, 4> getPlaneProjectionOnOxz(const glm::mat4& modelMatrix)
{
    std::array<glm::vec3, 4> plane;
    plane[0] = glm::vec3(-0.5f, 0.0f, -0.5f);
    plane[1] = glm::vec3(-0.5f, 0.0f, 0.5f);
    plane[2] = glm::vec3(0.5f, 0.0f, -0.5f);
    plane[3] = glm::vec3(0.5f, 0.0f, 0.5f);

    std::array<glm::vec2, 4> planeProjectionOnOxz;
    for (size_t i = 0; i < plane.size(); ++i)
    {
        plane[i] = glm::vec4(plane[i], 1) * modelMatrix;

        planeProjectionOnOxz[i].x = plane[i].x;
        planeProjectionOnOxz[i].y = plane[i].z;
    }

    return planeProjectionOnOxz;
}

glm::vec2 getSquareSize(const glm::mat4& modelMatrix)
{
    glm::vec3 planeSize = glm::vec3(1.0f, 0.0f, 1.0f);
    planeSize = glm::vec4(planeSize, 1) * modelMatrix;
    return glm::vec2(planeSize.x, planeSize.z);
}

std::optional<float> intersectRayTriangle
(
    const glm::vec3& orig, const glm::vec3& dir,
    const glm::vec3& vert0, const glm::vec3& vert1, const glm::vec3& vert2
)
{
    // find vectors for two edges sharing vert0
    const glm::vec3 edge1 = vert1 - vert0;
    const glm::vec3 edge2 = vert2 - vert0;

    // begin calculating determinant - also used to calculate U parameter
    const glm::vec3 p = glm::cross(dir, edge2);

    // if determinant is near zero, ray lies in plane of triangle
    const float det = glm::dot(edge1, p);
    const float detAbs = abs(det);
    if (detAbs < std::numeric_limits<float>::epsilon())
        return {}; // ray is parallel to the plane of the triangle

    const glm::vec3 dist = orig - vert0;
    glm::vec3 perpendicular = glm::cross(dist, edge1);

    // calculate U and V parameter
    glm::vec2 baryPosition;
    baryPosition.x = glm::dot(dist, p);
    baryPosition.y = glm::dot(dir, perpendicular);

    // test bounds
    if (baryPosition.x * det < static_cast<float>(0) ||
        baryPosition.y * det < static_cast<float>(0) ||
        abs(baryPosition.x) > detAbs ||
        abs(baryPosition.x + baryPosition.y) > detAbs)
    {
        return {};
    }

    // calculate distance, ray intersects triangle
    return glm::dot(edge2, perpendicular) / det;
}


std::optional<glm::vec3>
MultiLayeredHeightmap::checkVertexRect(const glm::mat4& modelMatrix,
                                       const glm::vec3& orig,
                                       const glm::vec3& dir,
                                       const std::array<int32_t, 4>& indeces) const
{
    std::array<glm::vec3, 4> rect;
    for (size_t i = 0; i < rect.size(); ++i)
    {
        auto& vertex = m_verticesData[indeces[i]].position;
        rect[i] = glm::vec4(vertex, 1) * modelMatrix;
    }

    glm::vec2 baryPoint;
    auto distance = intersectRayTriangle(
                orig, dir,
                rect[0], rect[1], rect[2]);
    if (distance.has_value() && distance.value() > 0.0f)
    {
        // Convert barycentric to world coordinates
        return orig + dir * distance.value();
    }

    distance = intersectRayTriangle(
                orig, dir,
                rect[3], rect[2], rect[1]);
    if (distance.has_value() && distance.value() > 0.0f)
    {
        // Convert barycentric to world coordinates
        return orig + dir * distance.value();
    }

    return {};
}

float distanceToNextPoint(float point, const int8_t& dir)
{
    float offset = point - static_cast<int32_t>(point);
    return dir < 0 ? offset : 1 - offset;
}

glm::vec2 distanceToNextPoint(const glm::vec2& point, const glm::ivec2& dir)
{
    return {distanceToNextPoint(point.x, dir.x),
            distanceToNextPoint(point.y, dir.y)};
}

std::optional<glm::vec3>
MultiLayeredHeightmap::traceLine(const Line& line,
                                 const glm::mat4& modelMatrix,
                                 const glm::vec3& orig,
                                 const glm::vec3& dir) const
{
    const glm::vec2 mapSize = glm::vec2(m_width - 1, m_height - 1);
    const glm::vec2 squareSize = getSquareSize(modelMatrix) / mapSize;

    const glm::vec2 lineDir = glm::vec2(dir.x, dir.z);
    const glm::vec2 dirAbs = glm::vec2(abs(lineDir.x), abs(lineDir.y));
    const glm::ivec2 dirSign = lineDir / dirAbs;

    const bool smallSide = dirAbs.x > dirAbs.y;
    const float stepAspect = dirAbs[smallSide] / dirAbs[!smallSide];

    const glm::vec2 firstPoint = line.p1 / squareSize + 0.5f * mapSize;

    const float distanceBetweenPoints = glm::length((line.p2 - line.p1) / squareSize);
    const float stepSize = glm::length(dirAbs);
    float distanceFromFirstPoint = -stepSize;

    const glm::vec2 toNextPoint = distanceToNextPoint(firstPoint, dirSign);
    bool smallerSideMoved = toNextPoint[smallSide] > stepAspect * toNextPoint[!smallSide];

    glm::vec2 point = firstPoint;
    do
    {
        const glm::ivec2 coord = point;
        bool isOutOfPlane = (coord.x < 0 || coord.x >= mapSize.x ||
                             coord.y < 0 || coord.y >= mapSize.y);
        if (!isOutOfPlane)
        {
            std::array<int32_t, 4> indeces;
            indeces[0] = coord.y * m_width + coord.x;
            indeces[1] = indeces[0] + 1;
            indeces[2] = indeces[0] + m_width;
            indeces[3] = indeces[2] + 1;

            auto result = checkVertexRect(modelMatrix, orig, dir, indeces);
            if (result.has_value())
            {
                return result.value();
            }
        }

        if (smallerSideMoved)
        {
            smallerSideMoved = false;
            point[!smallSide] += dirSign[!smallSide];
            distanceFromFirstPoint += stepSize;
            continue;
        }

        point[smallSide] += dirSign[smallSide] * stepAspect;
        smallerSideMoved = (int)point[smallSide] != coord[smallSide];
        if (!smallerSideMoved)
        {
            point[!smallSide] += dirSign[!smallSide];
            distanceFromFirstPoint += stepSize;
        }
    }
    while(distanceFromFirstPoint <= distanceBetweenPoints);

    return {};
}

std::optional<glm::vec3> MultiLayeredHeightmap::getIntersectedPoint(
    const glm::mat4& modelMatrix, const glm::vec3& orig, const glm::vec3& dir) const
{
    auto planeProjectionOnOxz = getPlaneProjectionOnOxz(modelMatrix);
    glm::vec2 origOxz(orig.x, orig.z);
    glm::vec2 dirOxz(dir.x, dir.z);
    auto projectionRayOnTerrainPlane = intersectRayRectangle(origOxz, dirOxz, planeProjectionOnOxz);

    // TODO: cut line by camera far value and terrain y min/max values
    return projectionRayOnTerrainPlane.has_value() ?
                traceLine(projectionRayOnTerrainPlane.value(), modelMatrix, orig, dir) :
                std::optional<glm::vec3>();
}

float barryCentric(const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& p3, const glm::vec2& position)
{
    float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
    float l1 = ((p2.z - p3.z) * (position.x - p3.x) + (p3.x - p2.x) * (position.y - p3.z)) / det;
    float l2 = ((p3.z - p1.z) * (position.x - p3.x) + (p1.x - p3.x) * (position.y - p3.z)) / det;
    float l3 = 1.0f - l1 - l2;
    return l1 * p1.y + l2 * p2.y + l3 * p3.y;
}

float MultiLayeredHeightmap::getHeight(const glm::mat4& modelMatrix, float x, float z) const
{
    auto squareSize = getSquareSize(modelMatrix) / glm::vec2(m_width, m_height);

    glm::ivec2 coord;
    coord.x = round(x / squareSize.x) + (m_width) / 2;
    coord.y = round(z / squareSize.y) + (m_height) / 2;

    if (coord.x < 0 || coord.x >= m_width - 1 ||
        coord.y < 0 || coord.y >= m_height - 1)
    {
        return NAN;
    }

    std::array<int32_t, 3> indeces;
    indeces[0] = coord.y * m_width + coord.x;
    indeces[1] = coord.y * m_width + coord.x + 1;
    indeces[2] = (coord.y + 1) * m_width + coord.x;

    if (x > (1 - z))
    {
        indeces[0] = (coord.y + 1) * m_width + coord.x + 1;
    }

    std::array<glm::vec3, 3> triangle;
    for (size_t i = 0; i < triangle.size(); ++i)
    {
        auto& vertex = m_verticesData[indeces[i]].position;
        triangle[i] = glm::vec4(vertex, 1) * modelMatrix;
    }

    return barryCentric(triangle[0], triangle[1], triangle[2], glm::vec2(x, z));
}
