#pragma once

#include "Model.h"

class Shader;

class SphereBound
{
public:
    // ctor/dtor
    SphereBound(float radius=1.0f, int sectorCount=36, int stackCount=18);

    void draw(const Shader* shader) const;

    static bool intersectBound(const glm::mat4& transform,
                               const glm::vec3& minBound,
                               const glm::vec3& maxBound,
                               const glm::vec3& orig,
                               const glm::vec3& dir,
                               float &distance);

    void setShader(const Shader* shader)
    {
        m_shader = shader;
    }
    const Shader* getShader() const
    {
        return m_shader;
    }
private:
    // member functions
    void buildVerticesSmooth(float radius, int sectors, int stacks);
    void buildIndices(int sectors, int stacks);

protected:

    const Shader* m_shader;

    GLuint m_indexBuffer;
    GLuint m_vertexBuffer;

    std::vector<uint32_t> m_indeces;
    std::vector<uint32_t> m_lineIndices;
};
