#pragma once

#include <assimp/scene.h>

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

static glm::quat assimpQuatToGlm(const aiQuaternion& quat)
{
	glm::quat result;
	result.w = quat.w;
	result.x = quat.x;
	result.y = quat.y;
	result.z = quat.z;
	return result;
}

static glm::vec3 assimpVecToGlm(const aiVector3D& vec)
{
	glm::vec3 result;
	result.x = vec.x;
	result.y = vec.y;
	result.z = vec.z;
	return result;
}

static glm::mat4 assimpMatrixToGlm(const aiMatrix4x4& mat)
{
	glm::mat4 result;
	size_t size = 4;
	for (size_t i = 0; i < size; ++i)
		for (size_t j = 0; j < size; ++j)
			result[j][i] = mat[j][i];
	return result;
}
