#include "CubeTexture.h"

#include <string>
#include <memory>
#include <algorithm>

#include "Utils/TGA.h"

bool CubeTexture::load(const std::string & filename, GLenum tiling)
{
	if (filename.empty())
	{
		return false;
	}

	// bind the texture to the 2D texture type 
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_texture);

	//set the filters for minification and magnification
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, tiling);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, tiling);
	//glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R_OES, tiling);

	auto tgaImage = TGA::Load(filename);
	if (nullptr == tgaImage)
	{
		return false;
	}

	if (tgaImage->bpp == 24)
	{
		struct pixel { uint8_t value[3]; };
		createTexture<pixel>(tgaImage->width, tgaImage->height, tgaImage->buffer.get(), GL_RGB, 3);
	}
	else if (tgaImage->bpp == 32)
	{
		struct pixel { uint8_t value[4]; };
		createTexture<pixel>(tgaImage->width, tgaImage->height, tgaImage->buffer.get(), GL_RGBA, 4);
	}
	else
	{
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
		return false;
	}

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	return true;
}

template<class BUFFER_TYPE>
void CubeTexture::createTexture(int32_t width, int32_t height,
	uint8_t* bufferTGA,
	GLenum pixelFormat,
	GLenum pixelSize)
{
	size_t imageWidth = width / 4;
	size_t imageHeight = height / 3;

	const size_t CUBE_FACE_NUMBER = 6;
	size_t bufferSize = imageWidth * imageHeight * pixelSize;
	std::unique_ptr<uint8_t[]> colorBuffers[CUBE_FACE_NUMBER];
	for (auto& buffer : colorBuffers)
		buffer = std::make_unique<uint8_t[]>(bufferSize);

	auto stride = imageWidth * pixelSize;

	auto reverseCopyStride = [&](size_t index, size_t shift, size_t dstI, size_t srcI)
	{
		auto dst = colorBuffers[index].get() + stride * dstI;
		size_t srcStep = width * pixelSize;
		auto src = bufferTGA + shift + srcI * srcStep;
		std::reverse_copy((BUFFER_TYPE *)src, (BUFFER_TYPE *)src + imageWidth, (BUFFER_TYPE  *)dst);
	};
	auto copyStride = [&](size_t index, size_t shift, size_t dstI, size_t srcI)
	{
		auto dst = colorBuffers[index].get() + stride * dstI;
		size_t srcStep = width * pixelSize;
		auto src = bufferTGA + shift + srcI * srcStep;
		std::copy((BUFFER_TYPE *)src, (BUFFER_TYPE *)src + imageWidth, (BUFFER_TYPE *)dst);
	};
	for (size_t i = 0; i < imageHeight; ++i)
	{
		reverseCopyStride(0, 2 * stride + width * imageHeight * pixelSize, i, imageHeight - i - 1); // right
		reverseCopyStride(1, width * imageHeight * pixelSize, i, imageHeight - i - 1); // left

		copyStride(2, stride + 2 * width * imageHeight * pixelSize, i, i); // top
		copyStride(3, stride, i, i); // bottom

		reverseCopyStride(4, 3 * stride + width * imageHeight * pixelSize, i, imageHeight - i - 1);// back
		reverseCopyStride(5, stride + width * imageHeight * pixelSize, i, imageHeight - i - 1); // front
	}

	for (size_t i = 0; i < CUBE_FACE_NUMBER; ++i)
	{
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, pixelFormat,
			imageWidth, imageHeight, 0, pixelFormat, GL_UNSIGNED_BYTE, colorBuffers[i].get());
	}
}
