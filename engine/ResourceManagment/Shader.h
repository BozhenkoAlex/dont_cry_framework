#pragma once

#include <vector>
#include <string>

#include <glm/glm.hpp>

#define GLEW_STATIC
#include <GL/glew.h>

class Shader
{
public:
	int32_t loadFromFile(const std::string& vertexFilename, const std::string& fragmentFilename);

	int32_t load(const std::string& vertexShader, const std::string& fragmentShader);

	void useProgram() const;

	void uniformTextures(const std::vector<GLuint>& textures) const;
	void uniformShadowMaps(const std::vector<GLuint>& shadowMaps) const;
	void uniformCubeTexture(GLuint cubeTexture) const;

    void uniformValues() const;
    void uniformAnimationValues() const;

	GLuint getProgram() const
	{
		return m_program;
	}

	GLuint getAttribLocation(const char* name) const
	{
		return glGetAttribLocation(m_program, name);
	}

	GLuint getUniformLocation(const char* name) const
	{
		return glGetUniformLocation(m_program, name);
	}

	~Shader();

protected:
	void initShaderParameters();

	static std::vector<GLint> getTexturesLocation(GLuint program, const std::string& name);

private:
	GLuint m_program;

	GLint m_positionAttribute;
	GLint m_normalAttribute;
	GLint m_uvAttribute;
	GLint m_boneIdsAttribute;
	GLint m_boneWeightsAttribute;

	std::vector<GLint> m_texturesToUniform;
	std::vector<GLint> m_shadowMapsToUniform;
	GLint m_cubeTextureUniform;
};
