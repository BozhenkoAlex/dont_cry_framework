﻿#include "Shader.h"

#include <string>

#include <glm/gtc/type_ptr.hpp>

#include "Utils/ShaderLoader.h"
#include "Utils/Vertex.h"

int32_t Shader::loadFromFile(const std::string& vertexFilename, const std::string& fragmentFilename)
{
    auto vertexShader = LoadShaderFromFile(GL_VERTEX_SHADER, vertexFilename.c_str());
    if (0 == vertexShader)
    {
        fprintf(stderr, "failed to build vertexShader\n");
        return -1;
    }

    auto fragmentShader = LoadShaderFromFile(GL_FRAGMENT_SHADER, fragmentFilename.c_str());
    if (0 == fragmentShader)
    {
        fprintf(stderr, "failed to build fragmentShader\n");
        glDeleteShader(vertexShader);
        return -2;
    }

    m_program = LoadProgram(vertexShader, fragmentShader);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    if (0 == m_program)
    {
        fprintf(stderr, "failed to LoadProgram\n");
        return -3;
    }

    initShaderParameters();

    return 0;
}

int32_t Shader::load(const std::string& vertexShader, const std::string& fragmentShader)
{
    auto vertexShaderID = LoadShader(GL_VERTEX_SHADER, vertexShader.c_str());
    if (0 == vertexShaderID)
    {
        return -1;
    }

    auto fragmentShaderID = LoadShader(GL_FRAGMENT_SHADER, fragmentShader.c_str());
    if (0 == fragmentShaderID)
    {
        glDeleteShader(fragmentShaderID);
        return -2;
    }

    m_program = LoadProgram(vertexShaderID, fragmentShaderID);

    glDeleteShader(vertexShaderID);
    glDeleteShader(fragmentShaderID);

    if (0 == m_program)
    {
        return -3;
    }

    initShaderParameters();

    return 0;
}

void Shader::useProgram() const
{
    glUseProgram(m_program);
}

void Shader::uniformTextures(const std::vector<GLuint>& textures) const
{
    GLuint textureIndex = 0;
    GLuint textureUnit = 0;
    for(auto& location: m_texturesToUniform)
    {
        glActiveTexture(GL_TEXTURE0 + textureUnit);
        glBindTexture(GL_TEXTURE_2D, textures[textureIndex]);
        glUniform1i(location, textureUnit);
        ++textureUnit;
        ++textureIndex;
    }
}

void Shader::uniformShadowMaps(const std::vector<GLuint>& textures) const
{
    GLuint textureIndex = 0;
    GLuint textureUnit = m_texturesToUniform.size();
    for (auto& location : m_shadowMapsToUniform)
    {
        glActiveTexture(GL_TEXTURE0 + textureUnit);
        glBindTexture(GL_TEXTURE_2D, textures[textureIndex]);
        glUniform1i(location, textureUnit);
        ++textureUnit;
        ++textureIndex;
    }
}

void Shader::uniformCubeTexture(GLuint cubeTexture) const
{
    GLuint textureUnit = m_texturesToUniform.size() + m_shadowMapsToUniform.size();
    if (-1 != m_cubeTextureUniform)
    {
        glActiveTexture(GL_TEXTURE0 + textureUnit);
        glBindTexture(GL_TEXTURE_CUBE_MAP, cubeTexture);
        glUniform1i(m_cubeTextureUniform, textureUnit);
    }
}

void Shader::uniformValues() const
{
    if (m_positionAttribute != -1)
    {
        glEnableVertexAttribArray(m_positionAttribute);
        glVertexAttribPointer(m_positionAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)offsetof(Vertex, position));
    }

    if (-1 != m_normalAttribute)
    {
        glEnableVertexAttribArray(m_normalAttribute);
        glVertexAttribPointer(m_normalAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)offsetof(Vertex, normal));
    }

    if (-1 != m_uvAttribute)
    {
        glEnableVertexAttribArray(m_uvAttribute);
        glVertexAttribPointer(m_uvAttribute, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)offsetof(Vertex, uv));
    }
}

void Shader::uniformAnimationValues() const
{
    if (-1 == m_boneWeightsAttribute || -1 == m_boneIdsAttribute)
    {
        return;
    }
    // OPENGL doesn't load more than 4 values in glVertexAttribPointer
    // so we should load it one by one or by strides
    for(int i = 0; i < Vertex::MAX_BONES_COUNT; ++i)
    {
        glEnableVertexAttribArray(m_boneIdsAttribute + i);
        GLvoid* strideOffset = (GLvoid *)(offsetof(Vertex, boneIds) + i * sizeof(int));
        glVertexAttribIPointer(m_boneIdsAttribute + i, 1, GL_INT, sizeof(Vertex), strideOffset);

        glEnableVertexAttribArray(m_boneWeightsAttribute + i);
        strideOffset = (GLvoid *)(offsetof(Vertex, boneWeights) + i * sizeof(float));
        glVertexAttribPointer(m_boneWeightsAttribute + i, 1, GL_FLOAT, GL_FALSE, sizeof(Vertex), strideOffset);
    }
}

Shader::~Shader()
{
    glDeleteProgram(m_program);
}

void Shader::initShaderParameters()
{
    //finding location of uniforms / attributes
    m_positionAttribute = glGetAttribLocation(m_program, "a_posL");
    m_uvAttribute = glGetAttribLocation(m_program, "a_uv");
    m_normalAttribute = glGetAttribLocation(m_program, "a_norm");
    m_boneIdsAttribute = glGetAttribLocation(m_program, "a_boneIDs[0]");
    m_boneWeightsAttribute = glGetAttribLocation(m_program, "a_weights[0]");

    m_cubeTextureUniform = glGetUniformLocation(m_program, "u_s_cubeTexture");
    m_texturesToUniform = getTexturesLocation(m_program, "u_s_textures");
    m_shadowMapsToUniform = getTexturesLocation(m_program, "u_s_shadowTextures");
}

std::vector<GLint> Shader::getTexturesLocation(GLuint program, const std::string& name)
{
    std::vector<GLint> textures;
    uint16_t textureUnit = 0;
    do
    {
        std::string textureName = name + "[" + std::to_string(textureUnit) + "]";
        auto location = glGetUniformLocation(program, textureName.c_str());

        if (-1 != location)
            textures.emplace_back(location);
        else
            break;

        textureUnit++;
    } while (true);

    return textures;
}
