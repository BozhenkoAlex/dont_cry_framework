#include "TextureSampler.h"

#include <string>

#define GLEW_STATIC
#include <GL/glew.h>

#include "Utils/TGA.h"

bool TextureSampler::load(const std::string& filename, GLenum tiling)
{
	if (filename.empty())

		return false;

	GLenum target = GL_TEXTURE_2D;

	// bind the texture to the 2D texture type 
	glBindTexture(target, m_texture);

	//set the filters for minification and magnification
	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//set the wrapping modes 
	glTexParameteri(target, GL_TEXTURE_WRAP_S, tiling);
	glTexParameteri(target, GL_TEXTURE_WRAP_T, tiling);

	glTexParameterf(target, GL_TEXTURE_LOD_BIAS, -0.4f);

	// create CPU buffer and load it with the image data
	auto tgaImage = TGA::Load(filename);
	if (nullptr == tgaImage)
	{
		return false;
	}

	GLenum pixelFormat = GL_RGB;
	if (tgaImage->bpp == 24)
	{
		pixelFormat = GL_RGB;
	}
	else if (tgaImage->bpp == 32)
	{
		pixelFormat = GL_RGBA;
	}
	else
	{
		return false;
	}

	glTexImage2D(target, 0, pixelFormat, tgaImage->width, tgaImage->height, 0, pixelFormat, GL_UNSIGNED_BYTE, tgaImage->buffer.get());

	// generate the mipmap chain 
	glGenerateMipmap(target);

	return true;
}
