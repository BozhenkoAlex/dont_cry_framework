#pragma once

#include <vector>
#include <memory>
#include <string>

#include <glm/glm.hpp>

class AssimpMesh;

struct AnimationNode;

struct aiNode;
struct aiAnimation;

class AssimpAnimation
{
public:
	AssimpAnimation();

	void load(double ticksPerSecond, double duration, const aiNode * pNode, const aiAnimation* pAnimation );

	~AssimpAnimation();

	void updateMeshes(const std::vector<std::unique_ptr<AssimpMesh>>&meshes, double time) const;

    glm::mat4 getRootTransform() const;

private:
	double getAnimationTime(double time) const;
private:
    std::unique_ptr<AnimationNode> m_rootNode;
	double m_ticksPerSecond;
    double m_duration;
};
