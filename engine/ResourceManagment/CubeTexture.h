#pragma once

#include "Texture.h"

class CubeTexture: public Texture
{
public:

	bool load(const std::string& filename, GLenum tiling) override;

private:
	template<class BUFFER_TYPE>
	void createTexture(int32_t width, int32_t height,
		uint8_t* bufferTGA,
		GLenum pixelFormat,
		GLenum pixelSize);
};

