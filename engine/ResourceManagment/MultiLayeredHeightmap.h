#pragma once

#include <memory>
#include <string>
#include <optional>

#include "Model.h"

struct Line;

namespace TGA
{
    struct Image;
}

class MultiLayeredHeightmap : public Model
{
public:

	MultiLayeredHeightmap();

	bool load(const std::string& filename) override;
    bool create(std::unique_ptr<TGA::Image>&& image);

    std::optional<glm::vec3> getIntersectedPoint(
            const glm::mat4& modelMatrix,
            const glm::vec3& orig,
            const glm::vec3& dir) const;

	float getHeight(const glm::mat4& modelMatrix, float x, float z) const;

protected:

    std::optional<glm::vec3> traceLine(const Line& line,
                                       const glm::mat4& modelMatrix,
                                       const glm::vec3& orig,
                                       const glm::vec3& dir) const;

    std::optional<glm::vec3> checkVertexRect(
            const glm::mat4& modelMatrix,
            const glm::vec3& orig,
            const glm::vec3& dir,
            const std::array<int32_t, 4>& indeces) const;

protected:
	int32_t m_width;
	int32_t m_height;
};
