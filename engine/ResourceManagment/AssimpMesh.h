#pragma once

#include <string>
#include <vector>
#include <memory>
#include <unordered_map>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

class Shader;
class Texture;

struct aiMesh;

class AssimpMesh
{
public:

	AssimpMesh();

	~AssimpMesh();

	void draw(const Shader * shader) const;

	void setBoneTransform(const std::string& name, const glm::mat4& transform);

    void setTexture(const std::shared_ptr<Texture>& texture);

    bool intersectBound(const glm::mat4& modelMatrix,
                         const glm::vec3& orig,
                         const glm::vec3& dir,
                         float &distance) const;

	static std::unique_ptr<AssimpMesh> processMesh(const aiMesh * mesh);

    glm::vec3 getMinBound() const
    {
        return m_minBound;
    }

    glm::vec3 getMaxBound() const
    {
        return m_maxBound;
    }

    glm::mat4 getRootTransform() const
    {
        return m_rootTransform;
    }

public:
	struct Bone
	{
        size_t index;
        glm::mat4 offsetMatrix = glm::mat4(1);
        glm::mat4 finalTransformation = glm::mat4(1);
	};

private:
    glm::mat4 m_rootTransform;

    std::shared_ptr<Texture> m_texture;

    std::unordered_map<std::string, Bone> m_boneTransforms;

    glm::vec3 m_minBound;
    glm::vec3 m_maxBound;

    std::vector<uint32_t> m_indices;
    uint32_t m_indexBuffer;
    uint32_t m_vertexBuffer;
};
