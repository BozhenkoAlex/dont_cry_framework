#include "AssimpMesh.h"

#define GLEW_STATIC
#include <GL/glew.h>


#include "AssimpMathUtils.h"
#include "Shader.h"
#include "Sphere.h"
#include "Texture.h"
#include "Utils/Vertex.h"

AssimpMesh::AssimpMesh()
{
	m_texture = nullptr;
    m_vertexBuffer = -1;
    m_minBound = glm::vec3(std::numeric_limits<float>::max());
    m_maxBound = glm::vec3(std::numeric_limits<float>::min());

    m_rootTransform = glm::mat4(1);
}

AssimpMesh::~AssimpMesh()
{
	glDeleteBuffers(1, &m_vertexBuffer);

	m_boneTransforms.clear();
}

void AssimpMesh::draw(const Shader * shader) const
{
	if (!shader)
	{
		return;
	}

	// Draw mesh
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);

    shader->uniformValues();
    shader->uniformAnimationValues();

    GLint location = shader->getUniformLocation("gBones[0]");
	if (-1 != location)
	{
        for (const auto& [name, bone] : m_boneTransforms)
		{
            auto &matrix = bone.finalTransformation;
            glUniformMatrix4fv(location + bone.index, 1, GL_TRUE, &matrix[0][0]);
		}
	}

	if (m_texture)
	{
		GLuint texture = m_texture->getTexture();
		shader->uniformTextures({ texture });
	}

    glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, m_indices.data());

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void AssimpMesh::setBoneTransform(const std::string& name, const glm::mat4& transform)
{
	if (m_boneTransforms.count(name))
	{
		auto& bone = m_boneTransforms[name];
        bone.finalTransformation = bone.offsetMatrix * transform;
        if (0 == bone.index)
        {
            m_rootTransform = bone.finalTransformation;
        }
	}
}

void AssimpMesh::setTexture(const std::shared_ptr<Texture> &texture)
{
    m_texture = texture;
}

std::vector<Vertex> LoadVertices(const aiMesh * mesh)
{
	std::vector<Vertex> vertices;
	vertices.resize(mesh->mNumVertices);

	// Walk through each of the mesh's vertices
	for (GLuint i = 0; i < mesh->mNumVertices; i++)
	{
		if (mesh->mVertices)
		{
            vertices[i].position = assimpVecToGlm(mesh->mVertices[i]);
		}

		// Normals
		if (mesh->mNormals)
		{
			vertices[i].normal = assimpVecToGlm(mesh->mNormals[i]);
		}

		// Texture Coordinates
		if (mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
		{
			glm::vec2 vec;
			// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertices[i].uv = vec;
        }
	}

	return vertices;
}

std::vector<GLuint> LoadIndices(const aiMesh * mesh)
{
	std::vector<GLuint> indices;
    indices.reserve(mesh->mNumFaces * 3);
	for (GLuint i = 0; i < mesh->mNumFaces; i++)
    {
        const aiFace& Face = mesh->mFaces[i];
        assert(Face.mNumIndices == 3);
        indices.push_back(Face.mIndices[0]);
        indices.push_back(Face.mIndices[1]);
        indices.push_back(Face.mIndices[2]);
	}
	return indices;
}

std::unordered_map<std::string, AssimpMesh::Bone>
LoadBones(const aiMesh * mesh,
          std::vector<Vertex>& vertices)
{
    std::unordered_map<std::string, AssimpMesh::Bone> boneTransforms;
    boneTransforms.reserve(mesh->mNumBones);
    for (GLuint i = 0; i < mesh->mNumBones; ++i)
    {
        const auto bone = mesh->mBones[i];

        std::string boneName = bone->mName.C_Str();
        boneTransforms[boneName].index = i;
        boneTransforms[boneName].offsetMatrix = assimpMatrixToGlm(bone->mOffsetMatrix);

        assert(bone->mWeights);
        for (GLuint w = 0; w < bone->mNumWeights; ++w)
        {
            auto weight = bone->mWeights[w];
            vertices[weight.mVertexId].addBoneData(i, weight.mWeight);
        }
    }

    return boneTransforms;
}

bool AssimpMesh::intersectBound(const glm::mat4& modelMatrix,
                                const glm::vec3& orig,
                                const glm::vec3& dir,
                                float& distance) const
{
    auto transformBound =  m_rootTransform * modelMatrix;
    return SphereBound::intersectBound(transformBound, m_minBound, m_maxBound, orig, dir, distance);
}

std::unique_ptr<AssimpMesh> AssimpMesh::processMesh(const aiMesh * mesh)
{
	auto result = std::make_unique<AssimpMesh>();
	auto vertices = LoadVertices(mesh);
	result->m_indices = LoadIndices(mesh);
    result->m_boneTransforms = LoadBones(mesh, vertices);

    for(auto& verticle: vertices)
    {
        verticle.normilizeBoneWeights();

        for(int i = 0;i<3; ++i)
        {
            result->m_minBound[i] = std::min(result->m_minBound[i], verticle.position[i]);
            result->m_maxBound[i] = std::max(result->m_maxBound[i], verticle.position[i]);
        }
    }

	glGenBuffers(1, &result->m_vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, result->m_vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return result;
}
