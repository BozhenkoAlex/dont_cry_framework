﻿#pragma once

#include <array>
#include <functional>

#define GLEW_STATIC
#include <GL/glew.h>

#include <glm/glm.hpp>

#include "ResourceManagment/Shader.h"

#include "FrameBufferObject.h"
#include "Utils/UniformValue.h"

#include <nlohmann/json.hpp>
using json = nlohmann::json;

class ImageRenderer;

class EffectManager
{
private:
	struct EffectPath
	{
		Shader* m_shader = nullptr;
		GLint m_fboIndex;
		std::vector<GLuint> m_textures;
		UniformValuesMap<float> m_unifroms;
	};

public:
	EffectManager();

	~EffectManager();

	int32_t init(const std::string &filename);

	int32_t addShader(size_t id, const std::string& vertexShader, const std::string& fragmentShader);

	void drawToTexture(size_t width, size_t height, std::function<void()>& draw);

	void drawThroughEffectPathes();

	void setFarAndNear(float nearPlane, float farPlane);

private:
	void draw(EffectPath* effectPath);

	void initShaders(const json& settings, const std::string& pathToShaders);

	void initEffectPathes(const json& settings);

	void uniformOffset(EffectPath* effectPath, GLuint programId);

private:
	size_t m_width;
	size_t m_height;

	std::unique_ptr<ImageRenderer> m_imageRenderer;
	
	FrameBufferObject m_fbo;
	std::unordered_map<size_t, std::unique_ptr<Shader>> m_shaders;

	std::vector<EffectPath*>m_effectPathes;
};
