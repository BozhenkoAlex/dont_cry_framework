﻿#include "FrameBufferObject.h"

FrameBufferObject::FrameBufferObject()
{
	m_fboId = 0;
}

FrameBufferObject::~FrameBufferObject()
{
	glDeleteBuffers(1, &m_fboId);
	glDeleteTextures(m_colorTexture.size(), m_colorTexture.data());
	glDeleteTextures(m_depthTexture.size(), m_depthTexture.data());
}

void FrameBufferObject::init(size_t texturesNum)
{
	glGenFramebuffers(1, &m_fboId);
	m_colorTexture.resize(texturesNum);
	glGenTextures(m_colorTexture.size(), m_colorTexture.data());
	m_depthTexture.resize(texturesNum);
	glGenTextures(m_depthTexture.size(), m_depthTexture.data());
}

void FrameBufferObject::bind(size_t index)
{
	if (isValidIndex(index))
	{
		glBindFramebuffer(GL_FRAMEBUFFER, m_fboId);
		attachTextureToFbo(m_colorTexture[index], GL_COLOR_ATTACHMENT0);
		attachTextureToFbo(m_depthTexture[index], GL_DEPTH_ATTACHMENT);
	}
}

void FrameBufferObject::unbind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FrameBufferObject::resize(size_t width, size_t height, GLenum wrap)
{
	for( auto& texture: m_colorTexture )
		resizeTexture(texture, width, height, GL_RGBA, wrap);

	for (auto& texture : m_depthTexture)
		resizeTexture(texture, width, height, GL_DEPTH_COMPONENT, wrap);
}

void FrameBufferObject::attachTextureToFbo(GLuint texture, GLenum attachmentFormat)
{
	glBindTexture(GL_TEXTURE_2D, texture);
	glFramebufferTexture2D(GL_FRAMEBUFFER,
		attachmentFormat,
		GL_TEXTURE_2D,
		texture,
		0);
}

void FrameBufferObject::resizeTexture(GLuint texture, size_t width, size_t height, GLenum textureFormat, GLenum wrap)
{
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);


	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap); // GL_REPEAT
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap);

	//NULL means reserve texture memory, but texels are undefined
	glTexImage2D(GL_TEXTURE_2D,
		0,
		textureFormat,
		width,
		height,
		0,
		textureFormat,
		GL_UNSIGNED_BYTE,
		nullptr);
}

bool FrameBufferObject::isValidIndex(size_t index) const
{
	return index < m_colorTexture.size() &&
		index < m_depthTexture.size();
}
