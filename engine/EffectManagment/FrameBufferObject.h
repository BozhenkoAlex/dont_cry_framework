﻿#pragma once

#include <vector>

#define GLEW_STATIC
#include <GL/glew.h>

class FrameBufferObject
{
public:
	FrameBufferObject();
	~FrameBufferObject();

	GLuint getColorTexture(size_t index) const
	{
		return isValidIndex(index) ? m_colorTexture[index] : 0;
	}

	GLuint getDepthTexture(size_t index) const
	{
		return isValidIndex(index) ? m_depthTexture[index] : 0;
	}

	void init(size_t texturesNum);

	void bind(size_t index);

	void unbind();

	void resize(size_t width, size_t height, GLenum wrap = GL_REPEAT);
private:

	void attachTextureToFbo(GLuint texture, GLenum attachmentFormat);

	void resizeTexture(GLuint texture, size_t width, size_t height, GLenum textureFormat, GLenum wrap);

	bool isValidIndex(size_t index) const;

private:
	GLuint m_fboId;
	std::vector<GLuint> m_colorTexture;
	std::vector<GLuint> m_depthTexture;
};
