﻿#include "EffectManager.h"

#include <fstream>

#include "ResourceManagment/ResourceLoader.h"
#include "Utils/ImageRenderer.h"

EffectManager::EffectManager()
	:
	m_width(0)
	, m_height(0)
{
	m_imageRenderer = std::make_unique<ImageRenderer>();
}

EffectManager::~EffectManager()
{
	m_shaders.clear();

	for (auto & effectPath : m_effectPathes)
		delete effectPath;
	m_effectPathes.clear();
}

int32_t EffectManager::init(const std::string &filename)
{
	std::ifstream iStream(filename);
	if (!iStream.is_open())
	{
		return -1;
	}

	json rootJson;
	iStream >> rootJson;

	if (rootJson.find("FBO") != rootJson.end())
	{
		auto value = rootJson["FBO"];
		if (!value.is_number() || value <= 0)
		{
			return -1;
		}
		m_fbo.init(value);
	}

	std::string pathToShaders;
	if (rootJson.find("pathToShaders") != rootJson.end())
	{
		pathToShaders = rootJson["pathToShaders"];
	}


	if(rootJson.find("shaders") != rootJson.end())
	{
		auto settings = rootJson["shaders"];
		initShaders(settings, pathToShaders);
	}

	if (rootJson.find("Pathes") != rootJson.end())
	{
		auto settings = rootJson["Pathes"];
		initEffectPathes(settings);
	}

	return 0;
}

int32_t EffectManager::addShader(size_t id, const std::string & vertexShader, const std::string & fragmentShader)
{
	m_shaders[id] = std::make_unique<Shader>();
	int32_t result = m_shaders[id]->loadFromFile(vertexShader, fragmentShader);
	if (result < 0)
	{
		m_shaders.erase(id);
	}
	return result;
}

void EffectManager::drawToTexture(size_t width, size_t height, std::function<void()>& draw)
{
	if (width < 1 || height < 1)
		return;

	if (width != m_width ||
		height != m_height)
	{
		m_width = width;
		m_height = height;
		m_fbo.resize(m_width, m_height);
	}

	m_fbo.bind(0);

	glClearColor(1.0f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glViewport(0, 0, width, height);

	draw();

	m_fbo.unbind();
}

void EffectManager::drawThroughEffectPathes()
{
	for (auto &effectPath : m_effectPathes)
	{
		draw(effectPath);
	}
}

void EffectManager::setFarAndNear(float nearPlane, float farPlane)
{
	for (auto &effectPath : m_effectPathes)
	{
		auto shader = effectPath->m_shader;
		auto programId = shader->getProgram();
		auto location = glGetUniformLocation(programId, "u_near");
		if (-1 != location)
			effectPath->m_unifroms["near"] = nearPlane;

		location = glGetUniformLocation(programId, "u_far");
		if (-1 != location)
			effectPath->m_unifroms["far"] = farPlane;

	}
}

void EffectManager::draw(EffectPath *effectPath)
{
	auto index = effectPath->m_fboIndex;
	m_fbo.bind(index);

	auto shader = effectPath->m_shader;
	shader->useProgram();

	shader->uniformTextures(effectPath->m_textures);

	auto programId = shader->getProgram();
	uniformOffset(effectPath, programId);
	effectPath->m_unifroms.uniform(programId);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_imageRenderer->bind();
	shader->uniformValues();
	m_imageRenderer->draw();
	m_imageRenderer->unbind();

	m_fbo.unbind();
}

void EffectManager::initShaders(const json& settings, const std::string& pathToShaders)
{
    for (auto &item : settings)
	{
        int32_t id = item["ID"];

        std::string vsFilename = item["VS"];
        std::string fsFilename = item["FS"];

		auto shader = std::make_unique<Shader>();
		int32_t result = shader->loadFromFile(pathToShaders + vsFilename, pathToShaders + fsFilename);
		if (result >= 0)
		{
			m_shaders[id] = std::move(shader);
		}
	}
}

void EffectManager::initEffectPathes(const json& settings)
{
	for (auto &item : settings)
	{
		m_effectPathes.push_back(new EffectPath());
		auto *lastElement = m_effectPathes.back();

		int32_t shaderId = item["Shader"];
		if (m_shaders.count(shaderId))
		{
            lastElement->m_shader = m_shaders[shaderId].get();
        }

		auto colorTexturesValue = item["ColorTextures"];
		for (auto &texture : colorTexturesValue)
		{
			if (texture.is_number())
			{
                int32_t fboTextureId = texture.get<int>() - 1;
				auto textureId = m_fbo.getColorTexture(fboTextureId);
				lastElement->m_textures.push_back(textureId);
			}
		}
		
		auto depthTexturesValue = item["DepthTextures"];
		for (auto &texture : depthTexturesValue)
		{
			if (texture.is_number())
			{
                int32_t fboTextureId = texture.get<int>() - 1;
				auto textureId = m_fbo.getDepthTexture(fboTextureId);
				lastElement->m_textures.push_back(textureId);
			}
		}

		auto targetValue = item["Target"];
		if (targetValue.is_number())
		{
            lastElement->m_fboIndex = targetValue.get<int>() - 1;
		}

		if (item.find("parameters") != item.end())
		{
			std::unordered_map<std::string, json> parameters = item["parameters"];
			for (auto& parameter : parameters)
			{
				float value = parameter.second;
				lastElement->m_unifroms[parameter.first] = value;
			}
		}
	}
}

void EffectManager::uniformOffset(EffectPath* effectPath, GLuint programId)
{
	auto location = glGetUniformLocation(programId, "u_offset");
	if (-1 != location)
	{
		GLfloat k = 2.0;
		if (effectPath->m_unifroms.count("radius"))
		{
			k = effectPath->m_unifroms["radius"];
		}
		GLfloat  offset[4];
		offset[0] = k / m_width;
		offset[1] = k / m_height;
		offset[2] = static_cast<float>(sqrt(2.) / 2.0) * offset[0];
		offset[3] = static_cast<float>(sqrt(2.) / 2.0) * offset[1];
		glUniform4fv(location, 1, offset);
	}
}
