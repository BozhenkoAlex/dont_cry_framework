#include "WaterObject.h"

#include "Utils/Vertex.h"

#include "ResourceManagment/Shader.h"
#include "ResourceManagment/Texture.h"

#include "lighting/SceneLight.h"

WaterObject::WaterObject()
	: m_shader(nullptr)
{
	std::vector<Vertex> verticlesData;
	verticlesData.resize(4);
	verticlesData[0].position = glm::vec3(-0.5, 0.0, -0.5);
	verticlesData[1].position = glm::vec3(-0.5, 0.0, 0.5);
	verticlesData[2].position = glm::vec3(0.5, 0.0, -0.5);
	verticlesData[3].position = glm::vec3(0.5, 0.0, 0.5);

	verticlesData[0].uv = glm::vec2(0.0, 0.0);
	verticlesData[1].uv = glm::vec2(0.0, 1.0);
	verticlesData[2].uv = glm::vec2(1.0, 0.0);
	verticlesData[3].uv = glm::vec2(1.0, 1.0);

	for (auto &vertex : verticlesData)
	{
		vertex.normal = glm::vec3(0, 1, 0);
	}

	m_indexes = { 0, 1, 2, 3 };

	glGenBuffers(1, &m_hVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_hVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * verticlesData.size(), verticlesData.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

    m_fbo.init(static_cast<size_t>(TexturesType::e_Count));
}

WaterObject::~WaterObject()
{
	glDeleteBuffers(1, &m_hVertexBuffer);
}

void WaterObject::drawObject(const glm::mat4& viewProjectionMatrix, const glm::vec4&)
{
	m_shader->useProgram();

	glBindBuffer(GL_ARRAY_BUFFER, m_hVertexBuffer);

    updateModelMatrix();

	m_shader->uniformValues();

	UniformValue<glm::mat4> viewProjection = viewProjectionMatrix;
	auto mvpMatrixLocation = m_shader->getUniformLocation("u_viewProjection");
	viewProjection.uniform(mvpMatrixLocation);

	auto modelMatrixLocation = m_shader->getUniformLocation("u_model");
	m_modelMatrix.uniform(modelMatrixLocation);

	uniformTextures();

	auto programId = m_shader->getProgram();
	m_vecParameters.uniform(programId);
	m_floatParameters.uniform(programId);

	if (m_sceneLight)
	{
		m_sceneLight->uniform(m_shader, programId);
	}

    glDrawElements(GL_TRIANGLE_STRIP, static_cast<GLsizei>(m_indexes.size()),
                   GL_UNSIGNED_BYTE, m_indexes.data());

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void WaterObject::drawRefraction(size_t width, size_t height, const std::function<void()>& draw)
{
    drawToTexture(TexturesType::e_Refraction, width, height, draw);
}

void WaterObject::drawReflection(size_t width, size_t height, const std::function<void()>& draw)
{
    drawToTexture(TexturesType::e_Reflection, width, height, draw);
}

void WaterObject::drawToTexture(TexturesType textureId, size_t width, size_t height, const std::function<void()>& draw)
{
	if (width < 1 || height < 1)
		return;

	if (width != m_width ||
		height != m_height)
	{
		m_width = width;
		m_height = height;
		m_fbo.resize(m_width, m_height);
	}

	m_fbo.bind(static_cast<size_t>(textureId));

	glClearColor(1.0f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
    glViewport(0, 0, static_cast<GLsizei>(width), static_cast<GLsizei>(height));

	draw();

	m_fbo.unbind();
}

void WaterObject::setFarAndNear(float nearPlane, float farPlane)
{
	m_floatParameters["far"] = farPlane;
	m_floatParameters["near"] = nearPlane;
}

void WaterObject::setShader(const Shader* shader)
{
	m_shader = shader;
}

const Shader* WaterObject::getShader() const
{
	return m_shader;
}

void WaterObject::setTextures(const std::vector<const Texture*>& textures)
{
	m_textures = textures;
}

const std::vector<const Texture*> WaterObject::getTextures() const
{
	return m_textures;
}

void WaterObject::uniformTextures() const
{
	std::vector<GLuint> textures;

    textures.push_back(m_fbo.getColorTexture(static_cast<size_t>(TexturesType::e_Reflection)));
    textures.push_back(m_fbo.getColorTexture(static_cast<size_t>(TexturesType::e_Refraction)));
    textures.push_back(m_fbo.getDepthTexture(static_cast<size_t>(TexturesType::e_Refraction)));
	textures.push_back(m_textures[0]->getTexture());
	textures.push_back(m_textures[1]->getTexture());
	
	m_shader->uniformTextures(textures);
}
