#pragma once

#include <memory>
#include <optional>
#include <unordered_map>

#include <glm/glm.hpp>

#include "lighting/SceneLight.h"

class SceneObject;
class ISceneObject;
class IHeightmap;
class TerrainObject;
class LookAtCamera;
class WaterObject;
class MultiLayeredHeightmap;

struct FogObject;

class SceneManager
{
public:
	friend class JsonLoader;
	friend class JsonWriter;

	SceneManager();
	~SceneManager();

	void resize(int32_t width, int32_t height);

	void updateCameraPosition();
	void draw(const glm::vec4& clipPlane = glm::vec4(0.0f)) const;
	void drawSelectedObj(int32_t selectedObjectId, const glm::vec4& clipPlane = glm::vec4(0.0f)) const;


#ifdef SHADOW_TEST
    void preranderShadows();
    void renderShadowMap();
#endif

	void preranderWater(size_t width, size_t height);
	void drawWater();

	void updateTime(float delta);

public:
	LookAtCamera* getCurrentCamera();
	const LookAtCamera* getCurrentCamera() const;
	void setCurrentCamera(size_t index);

    SceneObject* selectObject(int32_t x, int32_t y, int32_t width, int32_t height);
    ISceneObject* getObject(int32_t index);
    std::optional<size_t> getObjectIndex(const ISceneObject* object);
	std::vector<int32_t> getObjectsList();

    std::optional<glm::vec3>
    getTerrainInterscation(int32_t posX, int32_t posY, int32_t width, int32_t height) const;

protected:
	void setFog(std::unique_ptr<FogObject>&& fogObject);

protected:
    std::vector<std::unique_ptr<SceneObject>> m_sceneObjects;
    std::unique_ptr<TerrainObject> m_terrainObject;
    std::unique_ptr<SceneObject> m_waterObject;
    std::unique_ptr<SceneObject> m_skyObject;

	std::vector<LookAtCamera*> m_cameras;
	size_t m_currentCamera;

	SceneLighting m_sceneLighting;
    std::unique_ptr<FogObject> m_fogObject;
};
