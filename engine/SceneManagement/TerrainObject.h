#pragma once

#include <optional>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "SceneObject.h"

class TerrainObject : public SceneObject
{
public:
    std::optional<glm::vec3>
    getTerrainIntersectedPoint(const glm::vec3& orig, const glm::vec3& dir);
    float getTerrainHeigth(float x, float z);

    // TODO: should be a part of this class
    // returns ray origin position and direciton vector
    static std::optional<std::pair<glm::vec3, glm::vec3>>
    createScreenRay(
        int32_t x, int32_t y,
        int32_t width, int32_t height,
        const glm::mat4& viewMatrix,
        const glm::mat4& projectionMatrix)
    {
        const glm::vec4 viewport = glm::vec4(0.0f, 0.0f, width, height);
        const glm::vec3 nearPosition(x, height - y - 1, 0.0f);
        const glm::vec3 farPosition(x, height - y - 1, 1.0f);

        const glm::mat4& view = glm::transpose(viewMatrix);
        const glm::mat4& proj = glm::transpose(projectionMatrix);

        const glm::vec3 nearWorldRay = glm::unProject(nearPosition, view, proj, viewport);
        const glm::vec3 farWorldRay = glm::unProject(farPosition, view, proj, viewport);
        const glm::vec3 rayDirection = glm::normalize(farWorldRay - nearWorldRay);

        return std::make_pair(nearWorldRay, rayDirection);
    }
};
