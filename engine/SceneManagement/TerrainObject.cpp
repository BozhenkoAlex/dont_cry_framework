#include "TerrainObject.h"

#include "ResourceManagment/MultiLayeredHeightmap.h"

std::optional<glm::vec3>
TerrainObject::getTerrainIntersectedPoint(
        const glm::vec3& orig, const glm::vec3& dir)
{
    auto heightmapModel = dynamic_cast<const MultiLayeredHeightmap*>(m_model);
    if (!heightmapModel)
    {
        return {};
    }

    updateModelMatrix();

    return heightmapModel->getIntersectedPoint(m_modelMatrix, orig, dir);
}

float TerrainObject::getTerrainHeigth(float x, float z)
{
    auto heightmapModel = dynamic_cast<const MultiLayeredHeightmap*>(m_model);
    if (!heightmapModel)
    {
        return NAN;
    }

    updateModelMatrix();

    return heightmapModel->getHeight(m_modelMatrix, x, z);
}
