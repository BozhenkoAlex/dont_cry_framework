﻿#include "SceneManager.h"

#include <fstream>
#include <numeric>
#include <map>

#include "SceneObject.h"
#include "TerrainObject.h"
#include "FogObject.h"
#include "WaterObject.h"


SceneManager::SceneManager()
{
	m_currentCamera = -1;
	m_terrainObject = nullptr;
	m_skyObject = nullptr;
	m_waterObject = nullptr;
}

SceneManager::~SceneManager()
{
	m_sceneObjects.clear();

	for (auto& camera : m_cameras)
		delete camera;
	m_cameras.clear();
}

void SceneManager::resize(int32_t width, int32_t height)
{
	for (auto camera : m_cameras)
	{
		camera->updateProjectionAspect(static_cast<float>(width) / height);
	}
}

LookAtCamera* SceneManager::getCurrentCamera()
{
    bool isValid = m_currentCamera < m_cameras.size();
	return isValid ? m_cameras[m_currentCamera] : nullptr;
}

const LookAtCamera* SceneManager::getCurrentCamera() const
{
    bool isValid = m_currentCamera < m_cameras.size();
	return isValid ? m_cameras[m_currentCamera] : nullptr;
}

void SceneManager::setCurrentCamera(size_t index)
{
    bool isValid = index < m_cameras.size();
	if (isValid)
	{
        m_currentCamera = index;
#ifdef SHADOW_TEST
        m_sceneLighting.setCamera(m_cameras[m_currentCamera]);
#endif
    }

	assert(isValid);
}

void SceneManager::updateCameraPosition()
{
	auto currentCamera = getCurrentCamera();
	if (!currentCamera)
	{
		return;
	}

#ifdef SHADOW_TEST
    m_sceneLighting.cameraUpdated();
#endif

	auto& cameraPosition = currentCamera->getPosition();
	for (auto& obj : m_sceneObjects)
	{
		obj->updateCameraPosition(cameraPosition);
    }

	if (m_terrainObject)
	{
		m_terrainObject->updateCameraPosition(cameraPosition);
	}

	if (m_skyObject)
	{
		m_skyObject->updateCameraPosition(cameraPosition);
	}

	m_waterObject->updateCameraPosition(cameraPosition);

}

void SceneManager::draw(const glm::vec4& clipPlane) const
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CLIP_DISTANCE0);

	auto currentCamera = getCurrentCamera();
	if (!currentCamera)
	{
		return;
	}

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glClearColor(0.5f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	auto view = currentCamera->getViewMatrix();
	auto& projection = currentCamera->getPesrspective();
	auto viewProjectionMatrix = view * projection;

	for (auto& obj : m_sceneObjects)
	{
        obj->drawObject(viewProjectionMatrix, clipPlane);
#ifdef SELECTION_TEST
        obj->drawBound(viewProjectionMatrix);
#endif
    }

	if (m_terrainObject)
	{
		m_terrainObject->drawObject(viewProjectionMatrix, clipPlane);
	}

	if (m_skyObject)
    {
        glm::mat4 skyViewMatrix = view;
        skyViewMatrix[0][3] = skyViewMatrix[1][3] = skyViewMatrix[2][3] = 0;
        m_skyObject->drawObject(skyViewMatrix * projection, clipPlane);
	}

	glDisable(GL_BLEND);

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CLIP_DISTANCE0);
}

void SceneManager::drawSelectedObj(int32_t selectedObjectId, const glm::vec4& clipPlane) const
{
	auto currentCamera = getCurrentCamera();
    if (!currentCamera ||
        static_cast<size_t>(selectedObjectId) >= m_sceneObjects.size())
	{
		return;
	}
	auto view = currentCamera->getViewMatrix();
	auto& projection = currentCamera->getPesrspective();
	auto viewProjectionMatrix = view * projection;

	const auto& object = m_sceneObjects[selectedObjectId];

	glEnable(GL_STENCIL_TEST);
	glClear(GL_STENCIL_BUFFER_BIT);

	// 1. Write a 1 in the Stencil buffer for 
// every pixels of the first sphere:
// All colors disabled, we don't need to see that sphere
	glStencilFunc(GL_ALWAYS, 0x1, 0x1);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	glDepthMask(GL_FALSE);   // Edit: added this
	{
		object->drawBorder(viewProjectionMatrix, clipPlane);
	}

	// 2. Draw the second sphere with the following rule:
	// fail the stencil test for every pixels with a 1.
	// This means that  any pixel from first sphere will
	// not be draw as part of the second sphere.
	glStencilFunc(GL_EQUAL, 0x0, 0x1);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDepthMask(GL_TRUE);    // Edit: added this
	{
		object->addParameter("shift", 0.02f);
		object->drawBorder(viewProjectionMatrix, clipPlane);
	}

	object->addParameter("shift", 0.0f);
	glDisable(GL_STENCIL_TEST);
}

#ifdef SHADOW_TEST
void SceneManager::preranderShadows()
{
	auto currentCamera = getCurrentCamera();
	if (!currentCamera)
	{
		return;
	}

    // TODO: too huge lamda
	m_sceneLighting.prepranderShadows([this](const glm::mat4& viewProjectionMatrix)
									  {
										  glm::vec4 clipPlane(0.0f);

										  glEnable(GL_DEPTH_TEST);

										  glEnable(GL_BLEND);
										  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

										  glClearColor(0.5f, 0.0f, 0.0f, 1.0f);
										  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

										  for (auto& obj : m_sceneObjects)
										  {
											  obj->drawBorder(viewProjectionMatrix, clipPlane);
                                          }

										  if (m_terrainObject)
										  {
											  m_terrainObject->drawBorder(viewProjectionMatrix, clipPlane);
										  }

										  glDisable(GL_BLEND);

										  glDisable(GL_DEPTH_TEST);
									  });
}
	
void SceneManager::renderShadowMap()
{
	m_sceneLighting.renderShadowMap(0);
}
#endif

void SceneManager::preranderWater(size_t width, size_t height)
{
	auto currentCamera = getCurrentCamera();
	if (!currentCamera)
	{
		return;
	}

	float waterLevel = m_waterObject->getPosition().y;
	glm::vec3 cameraPosition = currentCamera->getPosition();
	float invertedPositionY = waterLevel - (cameraPosition.y - waterLevel);

	currentCamera->setPosition(glm::vec3(cameraPosition.x, invertedPositionY, cameraPosition.z));
	currentCamera->invertPitch();

    auto waterObject = static_cast<WaterObject*>(m_waterObject.get());

    waterObject->drawReflection(width, height, [this, waterLevel]()
								 {
									 draw(glm::vec4(0, 1, 0, -waterLevel));
								 });

	currentCamera->setPosition(cameraPosition);
	currentCamera->invertPitch();

    waterObject->drawRefraction(width, height, [this, waterLevel]()
								 {
									 draw(glm::vec4(0, -1, 0, waterLevel));
								 });
}

void SceneManager::drawWater()
{
	auto currentCamera = getCurrentCamera();
	if (!currentCamera)
	{
		return;
	}
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CLIP_DISTANCE0);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	auto viewProjectionMatrix = currentCamera->getViewMatrix() * currentCamera->getPesrspective();
    m_waterObject->drawObject(viewProjectionMatrix, glm::vec4(0.));

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CLIP_DISTANCE0);

	glDisable(GL_BLEND);
}

void SceneManager::updateTime(float delta)
{
	for (auto& obj : m_sceneObjects)
	{
		obj->updateTime(delta);
    }

	m_waterObject->updateTime(delta);
}

SceneObject* SceneManager::selectObject(int32_t x, int32_t y, int32_t width, int32_t height)
{
	auto currentCamera = getCurrentCamera();
	if (!currentCamera)
	{
		return nullptr;
	}

    auto ray = TerrainObject::createScreenRay(
                x, y, width, height, currentCamera->getViewMatrix(), currentCamera->getPesrspective());
    if (!ray.has_value())
	{
		return nullptr;
	}
    auto &[origin, direction] = ray.value();
    std::map<float, SceneObject*> preselectedObjects;
	for (auto& obj : m_sceneObjects)
    {
        float distance = obj->intersectBound(origin, direction);
        if (distance > 0.0f)
        {
            preselectedObjects[distance] = obj.get();
        }
	}

    for (auto& [distance, obj] : preselectedObjects)
    {
//        float accurateDistance = obj->intersect(ray.first, ray.second);
//        if (accurateDistance  > 0.0f)
        {
            return obj;
        }
    }

	return nullptr;
}

ISceneObject* SceneManager::getObject(int32_t index)
{
    bool isValid = static_cast<size_t>(index) < m_sceneObjects.size();
    return isValid ? m_sceneObjects[index].get(): nullptr;
}

std::optional<size_t> SceneManager::getObjectIndex(const ISceneObject* object)
{
	if (nullptr == object)
	{
        return {};
	}

	size_t index = 0;
	for (const auto& obj : m_sceneObjects)
	{
		if (obj.get() == object)
		{
			return index;
		}
		++index;
	}

    return {};
}

std::vector<int32_t> SceneManager::getObjectsList()
{
	std::vector<int32_t> list;
	list.resize(m_sceneObjects.size());
	std::iota(list.begin(), list.end(), 0);
	return list;
}

void SceneManager::setFog(std::unique_ptr<FogObject>&& fogObject)
{
    if (!fogObject)
    {
        return;
    }

    auto applyFog = [&fogObject](SceneObject* object)
    {
        object->addParameter("fogColor", fogObject->color);
        object->addParameter("start", fogObject->start);
        object->addParameter("range", fogObject->range);
    };

    if (m_waterObject)
    {
        applyFog(m_waterObject.get());
    }

    if (m_terrainObject)
    {
        applyFog(m_terrainObject.get());
    }

    for (auto& obj : m_sceneObjects)
    {
        applyFog(obj.get());
    }

    if (m_skyObject)
    {
        m_skyObject->addParameter("fogColor", fogObject->color);
        m_skyObject->addParameter("lowerLimit", fogObject->lowerLimit);
        m_skyObject->addParameter("upperLimit", fogObject->upperLimit);
    }


	m_fogObject = std::move(fogObject);
}

std::optional<glm::vec3>
SceneManager::getTerrainInterscation(
        int32_t posX, int32_t posY, int32_t width, int32_t height) const
{
    auto camera = getCurrentCamera();
    auto ray = TerrainObject::createScreenRay(
                posX, posY, width, height,
                camera->getViewMatrix(), camera->getPesrspective());
    if (!ray.has_value())
    {
        return {};
    }

    auto &[orig, dir] = ray.value();
    auto pos = m_terrainObject->getTerrainIntersectedPoint(orig, dir);
    return pos;
}
