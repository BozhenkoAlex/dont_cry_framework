#include "SceneObject.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include <array>

#include <glm/gtc/matrix_transform.hpp>

#include "ResourceManagment/Shader.h"
#include "ResourceManagment/Sphere.h"
#include "ResourceManagment/AssimpModel.h"
#include "ResourceManagment/Model.h"

#include "lighting/SceneLight.h"

static float TERRAIN_HEIGHT_SHIFT= 0.01f; // TODO: remove it

std::unique_ptr<SphereBound> SceneObject::m_boundSphere = nullptr;

SceneObject::SceneObject()
{
    m_model = nullptr;
    m_modelMatrix = glm::mat4(1);
    m_needCaluclateMatrix = false;

    m_floatParameters["time"] = 0.f;
    m_rotation = m_position = glm::vec3(0);
    m_scale = glm::vec3(1);

    m_terrainHeight = 0.0f;
    m_alignToTerrain = true;

    m_sceneLight = nullptr;
}

void SceneObject::setTerrainPosition(float x, float z, float height)
{
    m_position.x = x;
    m_position.z = z;
    setTerrainHeight(height);
    m_needCaluclateMatrix = true;
}

void SceneObject::setTerrainHeight(float height)
{
    if (!std::isnan(height))
    {
        m_terrainHeight = height + TERRAIN_HEIGHT_SHIFT;
        m_needCaluclateMatrix = true;
    }
}

glm::vec3 SceneObject::getObjectCenter() const
{
    if (!m_model)
    {
        return glm::vec3(NAN);
    }

    glm::vec4 center = glm::vec4(m_model->getObjectCenter(), 0) *
                       getTransformedModelMatrix();
    return m_position + glm::vec3(center);
}

void SceneObject::updateCameraPosition(const UniformValue<glm::vec3>& position)
{
    m_vecParameters["cameraPos"] = position;
}

void SceneObject::drawObject(const glm::mat4& viewProjectionMatrix, const glm::vec4& clipPlane)
{
    if (m_model)
    {
        auto shader = m_model->getShader();
        prepareDraw(shader, viewProjectionMatrix, clipPlane);
        m_model->draw(shader);
    }
}

void SceneObject::drawBorder(const glm::mat4& viewProjectionMatrix, const glm::vec4& clipPlane)
{
    if (m_model)
    {
        auto shader = m_model->getBorderShader();
        prepareDraw(shader, viewProjectionMatrix, clipPlane);
        m_model->draw(shader);
    }
}

void SceneObject::drawBound(const glm::mat4& viewProjectionMatrix) const
{
    if (m_model && SceneObject::m_boundSphere)
    {
        auto shader = SceneObject::m_boundSphere->getShader();
        shader->useProgram();
        shader->uniformValues();

        UniformValue<glm::mat4> viewProjection = viewProjectionMatrix;
        auto viewProjectionLocation = shader->getUniformLocation("u_viewProjection");
        viewProjection.uniform(viewProjectionLocation);

        glm::mat4 transform = m_modelMatrix;
        auto assimpModel = dynamic_cast<const AssimpModel*>(m_model);
        if (assimpModel)
        {
            auto rootTransform = assimpModel->getRootTransform();
            transform = rootTransform * transform;
        }

        auto minBound = m_model->getMinBound();
        auto maxBound = m_model->getMaxBound();
        glm::vec3 translatedMin = glm::vec4(minBound, 1.0f) * transform;
        glm::vec3 translatedMax = glm::vec4(maxBound, 1.0f) * transform;

        glm::vec3 sphereCenter = (translatedMax + translatedMin) * 0.5f;
        float radius = sqrt(glm::length(0.5f * (translatedMax - translatedMin)));

        glm::mat4 resultTransform(radius, 0, 0, sphereCenter.x,
                                  0, radius, 0, sphereCenter.y,
                                  0, 0, radius, sphereCenter.z,
                                  0, 0, 0, 1);

        auto modelMatrixLocation = shader->getUniformLocation("u_model");
        UniformValue(resultTransform).uniform(modelMatrixLocation);

        SceneObject::m_boundSphere->draw(shader);
    }
}

void SceneObject::initBoundSphere(const Shader* borderShader)
{
    SceneObject::m_boundSphere = std::make_unique<SphereBound>();
    SceneObject::m_boundSphere->setShader(borderShader);
}

void SceneObject::prepareDraw(const Shader* shader, const glm::mat4& viewProjectionMatrix, const glm::vec4& clipPlane)
{
    if (!shader)
    {
        return;
    }

    shader->useProgram();

    updateModelMatrix();

    UniformValue<glm::mat4> viewProjection = viewProjectionMatrix;
    auto viewProjectionLocation = shader->getUniformLocation("u_viewProjection");
    viewProjection.uniform(viewProjectionLocation);

    auto modelMatrixLocation = shader->getUniformLocation("u_model");
    m_modelMatrix.uniform(modelMatrixLocation);

    auto programId = shader->getProgram();
    m_vecParameters.uniform(programId);
    m_floatParameters.uniform(programId);

    if (m_sceneLight)
    {
        m_sceneLight->uniform(shader, programId);
    }

    UniformValue<glm::vec4> clipPlaneUniform = clipPlane;
    auto clipPlaneLocation = shader->getUniformLocation("u_clipPlane");
    clipPlaneUniform.uniform(clipPlaneLocation);
}

template<>
void SceneObject::addParameter(const std::string & name, const float& value)
{
    m_floatParameters[name] = value;
}

template<>
void SceneObject::addParameter(const std::string & name, const glm::vec3& value)
{
    m_vecParameters[name] = value;
}

void SceneObject::enableAlignToTerrain(bool enable)
{
    m_alignToTerrain = enable;
    if (m_alignToTerrain)
    {
        m_position.y = 0.0f;
    }

    m_needCaluclateMatrix = true;
}

float SceneObject::intersectBound(const glm::vec3& orig,
                                  const glm::vec3& dir) const
{
    float distance = -1.0f;
    if (m_model)
    {
        m_model->intersectBound(m_modelMatrix, orig, dir, distance);
    }

    return distance;
}

float SceneObject::intersect(const glm::vec3& /*orig*/,
                             const glm::vec3& /*dir*/) const
{
    // TODO:
    return -1.0f;
}

glm::mat4 SceneObject::calcModelMatrix(const glm::vec3& positionShift) const
{
    auto rotX = m_rotation.x ? glm::rotate(glm::mat4(1), abs(m_rotation.x), glm::vec3(m_rotation.x, 0, 0)) : glm::mat4(1);
    auto rotY = m_rotation.y ? glm::rotate(glm::mat4(1), abs(m_rotation.y), glm::vec3(0, m_rotation.y, 0)) : glm::mat4(1);
    auto rotZ = m_rotation.z ? glm::rotate(glm::mat4(1), abs(m_rotation.z), glm::vec3(0, 0, m_rotation.z)) : glm::mat4(1);
    auto rotationMatrix = rotY * rotX * rotZ;

    auto positionMatrix = glm::mat4(1.);
    positionMatrix[3][0] = positionShift.x;
    positionMatrix[3][1] = positionShift.y;
    positionMatrix[3][2] = positionShift.z;

    if (m_alignToTerrain)
    {
        positionMatrix[3][1] += m_terrainHeight;
    }

    auto scaleMatrix = glm::scale(glm::mat4(1.0), m_scale);

    return glm::transpose(positionMatrix * rotationMatrix * scaleMatrix);
}

void SceneObject::updateModelMatrix()
{
    if (m_needCaluclateMatrix)
    {
        m_modelMatrix = calcModelMatrix(m_position);
        m_needCaluclateMatrix = false;
    }
}

template<>
const std::unordered_map<std::string, UniformValue<glm::vec3>>& SceneObject::getParameters() const
{
    return m_vecParameters.data();
}

template<>
const std::unordered_map<std::string, UniformValue<float>>& SceneObject::getParameters() const
{
    return m_floatParameters.data();
}
