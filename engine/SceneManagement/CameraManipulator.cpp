#include "CameraManipulator.h"

#include <GLFW/glfw3.h>

#include "LookAtCamera.h"

CameraManipulator::CameraManipulator(LookAtCamera& camera)
    :
      m_currentCamera(camera)
{
}

void CameraManipulator::processKey(int32_t key, bool enable)
{
    switch(key)
    {
    case GLFW_KEY_W:
        m_movements[e_Forward] = enable;
        break;
    case GLFW_KEY_S:
        m_movements[e_Back] = enable;
        break;
    case GLFW_KEY_A:
        m_movements[e_Left] = enable;
        break;
    case GLFW_KEY_D:
        m_movements[e_Right] = enable;
        break;

    case GLFW_KEY_Q:
        m_rotations[e_Left] = enable;
        break;

    case GLFW_KEY_E:
        m_rotations[e_Right] = enable;
        break;

    case GLFW_KEY_SPACE:
        m_jump = enable;
        break;

    default:
        break;
    }
}

void CameraManipulator::rotate(float x, float y)
{
    m_currentCamera.rotatePitch(x);
    m_currentCamera.rotateYawByFloor(y);
}

glm::vec3 CameraManipulator::getDirectionVec(Directions direction)
{
    glm::vec3 forwardDirection(0);
    switch (direction)
    {
    case e_Forward:
    {
        forwardDirection = m_currentCamera.getRotation();
        break;
    }
    case e_Back:
    {
        forwardDirection = -m_currentCamera.getRotation();
        break;
    }
    case e_Left:
    {
        glm::vec3 cameraUp = m_currentCamera.getCameraTop();
        forwardDirection = -glm::normalize(glm::cross(m_currentCamera.getRotation(), cameraUp));
        break;
    }
    case e_Right:
    {
        glm::vec3 cameraUp = m_currentCamera.getCameraTop();
        forwardDirection = glm::normalize(glm::cross(m_currentCamera.getRotation(), cameraUp));
        break;
    }
    default:
        break;
    }

    return forwardDirection;
}

void CameraManipulator::move(const glm::vec3 &directionVec, float deltaTime)
{
    m_currentCamera.move(directionVec * deltaTime);
}

void CameraManipulator::update(float deltaTime)
{
    auto directionVec = glm::vec3(0);
    for (auto& [direction, isEnabled] : m_movements)
    {
        if (isEnabled)
        {
            directionVec += getDirectionVec(direction);
        }
    }

    const bool hasMove = glm::length(directionVec) > FLT_EPSILON;
    if (m_alignHorizontMovement && hasMove)
    {
        // ignores camera rotation by Y axis
        directionVec = glm::normalize(directionVec * glm::vec3(1,0,1));
    }

    if (m_alignHorizontMovement && m_jump)
    {
        directionVec += m_currentCamera.getCameraTop();
        m_jump = false;
    }

    if (hasMove)
    {
        move(directionVec, deltaTime);
    }
    else if(m_move)
    {
        stopMove();
    }

    if (m_rotations[e_Left] ^ m_rotations[e_Right])
    {
        m_currentCamera.rotateYawByFloor(
                    m_rotations[e_Right] ? deltaTime : -deltaTime);
    }
}
