#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class LookAtObject
{
public:
    LookAtObject();

    void setRotation(const glm::vec3 &rotation);

    const glm::vec3& getPosition() const { return m_lPosition; }
    const glm::vec3& getRotation() const { return m_wLookAt; }
    static glm::vec3 getCameraTop() { return m_cameraUp; }

    void invertPitch()
    {
        m_wLookAt.y = -m_wLookAt.y;
    }

    void setPosition(const glm::vec3 &position)
    {
        m_lPosition = position;
    }

    glm::mat4 getViewMatrix() const;

protected:
    void rotate(const glm::vec3 &axis, float angle);

protected:
    glm::vec3 m_lPosition;
    glm::vec3 m_wLookAt;

    static constexpr glm::vec3 m_cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);

//    glm::vec3 m_xAxis;
//    glm::vec3 m_yAxis;
//    glm::vec3 m_zAxis;
};
