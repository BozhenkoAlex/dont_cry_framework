#pragma once

#include "SceneObject.h"
#include "ResourceManagment/AssimpModel.h"

#include <iostream>

const float SPEED_MULTIPLIER = 5.0f;

class AnimationObject : public SceneObject
{
public:

    bool isAnimated() const override
    {
        return true;
    }

    void updateTime(float delta) override
    {
        SceneObject::updateTime(delta);

        auto assimpModel = dynamic_cast<const AssimpModel*>(m_model);
        assert(assimpModel);
        assimpModel->updateTime(m_currentAnimation, m_floatParameters["time"]);

        if (m_inMovement)
        {
            const glm::vec3 path = m_endPoint - m_position;
            const float pathLength = glm::length(path);

            const float deltaLenght = SPEED_MULTIPLIER * delta;
            if (deltaLenght < pathLength)
            {
                const glm::vec3 deltaPath = deltaLenght * glm::normalize(path);
                setPosition(m_position + deltaPath);
            }
            else
            {
                setPosition(m_endPoint);
                changeAnimation("idle");
                m_inMovement = false;
            }
        }
    }

    glm::mat4 getTransformedModelMatrix() const override
    {
        auto assimpModel = static_cast<const AssimpModel*>(m_model);
        auto rootTransform = assimpModel->getRootTransform();
        return rootTransform * (glm::mat4)m_modelMatrix;
    }

    void changeAnimation(const std::string& animation) override
    {
        m_currentAnimation = animation;
        m_floatParameters["time"] = 0;
    }

    void moveTo(const glm::vec3 &pos) override
    {
        if (!m_inMovement)
        {
            changeAnimation("walking");
        }

        m_endPoint = pos;
        m_inMovement = true;
        rotateObjectToDirection(m_endPoint - m_position);
    }

private:

    // TODO: refatcot it with ObjectManipulator
    void rotateObjectToDirection(const glm::vec3 &directionVec)
    {
        float angleSum = (directionVec.z) / (sqrt(directionVec.x * directionVec.x +
                                                  directionVec.z * directionVec.z));
        float angle = std::acos(angleSum);
        if (directionVec.x < 0)
        {
            angle = -angle;
        }

        auto objectRotation = getRotation();
        objectRotation.y = angle;
        setRotation(objectRotation);
    }

private:
    std::string m_currentAnimation = "idle";

    bool m_inMovement = false;
    glm::vec3 m_endPoint;
};

