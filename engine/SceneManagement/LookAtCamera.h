#pragma once

#include <array>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "LookAtObject.h"

class LookAtCamera : public LookAtObject
{
public:
    LookAtCamera();

    void setProjectionMatrix(float nearValue, float aspect, float farValue, float fov);
    void updateProjectionAspect(float aspect);

    void setSpeed(float speed)
    {
        m_cameraSpeed = speed;
    }

    void setRotationSpeed(float rotationSpeed)
    {
        m_cameraRotationSpeed = rotationSpeed;
    }

    const glm::mat4& getPesrspective() const { return m_perspective; }
    float getNear() const { return m_near; }
    float getFar() const { return m_far; }
    float getFovDegrees() const { return glm::degrees(m_fov); }
    float getFovRadians() const { return m_fov;	}
    float getSpeed() const { return m_cameraSpeed; }
    float getRotationSpeed() const { return m_cameraRotationSpeed; }
    float getAspect() const { return m_aspect; }

    void rotatePitch(float angle)
    {
        auto zAxis = glm::normalize(-m_wLookAt);
        auto xAxis = glm::normalize(cross(m_cameraUp, zAxis));
        rotate(xAxis, m_cameraRotationSpeed * angle);
    }

    void rotateYawByFloor(float angle)
    {
        rotate(glm::vec3(0, 1, 0), m_cameraRotationSpeed * angle);
    }

    void move(const glm::vec3& delta)
    {
        m_lPosition += delta * m_cameraSpeed;
    }

protected:
    std::array<glm::vec3, 3> getAxis() const
    {
        auto zAxis = glm::normalize(-m_wLookAt);
        auto xAxis = glm::normalize(cross(m_cameraUp, zAxis));
        auto yAxis = glm::cross(zAxis, xAxis);

        return {xAxis, yAxis, zAxis};
    }
private:
    float m_cameraSpeed;
    float m_cameraRotationSpeed;

    glm::mat4 m_perspective;

    float m_near;
    float m_far;
    float m_fov;
    float m_aspect;
};

