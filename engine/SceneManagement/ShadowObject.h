#pragma once

#include <array>
#include <functional>
#include <memory>

#include "EffectManagment/FrameBufferObject.h"
#include "SceneObject.h"


class Shader;
class LookAtCamera;

namespace test
{
	class ShadowMapping;
}

class ShadowObject
{
public:

	ShadowObject(int32_t width, int32_t height);
	~ShadowObject();

	void update();

	void setCamera(const LookAtCamera* camera);

	void setLightDirection(const glm::vec3& direction);

	void render(const std::function<void(const glm::mat4&)>& draw);

	void renderShadowMap() const;

	glm::mat4 getBiasMvp() const;

	GLuint getShadowMap() const;

protected:
    static std::array<glm::vec3, 8> calculateFrustumVertices(float cameraFov, float aspect, float nearPlane, float farPlane);

protected:

	glm::vec3 m_lightDirection;

	std::array<glm::vec3, 8> m_cameraDefaultFrustrum;

	FrameBufferObject m_fbo;
	size_t m_width;
	size_t m_height;

	const LookAtCamera* m_camera;

    std::unique_ptr<test::ShadowMapping> m_shadowMappingTest;

	glm::mat4 m_lightSpace;

	glm::vec3 m_minBound;
	glm::vec3 m_maxBound;
};

