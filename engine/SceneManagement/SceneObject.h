#pragma once

#include <unordered_map>
#include <vector>
#include <memory>

#define GLEW_STATIC
#include <gl/glew.h>

#include "Utils/UniformValue.h"

#include "interfaces/ISceneObject.h"

class SceneLighting;
class Shader;
class SphereBound;

class SceneObject: public ISceneObject
{
public:
	SceneObject();

    virtual ~SceneObject() = default;

	void setTerrainPosition(float x, float z, float height);
	void setTerrainHeight(float height);

    glm::vec3 getObjectCenter() const;
    virtual glm::mat4 getTransformedModelMatrix() const
    {
        return m_modelMatrix;
    }

	void updateCameraPosition(const UniformValue<glm::vec3>& cameraPosition);

    virtual void drawObject(const glm::mat4& viewProjectionMatrix, const glm::vec4& clipPlane);
    void drawBorder(const glm::mat4& viewProjectionMatrix, const glm::vec4& clipPlane);
    void drawBound(const glm::mat4& viewProjectionMatrix) const;

	template<class Type>
	void addParameter(const std::string & name, const Type& value);

    virtual void updateTime(float delta)
    {
        m_floatParameters["time"] = m_floatParameters["time"] + delta;
    }

    void enableAlignToTerrain(bool enable);

    float intersectBound(const glm::vec3& orig, const glm::vec3& dir) const;
    float intersect(const glm::vec3& orig, const glm::vec3& dir) const;

    void setSceneLight(const SceneLighting* light)
    {
        m_sceneLight = light;
    }

	template<class Type>
	const std::unordered_map<std::string, Type>& getParameters() const;

    virtual bool isAnimated() const
    {
        return false;
    }

    static void initBoundSphere(const Shader *borderShader);

protected:
    void prepareDraw(const Shader* shader, const glm::mat4& viewProjectionMatrix, const glm::vec4& clipPlane);

    glm::mat4 calcModelMatrix(const glm::vec3 &positionShift) const;
    void updateModelMatrix();

protected:
	UniformValue<glm::mat4> m_modelMatrix;

    float m_terrainHeight;

    bool m_alignToTerrain;

	UniformValuesMap<glm::vec3> m_vecParameters;
	UniformValuesMap<float> m_floatParameters;

	const SceneLighting* m_sceneLight;

    static std::unique_ptr<SphereBound> m_boundSphere;
};
