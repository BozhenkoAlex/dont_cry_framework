#pragma once

#include <string>
#include <memory>

#define GLEW_STATIC
#include <GL/glew.h>

#include <glm/glm.hpp>

#include "SceneManagement/LookAtCamera.h"
#include "ResourceManagment/Shader.h"
#include "Utils/UniformValue.h"

#ifdef SHADOW_TEST
#include "SceneManagement/ShadowObject.h"
#endif


class AmbientLight
{
public:

	void uniform(GLint programId) const
	{
		std::string colorLocationName = "u_ambientColor";
		auto colorLocation = glGetUniformLocation(programId, colorLocationName.c_str());
		m_color.uniform(colorLocation);

		std::string weightLocationName = "u_ambientWeight";
		auto weightLocation = glGetUniformLocation(programId, weightLocationName.c_str());
		m_weight.uniform(weightLocation);
	}

	void setColor(const glm::vec3& color)
	{
		m_color = color;
	}

	glm::vec3 getColor() const
	{
		return m_color;
	}

	void setWeight(float weight)
	{
		m_weight = weight;
	}

	float getWeight() const
	{
		return m_weight;
	}

protected:
	UniformValue<float> m_weight = 0.1f;
	UniformValue<glm::vec3> m_color = glm::vec3(1.0);
};

class PointLight
{
public:
	void uniform(GLint programId, GLuint unit) const
	{
		std::string uniformPrefix = "u_pointLights[" + std::to_string(unit) + "]";

		std::string colorLocationName = uniformPrefix + ".color";
		auto colorLocation = glGetUniformLocation(programId, colorLocationName.c_str());
		m_color.uniform(colorLocation);

		std::string poisitonLocationName = uniformPrefix + ".position";
		auto poisitonLocation = glGetUniformLocation(programId, poisitonLocationName.c_str());
		m_position.uniform(poisitonLocation);
	}

	UniformValue<glm::vec3> m_position;
	UniformValue<glm::vec3> m_color;
};

class DirectionalLight
{
public:
	void uniform(GLint programId, GLuint unit) const
	{
		std::string uniformPrefix = "u_directionLights[" + std::to_string(unit) + "]";

		std::string colorLocationName = uniformPrefix + ".color";
		auto colorLocation = glGetUniformLocation(programId, colorLocationName.c_str());
		m_color.uniform(colorLocation);

		std::string poisitonLocationName = uniformPrefix + ".direction";
		auto poisitonLocation = glGetUniformLocation(programId, poisitonLocationName.c_str());
		m_direction.uniform(poisitonLocation);

#ifdef SHADOW_TEST
		auto shadowMatrixLocation = glGetUniformLocation(programId, "u_shadowMapMatrix");
		UniformValue<glm::mat4> shadowMapMatrix = m_shadowsObject->getBiasMvp();
        shadowMapMatrix.uniform(shadowMatrixLocation);
#endif
	}

#ifdef SHADOW_TEST
	void setCamera(const LookAtCamera* camera)
    {
		if (m_shadowsObject)
		{
			m_shadowsObject->setCamera(camera);
		}
	}

	void renderShadowMap() const
	{
		if (m_shadowsObject)
		{
			m_shadowsObject->renderShadowMap();
		}
    }

	void prepranderShadowMap(const std::function<void(glm::mat4)>& drawFunc)
	{
		if (m_shadowsObject)
		{
			m_shadowsObject->render(drawFunc);
		}
    }

	void cameraUpdated()
	{
		if (m_shadowsObject)
		{
			m_shadowsObject->update();
        }
	}
#endif

	void setDirection(const glm::vec3& direction)
	{
		m_direction = direction;
#ifdef SHADOW_TEST
        m_shadowsObject->setLightDirection(m_direction);
#endif
    }

    glm::vec3 getDirection() const
    {
        return m_direction;
    }

	void setColor(const glm::vec3& color)
	{
		m_color = color;
	}

	glm::vec3 getColor() const
	{
		return m_color;
	}

#ifdef SHADOW_TEST
	GLuint getShadowMap()const
	{
		return m_shadowsObject->getShadowMap();
	}
#endif

protected:
	UniformValue<glm::vec3> m_direction;
	UniformValue<glm::vec3> m_color;

	const int32_t SHADOW_SIZE = 4096;
#ifdef SHADOW_TEST
    std::unique_ptr<ShadowObject> m_shadowsObject = std::make_unique<ShadowObject>(SHADOW_SIZE, SHADOW_SIZE);
#endif
};

class SceneLighting
{
public:
	void uniform(const Shader* shader, GLint programId) const
	{
        if (m_ambientLight)
        {
            m_ambientLight->uniform(programId);
        }

        for (GLuint i = 0; i < m_pointLights.size(); ++i)
		{
			m_pointLights[i]->uniform(programId, i);
        }

#ifdef SHADOW_TEST
		// TODO: precalculate it
		std::vector<GLuint> shadowMaps(m_directionalLights.size());
		for (size_t i = 0; i < m_directionalLights.size(); ++i)
		{
			m_directionalLights[i]->uniform(programId, i);
			shadowMaps[i] = m_directionalLights[i]->getShadowMap();
        }

		shader->uniformShadowMaps(shadowMaps);
#else
        for (GLuint i = 0; i < m_directionalLights.size(); ++i)
        {
            m_directionalLights[i]->uniform(programId, i);
        }
#endif
	}

    void addPointLight(std::unique_ptr<PointLight>&& light)
	{
		m_pointLights.emplace_back(std::move(light));
	}

    void addDirectionalLight(std::unique_ptr<DirectionalLight>&& light)
	{
		m_directionalLights.emplace_back(std::move(light));
	}

	void setAmbientLight(std::unique_ptr<AmbientLight>&& ambientLight)
	{
		m_ambientLight = std::move(ambientLight);
	}

	const std::vector<std::unique_ptr<PointLight>>& getPointLights() const
	{
		return m_pointLights;
	}

	const std::vector<std::unique_ptr<DirectionalLight>>& getDirectionalLights() const
	{
		return m_directionalLights;
	}

	const std::unique_ptr<AmbientLight>& getAmbientLight() const
	{
		return m_ambientLight;
    }

#ifdef SHADOW_TEST
    void setCamera(const LookAtCamera* camera)
    {
        for (auto& light : m_directionalLights)
        {
            light->setCamera(camera);
        }
    }

    void cameraUpdated()
    {
        for (auto& light : m_directionalLights)
        {
            light->cameraUpdated();
        }
    }

	void renderShadowMap(size_t index) const
	{
		if (index < m_directionalLights.size())
		{
			m_directionalLights[index]->renderShadowMap();
		}
	}

	void prepranderShadows(const std::function<void(glm::mat4)>& drawFunc)
	{
		for (auto& light : m_directionalLights)
		{
			light->prepranderShadowMap(drawFunc);
		}
    }
#endif

protected:
	std::vector<std::unique_ptr<PointLight>> m_pointLights;
	std::vector<std::unique_ptr<DirectionalLight>> m_directionalLights;

	std::unique_ptr<AmbientLight> m_ambientLight;
};
