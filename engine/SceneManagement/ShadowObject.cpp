#include "ShadowObject.h"

#include <glm/gtc/matrix_transform.hpp>

#include "Utils/Vertex.h"

#include "ResourceManagment/Shader.h"
#include "ResourceManagment/TextureSampler.h"

#include "LookAtCamera.h"

#include "Test/ShadowMappingTest.h"

static const float SHADOW_DISTANCE = 25.0f;

ShadowObject::ShadowObject(int32_t width, int32_t height)
	:
	m_width(width)
	, m_height(height)
	, m_camera(nullptr)
{
	m_fbo.init(1);
    m_fbo.resize(m_width, m_height, GL_CLAMP_TO_BORDER);

    m_shadowMappingTest = std::make_unique<test::ShadowMapping>();
}

ShadowObject::~ShadowObject()
{
}


glm::vec3 calculateLightSpaceFrustumCorner(glm::vec3 startPoint, glm::vec3 direction, float width)
{
	return startPoint + direction * width;
}

std::array<glm::vec3, 8> ShadowObject::calculateFrustumVertices(float cameraFov, float aspect, float nearPlane, float farPlane)
{
	float tanFov = tan(cameraFov);
		
	float nearHeight = nearPlane * tanFov;
	float farHeight = SHADOW_DISTANCE * tanFov;

	float nearWidth = nearHeight * aspect;
	float farWidth = farHeight * aspect;

	glm::vec3 forwardVec = glm::vec3(0, 0, -1);
	glm::vec3 upVec = glm::vec3(0, 1, 0);
	glm::vec3 rightVec = glm::cross(forwardVec, upVec);
	glm::vec3 leftVec = -rightVec;

	glm::vec3 nearBottom = nearPlane * forwardVec - nearHeight * upVec;
	glm::vec3 farBottom = farPlane * forwardVec - farHeight * upVec;

	glm::vec3 nearTop = nearPlane * forwardVec + nearHeight * upVec;
	glm::vec3 farTop = farPlane * forwardVec + farHeight * upVec;

	std::array<glm::vec3, 8> points;
	points[0] = calculateLightSpaceFrustumCorner(farTop, rightVec, farWidth);
	points[1] = calculateLightSpaceFrustumCorner(farTop, leftVec, farWidth);
	points[2] = calculateLightSpaceFrustumCorner(farBottom, rightVec, farWidth);
	points[3] = calculateLightSpaceFrustumCorner(farBottom, leftVec, farWidth);
	points[4] = calculateLightSpaceFrustumCorner(nearTop, rightVec, nearWidth);
	points[5] = calculateLightSpaceFrustumCorner(nearTop, leftVec, nearWidth);
	points[6] = calculateLightSpaceFrustumCorner(nearBottom, rightVec, nearWidth);
	points[7] = calculateLightSpaceFrustumCorner(nearBottom, leftVec, nearWidth);
	return points;
}

void ShadowObject::update()
{
	if (!m_camera)
	{
		return;
	}

	glm::vec3 rotation = glm::vec3(0,0,-1);
	//glm::vec3 rotation = m_camera->getRotation();
	glm::mat4 rotationMatrix = glm::lookAt(glm::vec3(0), rotation, glm::vec3(0, 1, 0));

	m_minBound = m_maxBound = glm::vec4(m_cameraDefaultFrustrum.front(), 1.0) * rotationMatrix;
	for (const auto& point : m_cameraDefaultFrustrum)
	{
		auto rotatedPoint = glm::vec4(point, 1.0) * rotationMatrix;
		for (size_t i = 0; i < 3; ++i)
		{
			m_minBound[i] = std::min(m_minBound[i], rotatedPoint[i]);
			m_maxBound[i] = std::max(m_maxBound[i], rotatedPoint[i]);
		}

//		printf("%f %f %f \n", rotatedPoint.x, rotatedPoint.y, rotatedPoint.z);
	}
//	printf("---------------\n");

//	printf("min box = %f %f %f \n", m_minBound.x, m_minBound.y, m_minBound.z);
//	printf("max box = %f %f %f \n", m_maxBound.x, m_maxBound.y, m_maxBound.z);
//	printf("---------------\n");

	glm::mat4 lightView = glm::lookAt(m_lightDirection, glm::vec3(0.0f), glm::vec3(0, 1, 0));

	glm::vec3 cameraPos = glm::vec3(0);
	//glm::vec3 cameraPos = m_camera->getPosition();
	glm::vec3 boundCenter = 0.5f * (m_minBound + m_maxBound) + cameraPos;
	glm::mat4 view = glm::translate(lightView, -boundCenter);
	

	glm::vec3 boundSize = 0.5f * (m_maxBound - m_minBound);
	std::swap(boundSize.y, boundSize.z);
	//boundSize.z = 7.0f;

	auto projection = glm::ortho(-boundSize.x, boundSize.x,
								 -boundSize.y, boundSize.y,
								 -boundSize.z, boundSize.z);

	m_lightSpace = glm::transpose(view) * glm::transpose(projection);
}

void ShadowObject::setCamera(const LookAtCamera* camera)
{
	m_camera = camera;
	if (m_camera)
	{
		float cameraFov = m_camera->getFovRadians();
		float aspect = m_camera->getAspect();
		float nearPlane = m_camera->getNear();
		m_cameraDefaultFrustrum = ShadowObject::calculateFrustumVertices(cameraFov, aspect, nearPlane, SHADOW_DISTANCE);
	}
}

void ShadowObject::setLightDirection(const glm::vec3& direction)
{
    m_lightDirection = direction;

    // TODO: for test only
    m_lightDirection = glm::vec3(0, 0.99, 0.01);
}

void ShadowObject::render(const std::function<void(const glm::mat4&)>& draw)
{
	m_fbo.bind(0);

	glClearColor(1.0f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glViewport(0, 0, m_width, m_height);

	draw(m_lightSpace);

	m_fbo.unbind();
}

void ShadowObject::renderShadowMap() const
{
    m_shadowMappingTest->draw(getShadowMap());
}

glm::mat4 ShadowObject::getBiasMvp() const
{
	return m_lightSpace;
}

GLuint ShadowObject::getShadowMap() const
{
	return m_fbo.getDepthTexture(0);
}
