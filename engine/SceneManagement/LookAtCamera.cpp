#include "LookAtCamera.h"

#include <glm/gtc/matrix_transform.hpp>

LookAtCamera::LookAtCamera()
: LookAtObject()
{
    m_perspective = glm::identity<glm::mat4>();
    m_near = 0.01f;
    m_far = 100.0f;
    m_fov = glm::radians(130.0f);
    m_aspect = 1.0f;

    m_cameraRotationSpeed = 30;
    m_cameraSpeed = 9;
}

void LookAtCamera::setProjectionMatrix(float nearValue, float aspect, float farValue, float fov)
{
	m_near = nearValue;
    m_far = farValue;

	m_fov = glm::radians(fov);

	updateProjectionAspect(aspect);
}

void LookAtCamera::updateProjectionAspect(float aspect)
{
	m_aspect = aspect;

    m_perspective = glm::transpose(
                glm::perspective(m_fov, m_aspect, m_near, m_far));
}
