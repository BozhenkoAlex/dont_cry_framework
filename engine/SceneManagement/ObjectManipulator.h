#pragma once

#include "CameraManipulator.h"

class SceneObject;

class ObjectManipulator : public CameraManipulator
{
public:
    explicit ObjectManipulator(SceneObject& sceneObject,
                               LookAtCamera& camera);

    void rotate(float x, float y) override;

protected:
    void move(const glm::vec3& directionVec, float deltaTime) override;
    void stopMove() override;

protected:
    void rotateCameraToDirection(const glm::vec3& directionVec);
    void rotateObjectToDirection(const glm::vec3& directionVec);
    void setCameraBehindObject();

protected:
    SceneObject& m_sceneObject;
};
