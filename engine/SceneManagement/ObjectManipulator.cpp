#include "ObjectManipulator.h"

#include "CameraManipulator.h"
#include "SceneObject.h"
#include "LookAtCamera.h"

// TODO: it should be parameters
const glm::vec3 POSITION_SHIFT = glm::vec3(0, 1, -1);
const float SPEED_MULTIPLIER = 10.0f;

ObjectManipulator::ObjectManipulator(SceneObject& sceneObject,
                                     LookAtCamera& camera)
    : CameraManipulator(camera)
    , m_sceneObject(sceneObject)
{
    m_alignHorizontMovement = true;
    rotateCameraToDirection(m_sceneObject.getRotation());
    setCameraBehindObject();
}

void ObjectManipulator::rotate(float x, float y)
{
    // TODO: fixme
    CameraManipulator::rotate(x, y);
    setCameraBehindObject();
}

void ObjectManipulator::move(const glm::vec3& directionVec, float deltaTime)
{
    auto objectPosition = m_sceneObject.getPosition();
    objectPosition += SPEED_MULTIPLIER * directionVec * deltaTime;
    m_sceneObject.setPosition(objectPosition);

    rotateObjectToDirection(m_currentCamera.getRotation());
    setCameraBehindObject();

    if (!m_move)
    {
        m_move = true;
        m_sceneObject.changeAnimation("walking");
    }
}

void ObjectManipulator::stopMove()
{
    m_move = false;
    m_sceneObject.changeAnimation("idle");
}

void ObjectManipulator::rotateCameraToDirection(const glm::vec3 &directionVec)
{
    glm::mat3 rotationMatrix = glm::rotate(glm::mat4(1),
                                           -directionVec.y,
                                           glm::vec3(0,1,0));
    auto objectDirection = glm::vec3(0,0,1) * rotationMatrix;
    m_currentCamera.setRotation(objectDirection);
}

void ObjectManipulator::rotateObjectToDirection(const glm::vec3 &directionVec)
{
    float angleSum = (directionVec.z) / (sqrt(directionVec.x * directionVec.x +
                                              directionVec.z * directionVec.z));
    float angle = std::acos(angleSum);
    if (directionVec.x < 0)
    {
        angle = -angle;
    }

    auto objectRotation = m_sceneObject.getRotation();
    objectRotation.y = angle;
    m_sceneObject.setRotation(objectRotation);
}

void ObjectManipulator::setCameraBehindObject()
{
    auto objectPosition = m_sceneObject.getObjectCenter();
    glm::mat3 cameraDirectionMatrix = glm::lookAt(objectPosition,
                                                  objectPosition - m_currentCamera.getRotation(),
                                                  m_currentCamera.getCameraTop());
    objectPosition += POSITION_SHIFT * cameraDirectionMatrix;
    m_currentCamera.setPosition(objectPosition);
}
