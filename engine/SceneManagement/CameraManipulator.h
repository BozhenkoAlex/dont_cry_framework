#pragma once

#include "interfaces/ICameraManipulator.h"

#include <unordered_map>
#include <functional>

#include <glm/glm.hpp>

class LookAtCamera;

class CameraManipulator: public ICameraManipulator
{
public:
    explicit CameraManipulator(LookAtCamera& camera);

    void processKey(int32_t key, bool enable) override;

    void rotate(float x, float y) override;
    void update(float deltaTime) override;

protected:

    glm::vec3 getDirectionVec(Directions direction);
    virtual void move(const glm::vec3& directionVec, float deltaTime);
    virtual void stopMove() {}

protected:
    LookAtCamera& m_currentCamera;

    std::unordered_map<Directions, bool> m_movements;
    std::unordered_map<Directions, bool> m_rotations;
    bool m_jump = false;
    bool m_move = false;

    bool m_alignHorizontMovement = false;
};
