#pragma once

#include <glm/glm.hpp>

struct FogObject
{
	glm::vec3 color;
	float start;
	float range;
	float lowerLimit;
	float upperLimit;
};
