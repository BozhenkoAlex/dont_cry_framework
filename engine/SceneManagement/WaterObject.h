#pragma once

#include <functional>

#include "EffectManagment/FrameBufferObject.h"
#include "SceneObject.h"

class Shader;

class Texture;

class WaterObject : public SceneObject
{
public:

	WaterObject();

	~WaterObject();

    void drawObject(const glm::mat4 & viewProjectionMatrix, const glm::vec4& clipPlane) override;

	void drawRefraction(size_t width, size_t height, const std::function<void()>& draw);
	void drawReflection(size_t width, size_t height, const std::function<void()>& draw);

	void setFarAndNear(float nearPlane, float farPlane);

	void setShader(const Shader* shader);
	const Shader* getShader() const;

    void setTextures(const std::vector<const Texture*>& textures);
    const std::vector<const Texture*> getTextures() const;

protected:

    enum class TexturesType : int8_t
	{
		e_Reflection = 0,
		e_Refraction,

		e_Count
	};

    void drawToTexture(WaterObject::TexturesType textureId, size_t width, size_t height, const std::function<void()>& draw);

	void uniformTextures() const;

protected:

    std::vector<const Texture*> m_textures;

	GLuint m_hVertexBuffer;
	std::vector<uint8_t> m_indexes;

	FrameBufferObject m_fbo;
	size_t m_width;
	size_t m_height;

	const Shader* m_shader;
};
