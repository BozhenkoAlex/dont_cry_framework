#include "LookAtObject.h"

LookAtObject::LookAtObject()
{
    m_lPosition = glm::vec3(0,0,1);
    m_wLookAt = glm::vec3(0,0,-1);

//    m_xAxis = m_yAxis = m_zAxis = glm::vec3(0);
//    m_xAxis.x = m_yAxis.y = m_zAxis.z = 1;
}

void LookAtObject::setRotation(const glm::vec3 &rotation)
{
    m_wLookAt = rotation;

//    m_zAxis = glm::normalize(-m_wLookAt);
//    m_xAxis = glm::normalize(cross(m_cameraUp, m_zAxis));
//    m_yAxis = glm::cross(m_zAxis, m_xAxis);
}

glm::mat4 LookAtObject::getViewMatrix() const
{
    //glm::mat4 view(1);
    //view[0][0] = m_xAxis.x;
    //view[1][0] = m_xAxis.y;
    //view[2][0] = m_xAxis.z;
    //view[0][1] = m_yAxis.x;
    //view[1][1] = m_yAxis.y;
    //view[2][1] = m_yAxis.z;
    //view[0][2] = m_zAxis.x;
    //view[1][2] = m_zAxis.y;
    //view[2][2] = m_zAxis.z;
    //view[3][0] = -glm::dot(m_xAxis, m_lPosition);
    //view[3][1] = -glm::dot(m_yAxis, m_lPosition);
    //view[3][2] = -glm::dot(m_zAxis, m_lPosition);

    glm::mat4 view = glm::lookAt(m_lPosition, m_lPosition + m_wLookAt, m_cameraUp);
    return glm::transpose(view);
}

void LookAtObject::rotate(const glm::vec3 &axis, float angle)
{
    auto rotationMatrix = glm::mat3( glm::rotate(glm::mat4(1), angle, axis) );

    m_wLookAt = m_wLookAt * rotationMatrix;

//    m_xAxis = m_xAxis * glm::mat3(rotationMatrix);
//    m_yAxis = m_yAxis * glm::mat3(rotationMatrix);
//    m_zAxis = m_zAxis * glm::mat3(rotationMatrix);
}
