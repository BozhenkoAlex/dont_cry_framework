cmake_minimum_required(VERSION ${cmake_use_version})

set(SOURCE
    ${CMAKE_CURRENT_SOURCE_DIR}/engine.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/interfaces/GlfwWindowWrapper.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/interfaces/InterfaceBuilder.cpp

    ${CMAKE_CURRENT_SOURCE_DIR}/Utils/JsonWriter.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Utils/UniformValue.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Utils/JsonLoader.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Utils/ShaderLoader.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Utils/TGA.cpp

    ${CMAKE_CURRENT_SOURCE_DIR}/EffectManagment/EffectManager.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/EffectManagment/FrameBufferObject.cpp

    ${CMAKE_CURRENT_SOURCE_DIR}/SceneManagement/WaterObject.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/SceneManagement/LookAtObject.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/SceneManagement/LookAtCamera.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/SceneManagement/ShadowObject.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/SceneManagement/CameraManipulator.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/SceneManagement/ObjectManipulator.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/SceneManagement/SceneManager.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/SceneManagement/SceneObject.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/SceneManagement/TerrainObject.cpp

    ${CMAKE_CURRENT_SOURCE_DIR}/ResourceManagment/AssimpAnimation.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/ResourceManagment/ResourceLoader.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/ResourceManagment/MultiLayeredHeightmap.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/ResourceManagment/AssimpModel.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/ResourceManagment/CubeTexture.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/ResourceManagment/Model.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/ResourceManagment/TextureSampler.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/ResourceManagment/Shader.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/ResourceManagment/Sphere.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/ResourceManagment/AssimpMesh.cpp
    )

set(HEADERS
    ${THIRDPARTY_HEADERS}
    ${CMAKE_CURRENT_SOURCE_DIR}/
    ${CMAKE_CURRENT_SOURCE_DIR}/Test
    )

# add the executable
add_library(engine ${SOURCE})

target_include_directories(engine
    PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/interfaces)
target_include_directories(engine PRIVATE ${HEADERS})

target_include_directories(engine PRIVATE ${OpenGL_INCLUDE_DIRS} ${GLEW_INCLUDE_DIRS} ${ASSIMP_INCLUDE_DIR})
target_link_libraries(engine PRIVATE ${OPENGL_LIBRARY} ${GLEW_LIBRARIES} glfw ${ASSIMP_LIBRARIES})


#add a test
if (${TEST_ENGINE})
    set(SOURCE ${CMAKE_CURRENT_SOURCE_DIR}/Tests/heightmap_test.cpp)

    # add the executable
    add_executable(heightmap_test ${SOURCE})

    set(LINK_LIBS engine imgui)
    target_include_directories(heightmap_test PRIVATE ${HEADERS})
    target_link_libraries(heightmap_test PUBLIC ${LINK_LIBS})
    target_include_directories(heightmap_test PUBLIC ${HEADERS})
endif()
